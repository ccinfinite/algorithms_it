
Capitolo Sei: Ordinamenti
*************************

Sezione 1. Introduzione
-----------------------

Data una collezione di elementi, e supposto che esista, nel linguaggio, un ordine definito su di essi,
*ordinare* significa riarrangiare gli elementi in modo tale che essi siamo disposti dal più piccolo
al più grande.
In Pyhton l'ordine di default degli oggetti è definito usando l'operatore \\( < \\), 
che ha la proprietà di essere irriflessivo ( \\(k\\) non è minore di se stesso) e transitivo
(se \\(k_1 < k_2\\) e \\(k_2 < k_3\\), allora \\(k_1 < k_3\\) ).
Inoltre, Python ha alcune funzione built-in per ordinare i dati, nella forma del metodo ``sort`` 
della classe ``list`` (che riarrangia il contenuto di una lista), e della funzione built-in ``sorted`` 
che produce una nuova lista contenente gli elementi ordinati di una arbitraria collezione.
Queste funzioni sono altamente ottimizzate, ed usano algoritmi avanzati.

Un programmatore solitamente usa funzioni di ordinamento built-in, poiché è inusuale avere esigenze
così particolari da dover implementare un nuovo e diverso algoritmo di ordinamento.
Tuttavia è importante capire come sono costruiti gli ordinamenti, per poter valutare le loro performance.

L'ordinamento è uno dei problemi più studiati in informatica; insiemi di dati sono spesso immagazzinati
in un certo ordine, ad esempio, per permettere ricerche più efficienti con l'algoritmo di ricerca binaria.
Molti algoritmi avanzati per una varietà di problemi si basano sull'ordinamento come subroutine.


Sezione 2. Algoritmi di ordinamento basati su array
---------------------------------------------------
Sezione 2.1 Bubble sort
=======================

Il *bubble sort* compie un processo molto semplice: realizza scansioni multiple di una lista, confrontando
coppie di valori adiacenti e scambiandoli se essi non sono nell'ordine corretto.
Per ogni scansione il valore più grande *galleggia* fino alla sua locazione corretta.

Il codice Python del bubble sort è il seguente::

 1  def bubbleSort(alist):
 2    for passnum in range(len(alist)-1,0,-1):
 3      for i in range(passnum):
 4        if alist[i] > alist[i+1]:
 5          temp = alist[i]
 6          alist[i] = alist[i+1]
 7          alist[i+1] = temp

Si noti che se la lista o array ha \\(n\\) elementi, ci saranno \\(n-1\\) confronti durante la prima scansione,
\\(n-2\\) durante la seconda scansione, e così via, finché solo un confronto è necessario al passo
\\(n-1\\)-mo, con il valore più piccolo nella sua corretta posizione.
Il numero totale di confronti è la somma del primi \\(n-1\\) interi, cioè \\((n-1)n/2\\).
Ciò significa che l'algoritmo ha complessità temporale \\(O(n^2)\\).
Nel caso migliore la lista è già ordinata, e non ci sono scambi.
Nel caso peggiore ogni confronto causa uno scambio, rendendo il bubble sort molto inefficiente.
Notare che se durante una scansione non ci sono scambi, allora la lista è già ordinata.

La figura seguente mostra la prima scansione del bubble sort; gli oggetti ombreggiati sono
confrontati ed eventualmente scambiati.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura61.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Si può scrivere una versione leggermente modificata dell'algoritmo, il *short bubble sort*,
che si ferma prima se trova che una lista è già ordinata::

 1  def shortbubbleSort(alist):
 2    exchanges = True
 3    passnum = len(alist)-1
 4    while passnum > 0 and exchanges:
 5      exchanges = False
 6      for i in range(passnum):
 7        if alist[i] > alist[i+1]:
 8          exchanges = True
 9          temp = alist[i]
 10         alist[i] = alist[i+1]
 11         alist[i+1] = temp
 12     passnum = passnum-1


Sezione 2.2 Selection sort
==========================

Il *selection sort* compie scansioni multiple su una lista di lunghezza \\(n\\),
cercando il valore più grande e spostandolo nella sua corretta locazione;
ciò significa che si ha un solo scambioper ogni scansione.
Dopo la prima scansione l'oggetto più grande è nella giusta posizione.
Dopo la seconda scansione quello immediatamente più piccolo è nella giusta posizione.
Il processo continua per \\(n-1\\) scansioni, quando l'ultimo oggetto è nel suo posto.

Il codice Python è dato di seguito::

 1  def selectionSort(alist):
 2    for fillslot in range(len(alist)-1,0,-1):
 3      positionOfMax=0
 4      for location in range(1,fillslot+1):
 5        if alist[location] > alist[positionOfMax]:
 6           positionOfMax = location
 7      temp = alist[fillslot]
 9      alist[fillslot] = alist[positionOfMax]
 10     alist[positionOfMax] = temp

Anche se c'è solo uno scambio per scansione, il selection sort compir lo stesso numero di confronti del
bubble sort; quindi la sua complessità temporale è \\(O(n^2)\\).

La figura seguente mostra il processo di ordinamento. 
Per ogni passo l'elemento più grande fra i rimanenti è selezionato e posto nella sua posizione corretta.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura62.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Sezione 2.3 Insertion sort
==========================

L'*insertion sort* crea una sottolista ordinata (inizialmente con un solo elemento) nelle posizioni
iniziali della lista, e inserisce in questa sottolista un nuovo elemento, creando una nuova sottolista
ancora ordinata.
Partendo da una lista con un solo elemento (il primo della lista), ogni oggetto dal secondo fino 
all'ultimo è confrontato con quelli già presenti nella sottolista ordinata; 
gli oggetti più grando sono shiftati a destra, e lo'ggetto corrente è inserito nella sottolista
quando un elemento più piccolo è raggiunto.

Il codice Python per l'insertion sort è scritto di seguito::

 1  def insertionSort(alist):
 2    for index in range(1,len(alist)):
 3      currentvalue = alist[index]
 5      position = index
 6      while position > 0 and alist[position-1] > currentvalue:
 8        alist[position]=alist[position-1]
 9        position = position-1
 10     alist[position]=currentvalue

Per ordinare \\(n\\) elementi si compiono \\(n-1\\) scansioni.
Il numero massimo dei confronti per l'insertion sort è, ancora una volta, la somma dei primi
\\(n-1\\) interi, quindi \\(O(n^2)\\).
Nel caso migliore solo un confronto deve essere fatto per ogni scansione.
Questo si verifica quando la lista è già ordinata.

La seguente figura mostra il processo di ordinamento.
Gli oggetti ombreggiati rappresentano le sottoliste ordinate per ogni scansione.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura63.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Sezione 2.4 Merge sort
======================

Il *merge sort* è un algoritmo ricorsivo.
Se la lista è vuota o ha un solo elemento, allora è ordinata (caso base).
Se la lista ha più di un elemento, allora è divisa in due e il merge sort è richiamato ricorsivamente
su entrambe le metà. Quando le due metà sono ordinate è realizzata una fusione appropriata 
delle due sottoliste in una singola lista ordinata.

Questo algoritmo è un esempio della strategia *divide and conquer*.
Nel passo *divide*, quando la dimensione dell'input è più piccola di una data soglia, 
la soluzione può essere fornita direttamente; altrimenti, l'input è suddiviso in sottoinsiemi disgiunti.
Nel passo *conquer*, i sottoinsiemi associati con i sottoinsiemi sono risolti ricorsivamente.
Nel passo *combine*, le soluzioni dei sottoproblemi sono unite per ottenere una soluzione al problema 
originario.

L'algoritmo generale per il merge sort è scritto in seguito, senza considerare se la sequenza \\(S\\) 
è implementata co nun array o con una lista concatenata:

- Divide: se \\(S\\) ha zero o un elemento, restituire \\(S\\);
  altrimenti, dividere\\(S\\) a metà e inserire gli elementi in due sequenze, \\(S_1\\) e \\(S_2\\).
- Conquer: ordinare ricorsivamente \\(S_1\\) e \\(S_2\\).
- Combine: reinserire gli elementi in \\(S\\) fondendo le due sequenze ordinate \\(S_1\\) e \\(S_2\\) in una unica sequenza ordinata.

Il codice Python per una sequenza basata su array è dato in seguito::

 1  def mergeSort(alist):
 2    print("Splitting ",alist)
 3    if len(alist) > 1:
 4      mid = len(alist)//2
 5      lefthalf = alist[:mid]
 6      righthalf = alist[mid:]
 7
 8      mergeSort(lefthalf)
 9      mergeSort(righthalf)
 10
 11     i=0
 12     j=0
 13     k=0
 14     while i < len(lefthalf) and j < len(righthalf):
 15       if lefthalf[i] <= righthalf[j]:
 16         alist[k] = lefthalf[i]
 17         i=i+1
 18       else:
 19         alist[k] = righthalf[j]
 20         j=j+1
 21       k=k+1
 22
 23     while i < len(lefthalf):
 24       alist[k]=lefthalf[i]
 25       i=i+1
 26       k=k+1
 27
 28     while j < len(righthalf):
 29       alist[k]=righthalf[j]
 30       j=j+1
 31       k=k+1
 32   print("Merging ",alist)

La chiamata ricorsiva della funzione ``mergesort`` è fatta sulle metà destra e sinistra della lista,
assumendo che esse siano state ordinate. 
Il resto della funzione fonde le due sottoliste ordinate in una lista ordinata.
Ciò è ottenuto spostando gli elementi delle due liste in ``alist`` uno alla volta,
prendendo ogni volta il più piccolo dalle liste ordinate.

In figura si osserva il processo di divisione e fusione, rispettivamente.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura64.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura65.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Per analizzare la complessità dell'algoritmo, consideriamo che la lista è divisa ricorsivamente in due metà:
questo processo può essere ripetuto per \\(\\log n\\) volte, se la lista è lunga \\(n\\).
Ogni coppia di sottoliste è fusa in una singola lista, e questo richiede \\(n\\) operazioni,
poiché ogni oggetto nelle sottoliste deve essere processato. 
Quindi, il costo totale dell'algoritmo è \\(O(n \\log n)\\).
Il merge sort richiede spazionper immagazzinare le sottoliste estratte a ogni passo.
Questa richiesta addizionale rende questo algoritmo critico quando applicato a grandi data set.


Sezione 2.5 Quicksort
=====================

Il *quick sort* usa l'approccio divide and conquer ma, rispetto al merge sort, non usa spazio addizionale
per immagazzinare le sottoliste. 
Come trade-off, può capitare che le due sottoliste non abbiano la stessa lunghezza, causando un rallentamento
dell'algoritmo. 
Il quick sort comincia selezionando un valore, il *pivot*, che è il primo elemento della lista, solitamente;
la sua corretta posizione nella lista è trovata, e questa posizione  è usata nel processo
di divisione della lista in due sottoliste. Il quick sort è richiamato ricorsivamente sulle due sottoliste.

Più precisamente, una volta che il pivot è selezionato sono definiti due marcatori (*leftmark* e *rightmark*) 
come la prima e l'ultima posizione degli oggetti rimanenti nella lista, rispettivamente.
Leftmark è incrementato fino a che un valore più grande del pivot è trovato;
quindi, rightmark è decrementato fino a che un valore più piccolo del pivot è trovato.
I due valori sono ovviamente fuori posto, e quinsi sono scambiati.

Il processo continua fino a che rightmark diventa minore di leftmark; a questo punto abbiamo
trovato lo *split point*. Il valore delpivot è scambiato con quello dello split point, e quindi il pivot
è adesso nella sua giusta posizione. Nella figura seguente è trovato lo split point corretto per 54,
e i due valori sono scambiati.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura66.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Si noti che gli oggetti alla sinistra dello split point sono minori del valore del pivot,
e gli oggetti alla destra dello split point sono maggiori del valore del pivot.
La lista è divisa in corrispondenza dello split point e il quicksort è invocato ricorsivamente 
sulle due sottoliste.

La funzione quicksort è definita come segue::

 1  def quickSort(alist,first,last):
 2    if first < last:
 3      splitpoint = partition(alist,first,last)
 4      quickSort(alist,first,splitpoint-1)
 5      quickSort(alist,splitpoint+1,last)
 6
 7  def partition(alist,first,last):
 8    pivotvalue = alist[first]
 9    leftmark = first+1
 10   rightmark = last
 11
 12   done = False
 13   while not done:
 14     while leftmark <= rightmark and alist[leftmark] <= pivotvalue:
 15       leftmark = leftmark + 1
 16     while leftmark <= rightmark and alist[rightmark] >= pivotvalue:
 17       rightmark = rightmark -1
 18
 19     if rightmark < leftmark:
 29       done = True
 20     else:
 21       temp = alist[leftmark]
 22       alist[leftmark] = alist[rightmark]
 23       alist[rightmark] = temp
 24
 25   temp = alist[first]
 26   alist[first] = alist[rightmark]
 27   alist[rightmark] = temp
 28   return rightmark

Se la divisione si verifica sempre a metà della lista, ci saranno ancora  \\(\\log n\\) divisioni, 
per una lista lunga \\(n\\).
Per trovare lo split point, ognuno degli  \\(n\\) oggetti deve essere confrontato con il valore del pivot.
Ciò implica che i passi totali sono \\(O(n \\log n)\\). 
Inoltre, non è usata memoria addizionale.

Gli split point non sono sempre a metà della lista;
nel caso peggiore, il partizionamento divide una lista di \\(n\\) elementi in una lista di \\(1\\) 
elemento e una lista di \\(n-1\\) elementi.  Quindi, la lista di \\(n-1\\) elementi è divisa un 
una lista di \\(1\\) elemento e una lista di \\(n-2\\) elementi, rispettivamente.
Ciò porta a \\(O(n^2)\\) passi.



Sezione 3. Ordinamento in tempo lineare
---------------------------------------

Si dimostra che il tempo \\(O(n \\log n)\\) è necessario per ordinare una sequenza di \\(n\\) elementi
usando algoritmi basati su confronto.
Una domanda naturale è se esistono algoritmi di ordinamento più veloci;
la risposta è sì, ma essi richiedono speciali restrizioni sulla sequenza da ordinare.

Sezione 3.1 Bucket sort
=======================

Data una sequenza\\(S\\) di \\(n\\) elementi, le cui chiavi siano interi nell'intervallo \\([0,N -1]\\),
per \\(N \\geq 2\\), si supponga che \\(S\\) debba essere ordinato in base ai valori delle chiavi.
In questo caso è possibile scrivere un algoritmo per ordinare \\(S\\) in tempo \\(O(n+N)\\).
Quindi possiamo ottenere una complessità temporale lineare per un algoritmo di ordinamento;
il risultato dipende dalla restrizione imposta.

Il bucket sort non è basato su confronti, ma sull'uso delle chiavi come indici di un *bucket array* \\(B\\)
che ha celle indicizzate da \\(0\\) to \\(N-1\\). Un elemento con chiave \\(k\\) è messo nel bucket \\(B[k]\\),
che a sua volta è una sequenza di elementi con chiave \\(k\\).
Dopo che ogni elemento della sequenza di input \\(S\\) è stata messa nel suo bucket, i valori sono riportati
in \\(S\\) in modo ordinato, semplicemente enumerando il contenuto del bucket \\(B[0], B[1], \\ldots, B[N -1]\\).
Lo pseudocodice per il bucket sort è quello che segue::

 1  BucketSort(S):
 2  Input: sequence S of entries with integer keys in the range [0,N-1]
 3  Output: sequence S sorted in increasing order of the keys
 4  B is an array of N sequences, initially empty
 5
 6  for each entry e in S do
 7    k = the key of e
 8    remove e from S and insert it at the end of bucket B[k]
 9  for i = 0 to N-1 do
 10   for each entry e in sequence B[i] do
 11     remove e from B[i] and insert it at the end of S

Il bucket sort è eseguito in tempo \\(O(n+N)\\) e usa la stessa quantità di spazio;
ciò significa che esso è efficiente solo se l'intervallo \\(N\\) dei valori è piccolo in confronto
alla dimensione \\(n\\) della sequenza, ad esempio \\(N = O(n)\\) o \\(N = O(n \\log n)\\).
Una importante proprietà del bucket sort è che esso lavora correttamente anche se ci sono più
elementi con la stessa chiave. In particolare, ha la proprietà di essere *stabile*.

Sia \\(S = ((k_0,v_0), \\ldots , (k_{n-1},v_{n-1}))\\) una sequenza di elementi;
un algoritmo di ordinamento è *stabile* se, per ogni coppia \\((k_i,v_i)\\) e \\((k_j ,v_j)\\) di
\\(S\\) tale che \\(k_i = k_j\\), e \\((k_i,v_i)\\) precede \\((k_j ,v_j)\\) in \\(S\\) prima dell'ordinamento,
abbiamo che \\((k_i,v_i)\\) precede \\((k_j ,v_j)\\) dopo l'ordinamento.
La stabilità è importante per un algoritmo di ordinamento poiché varie applicazione potrebbero necessitare
di mantenere l'ordine iniziale degli elementi con la stessa chiave.

La descrizione informale del bucket sort data in precedenza garantisce la stabilità se ci assicuriamo che
tuttele sequenze agiscano come code, con gli elementi processati e rimossi dalla testa di una sequenza
e inseriti in fondo.

Sezione 3.2 Radix sort
======================

Si consideri il caso generale in cui vogliamo ordinare liste le cui chiavi sono coppie \\( (k, l)\\),
con \\(k\\) e \\(l\\) interi nell'intervallo \\([0,N-1]\\), per \\(N \\geq 2\\).
Un ordine di queste chiavi è quello lessicografico, dove 
\\((k_1, l_1) < (k_2, l_2)\\) se \\(k_1 < k_2\\) o se \\(k_1 =k_2\\) e \\(l_1 < l_2\\).

Il radix sort ordina una sequenza \\(S\\) applicando il bucket sort due volte: la prima, usando una 
componente della coppia come chiave durante il processo di ordinamento, e la seconda, usando l'altra componente.
Si ha un problema: quale scelta è quella corretta? Cioè, dobbiamo ordinare rispetto alla prima
componente e poi rispetto alla seconda, o viceversa?

Ad esempio, si consideri la sequenza
\\(S = ((3,3), (1,5), (2,5), (1,2), (2,3), (1,7), (3,2), (2,2))\\).
Se ordiniamo \\(S\\) rispetto alla prima componente si ottiene la sequenza
\\(S_1 = ((1,5), (1,2), (1,7), (2,5), (2,3), (2,2), (3,3), (3,2))\\).
Se ora ordiniamo \\(S_1\\) usando la seconda componenete otteniamo la sequenza
\\(S_{1,2} = ((1,2), (2,2), (3,2), (2,3), (3,3), (1,5), (2,5), (1,7))\\),
che chiaramente non è ordinata. Ripetendo il procedimento sulla seconda e sulla prima componente,
rispettivamente, otterremmo la sequenza finale 
\\(S_{2,1} = ((1,2), (1,5), (1,7), (2,2), (2,3), (2,5), (3,2), (3,3))\\),
che è ordinata lessicograficamente.

Il risultato può essere esteso al caso generale; ordinare la sequenza prima rispetto alla seconda componente
e poi rispetto alla prima garantisce che se due elementi sono uguali nel secondo ordinamento,
allora il loro ordine relativo nella sequenza di partenza è preservato.

In generale, data una sequenza \\(S\\)  di \\(n\\) coppie, ciascuna delle quali ha la forma 
\\((k_1,k_2, \\ldots ,k_d)\\), con \\(k_i\\) intero nell'intervallo \\([0,N -1]\\), per \\(N \\geq 2\\),
\\(S\\) può essere ordinato in tempo \\(O(d(n+N))\\), usando il radix sort.
Esso può essere applicato a ogni elemento composto. Ad esempio, per ordinare stringhe di caratteri di lunghezza
moderata, poiché ogni carattere può essere rappresentato con un valore intero.


Sezione 4. Il miglior algoritmo di ordinamento
----------------------------------------------

Esistono numerosi algoritmi per ordinare una sequenza. Fra essi, l'insertion, bubble o selection sort
hanno il caso medio e il caso peggiore in \\(O(n^2)\\), rendendoli poco adatti a essere usati in situazioni reali.
Altri metodi, come il merge e il quick sort, hanno complessità temporale \\(O(n \\log n)\\) 
(che è anche la complessità migliore che si possa raggiungere per l'ordinamento).
Con chiavi specializzate esistono algoritmi che lavorano in tempo lineare, come il bucketsort e il radixsort.

In generale ci sono vari fattori da considerare nella valutazione della bontà di un algoritmo;
la complessità temporale è importante, ma c'è sempre un trade-off fra efficienza, uso della memoria
e stabilità. Ad esempio, il tempo di esecuzione dell'insertion sort potrebbe essere \\(O(n+m)\\), 
con  \\(m\\) il numero di inversioni (il numero di coppie non ordinate) nella sequenza.
Sequenze con un basso numero di inversioni possono essere ordinate efficientemente.

Per il quick sort, la sua complessità è  \\(O(n \\log n)\\) se la lista è partizionata in due sottoliste
dii uguale lunghezza; sfortunatamente non possiamo garantire che ciò accada ogni volta, e il caso peggiore
rimane sempre \\(O(n^2)\\); inolte il quick sort non è un metodo stabile.
malgrado tutti questi problemi esso è considerato coma la scelta migliore per un algoritmo enenrale;
ad esempio, è usato nelle librerie del C, nel sistema operativo Unix e in numerose vesioni di Java.

Per il merge sort il caso peggiore è \\(O(n \\log n)\\), e non è un metodo *in-place*,
a causa della memoria supplementare richieta per allocare gli array temporanei e per effettuare
le copie fra array. Il merge sort non è attraente se confrontato con un algoritmo in-place come il quick sort,
ma è una opzione eccellente quando l'input è stratifica to attraverso vari livelli della gerarchia di memoria del computer
(cache, main memory, external memory). 
In questo caso il modo in cui esso processa i dati in lunghe sequenze da fondere fa l'uso migliore dei dati portati
come blocchi attraverso i livelli della memoria, riducento il numero totale dei trasferimenti.
Recenti versioni di Linux usamo un merge sort multiway.
Il metodo standard di ordinamento di Python per la classe lista e  uello per gli array in Java 7
è essenzialmente un merge sort.

Per concludere, se dobbiamo ordinare sequenze di piccoli interi o caratteri, il bucket sort o il radix sort
sono una scelta eccellente, poiché sono eseguiti in tempo \\(O(d(n+N))\\), con \\([0,N-1]\\) l'intervallo
degli interi (e \\(d = 1\\) per il bucket sort). Se \\(d(n+N)\\) è più piccolo della funzione 
\\(n \\log n\\), allora questi metodi lavorano più velocemente degli altri.


Sezione 5. Selezione
--------------------

La *selezione* è il problema di trovare il \\(k\\)-mo più piccolo elemento in una collezione non ordinata
di \\(n\\) elementi.
Una soluzione immediata consiste nell'ordinare la collezione e indicizzare la sequenza ottenuta a\\(k-1\\).
Ciò richiede tempo \\(O(n \\log n)\\).

Il problema può essere risolto in tempo \\(O(n)\\) con una tecnica che si chiama *prune-and-search*.
In questo caso un problema definito su  una collezione di \\(n\\) elementi è risolto eliminando una 
parte degli elementi, e risolvendo ricorsivamente il problema più piccolo.
Quando il problema è ridotto a una collezione di oggetti di dimensione costante, allora esso
può essere risolto per mezzo di qualche metodo diretto.
Risalire lungo tutte le chiamate ricorsive completa la soluzione.
Ad esempio, la *binary search* è un esempio di aplicazione della tecnica prune-and-search.

Il *randomized quick-select* è una applicazione dello stesso metodo per risolvere il problema della
selezione. Data una sequenza non ordinata \\(S\\) di \\(n\\) elementi confrontabili, e un intero
\\(k\\) in \\([1,n]\\),
si prende casualmente un elemento pivot da \\(S\\) e lo si usa per dividere \\(S\\) in
tre sottosequenze \\(L\\), \\(E\\), e \\(G\\), 
che contengono gli elementi di \\(S\\) minori di, uguali a, o maggiori del pivot, rispettivamente.
Nel passo di *prune*, si trova quale dei tre sottoinsiemi contiene l'elemento desiderato,
basandosi sulvalore di \\(k\\) e sulla dimensione dei sottoinsiemi.
La ricorsione è applicata al sottoinsieme appropriato.

Una implementazione è illustrata di seguito::

 1  def quickselect(S, k):
 2    # returns the k-th smallest element of S, for k from 1 to len(S)
 3    if len(S) == 1:
 4      return S[0]
 5    pivot = random.choice(S)            # pick random pivot element from S
 6    L = [x for x in S if x < pivot]     # elements less than pivot
 7    E = [x for x in S if x == pivot]    # elements equal to pivot
 8    G = [x for x in S if pivot < x]     # elements greater than pivot
 9    if k <= len(L):
 10     return quickselect(L, k)     # the k-th smallest is in L
 11   elif k <= len(L) + len(E):
 12     return pivot                 # the k-th smallest is equal to pivot
 13   else:
 14     j = k-len(L)-len(E)
 15     return quickselect(G, j)     # the k-th smallest is the j-th in G

Per mezzo di argomenti probabilistici si prova che questo algoritmo è eseguito mediamente in tempo \\(O(n)\\),
quale che sia la scelta del pivot. Nel caso peggiore esso è eseguito in tempo \\(O(n^2)\\).
