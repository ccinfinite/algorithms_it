

Capitolo Quattro: pile, code, code a due vie - esercizi e sfide
**************************************************************



Sezione 1. Revisione della teoria
--------------------------------

**Esercizio 1.1**

Completare ed eseguire i seguenti frammenti di codice (che sono anche nel Capitolo Quattro),
in cui un ADT pila è implementato per mezzo di una classe lista di Python.
Mostrare qualche esempio di come lavora la pila, e aggiungere l'eccezione ``Empty`` che è sollevata quando
sono chiamati ``pop`` o ``top`` su una pila vuota.

.. activecode:: stack-as-a-list
   :language: python
   :caption: Implementation of a stack by means of a list

   class ArrayStack:
   # LIFO Stack implementation using a Python list as storage

     # Creates an empty stack
     def __ init __ (self):
       self._data = [ ]

     # Returns the number of elements in the stack
     def __ len __ (self):
       return len(self._data)

     # Returns True if the stack is empty
     def is_empty(self):
       return len(self._data) == 0

     # Adds element e to the top of the stack
     def push(self, e):
       self._data.append(e)

     # Returns (but don't remove) the element at the top of the stack
     # raising an exception if the stack is empty
     def top(self):
       if self.is_empty( ):
         raise Empty( Stack is empty )
       return self._data[−1]

     # Remove and return the element from the top of the stack
     # raising an exception if the stack is empty
     def pop(self):
       if self.is_empty( ):
         raise Empty( 'Stack is empty' )
       return self._data.pop( )

           # INSERT NEW CODE HERE


**Esercizio 1.2**

Scrivere una semplice implementazione per una coda, adattando una lista di Python;
mostrare qualche esempio di come lavora la coda (nella Sezione 2.1 del Capitolo Quattro si possono trovare
dei suggerimenti: se è chiamata una ``pop(0)`` per rimuovere in primo elemento della lista, allora tutti i 
rimanenti elementi devono essere shiftati a sinistra).

.. activecode:: queue-as-a-list
   :language: python
   :caption: Implementation of a queue by means of a list

     # INSERT NEW CODE HERE


Poi, scrivere una versione più efficiente basata su un array circolare; mostrare qualche esempio di funzionamento.

.. activecode:: queue-as-a-circular-array
   :language: python
   :caption: Implementation of a queue by means of a circular array

   class ArrayQueue:
   # queue implemented with a list
     DEFAULT = 10          # capacity of all queues

     def __init__(self):
       self._data = [None]*ArrayQueue.DEFAULT
       self._size = 0
       self._front = 0

     def __len__(self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def first(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       return self._data[self._front]

     def dequeue(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       element = self._data[self._front]
       self._data[self._front] = None
       self._front = (self._front + 1) % len(self._data)
       self._size −= 1
       return element

     def enqueue(self, e):
       if self._size == len(self._data):
         self._resize(2*len(self.data))           # double the array size
      position = (self._front + self._size) % len(self._data)
      self._data[position] = e
      self._size += 1

     def _resize(self, capacity):
       old = self._data                   # keeps track of existing list
       self._data = [None]*capacity       # allocate list with new capacity
       walk = self._front
       for k in range(self._size):
         self._data[k] = old[walk]        # shifts the indices
         walk = (1 + walk) % len(old)     # use old size
       self._front = 0                    # realigs front

          # INSERT NEW CODE HERE


**Esercizio 1.3**

Scrivere una implementazione semplice di una coda a due vie (deque), adattando una lista di Python;
mostare qualche esempio di funzionamento.

.. activecode:: deque-as-a-list
   :language: python
   :caption: Implementation of a deque by means of a list

     # INSERT NEW CODE HERE




Sezione 2. Problemi su pile, code, deque
----------------------------------------

In questa sezione affronteremo alcuni problemi classici che possono essere risolti usando pile, code e deque.
Studiare ogni esercizio, verificarli e completarli (se necessario), ed eseguire i codici fornit.

**Esercizio 2.1**

Scrivere un algoritmo che converte numeri interi decimali (in base 10) in numeri binari (in base 2).

La soluzione a questo problema usa una pila per tenere traccia delle cifre delle soluzioni binarie parziali.
Si comincia con un numero decimale maggiore di 0.
Poi si itera la divisione  del numero per 2, e si inserisce il resto (0 o 1) nella pila.
Ciò significa che stiamo costruendo il numero binario come una sequenza di cifre;
il primo resto che calcoliamo sarà l'ultima cifra della sequenza.
Alla fine dell'iterazione, le cifre binarie sono estratte dalla pila e aggiunte alla destra di una stringa,
che sarà la rappresentazione in base 2 del numero.

Verificare, completare ed eseguire il seguente ActiveCode.

.. activecode:: decimal-binary-convertion
   :language: python
   :caption: Convertion of a decimal number into binary representation

   from pythonds.basic import Stack

   def dectobin(decnumber):
     remstack = Stack()
     while decnumber > 0:
       remstack.push(decnumber % 2)
       decnumber = decnumber // 2

     binnumber = ""
     while not remstack.isEmpty():
       binnumber = binnumber + str(remstack.pop())
     return binnumber

   print(dectobin(51))




**Esercizio 2.2**

Unnpalindromo è una stringa che può essereletta allo stesso modo in avanti o al contrario
(per esempio, radar, madam, o madam I'm Adam).
Scrivere un algorimo che verifica che una stringa di caratteri sia palindroma.

La soluzione usa una deque per immagazzinare i caratteri della stringa.
Aggiungiamo ogni nuovo carattere in fondo alla deque;
poi, confrontiamo la testa della deque (il primo carattere della stringa) e il fondo della deque
(l'ultimo carattere della stringa), e continuiamo se essi sono uguali.
Infine, i caratteri finiscono o c'è solo un carattre nella deque, in base alla lunghezza della stringa
(pari o dispari). In entrambi i casi la stringa deve essere un palindromo.

Verificare, completare ed eseguire il seguente codice.

.. activecode:: palindrome-checker
   :language: python
   :caption: Checking whether a string is a palindrome or not

   from pythonds.basic import Deque

   def palcheck(aString):
     word = Deque()
     for c in aString:
       word.addRear(c)

     equal = True
     while word.size() > 1 and equal:
       first = word.removeFront()
       last = word.removeRear()
       if first != last:
         equal = False
     return equal

   print(palcheck("lsdkjfskf"))
   print(palcheck("radar"))



**Esercizio 2.3**

La *notazione postfissa* è un modo non ambiguo di scrivere una espressione aritmetica senza usare le
parentesi. Se *(exp1) op (exp2)* è una *espressione infissa* il cui operatore è *op*,
la versione postfissa è *pexp1 pexp2 op*, con *pexp1* la versione postfissa di *exp1* 
e *pexp2* la versione postfissa di *exp2*.
Ad esempio, la versione postfissa di \\( ((3+2) ∗ (5−3))/2\\) è \\(3 2 + 5 3 − ∗ 2 / \\).

Per fornire un algoritmo che valuta una espressione in forma postfissa,
si noti che se un operatore è in input, i due operandi più recenti devono essere usati nella valutazione.
Ad esempio, si consideri l'espressione \\(3 2 5 * +\\) (che rappresenta \\( 3 + 2 * 5 \\) ).

Scandendo l'espressione da sinistra a destra, si incontrano gli operandi 3, 2, and 5,
che sono collocati in una pila, rendendoli disponibili per il calcoli futuri.
Se si incontra un operatore (``*``, in questo esempio) esso deve essere applicato ai due più recenti
operandi. Effettuando due pop sulla pila,possiamo ottenere gli operandi appropriati e moltiplicarli
(ottenendo 10 in questo caso).
Possiamo inserire il risultato nella pila in modo che esso possa essere utilizzato come operando per
altre operazioni successive.
Quando è processato l'operatore finale, ci sarà solo un valore nella pila, quello dell'espressione.
Di seguito mostriamo uno pseudocodice per questo algoritmo::

 Create an empty stack S
 Scan the expression from left to right
     if the element is an operand
       push it onto S;
     if the element is an operator, *, /, +, or -
       pop from S twice
       perform the arithmetic operation
       push the result back onto S
 When the expression ends, the result is on the stack.

Completare e eseguire il seguente codice, in cui la definizione di una funzione per la valutazione di una espressione
postfissa è parzialmente scritta.
La funzione ``doMath`` prende due operandi e un operatore ed esegue l'appropriata operazione aritmetica.

.. activecode:: postfix-evaluation
   :language: python
   :caption: Evaluation of a postfix expression using a stack

   from pythonds.basic import Stack

   def postfixEval(postfixExpr):
     operandStack = Stack()
     tokenList = postfixExpr.split()

     for token in tokenList:
       if token in "0123456789":
         operandStack.push(int(token))
       else:
         operand2 = operandStack.pop()
         operand1 = operandStack.pop()
         result = doMath(token,operand1,operand2)
         operandStack.push(result)
     return operandStack.pop()

   def doMath(op, op1, op2):
     if op == "*":
       return op1 * op2
     elif op == "/":
       return op1 / op2
     elif op == "+":
       return op1 + op2
     else:
       return op1 - op2

   print(postfixEval('7 8 + 3 2 + /'))




Sezione 3. Esercizi e autovalutazione
--------------------------------------

**Esercizio 3.1**

Quali valori sono restituiti se la seguente serie di operazioni e eseguita su una pila vuota?
``push(5)``, ``push(3)``, ``pop()``, ``push(2)``, ``push(8)``, ``pop()``, ``pop()``,
``push(9)``, ``push(1)``, ``pop()``, ``push(7)``, ``push(6)``, ``pop()``, ``pop()``, ``push(4)``, ``pop()``, ``pop()``.
(Suggerimento: usare una delle implementazioni già studiate)

**Esercizio 3.2**

Qual è la dimensione di una pila S, inizialmente vuota, dopo l'esecuzione di 25 ``push``,
12 ``top``, e 10 ``pop``?
(Suggerimento: usare una delle implementazioni già studiate)

**Esercizio 3.3**

Scrivere la funzione ``transfer(S,T)`` che trasferisce gli elementi da S a T in modo che l'elemento al top di S
sia il primo ad essere inserito in T, e l'elemento in fondo a S diventi il top di T.
(Suggerimento: trasferire un elemento alla volta)

**Esercizio 3.4**

Scrivere un metodo ricorsivo che rimuove tutti gli elementi da una pila.

**Esercizio 3.5**

Scrivere una funzione che rovesci una lista di elementi.

**Esercizio 3.6**

Che valori sono restituiti se la serie di operazioni seguenti è eseguita su una coda vuota?
``enqueue(5)``, ``enqueue(3)``, ``dequeue()``, ``enqueue(2)``, ``enqueue(8)``, ``dequeue()``, ``enqueue(9)``,
``enqueue(1)``, ``dequeue()``, ``enqueue(7)``, ``enqueue(6)``, ``dequeue()``, ``dequeue()``,
``enqueue(4)``, ``dequeue()``, ``dequeue()``.
(Suggerimento: usare una delle implementazioni già studiate)

**Esercizio 3.7**

Qual è la dimensione di una coda Q, inizialmente vuota, dopo l'esecuzione di 32 ``enqueue``,
10 ``first``, e 15 ``dequeue``
(Suggerimento: usare una delle implementazioni già studiate)

**Esercizio 3.8**

Che valori sono restituiti durante la seguente sequenza di operazioni suuna deque vuota?
``add_first(4)``, ``add_last(8)``, ``add_last(9)``, ``add_first(5)``, ``back()``, ``delete_first( )``,
``delete_last( )``, ``add_last(7)``, ``first( )``, ``last( )``, ``add_last(6)``, ``delete_first( )``, ``delete_first( )``.
(Suggerimento: usare una delle implementazioni già studiate)

**Esercizio 3.9**

Una deque D contiene i numeri (1,2,3,4,5,6,7,8), in questo ordine, e la coda Q è inizialmente vuota.
Scrivere un codice che usi solo D e Q e che immagazzini i numeri in D come (1,2,3,5,4,6,7,8).
(Suggerimento: usare il valore di ritorno di un metodo di rimozione come parametro per un metodo di inserimento)

**Esercizio 3.10**

Ripetere l'esercizio precedente usando una pila S invece della coda Q.

**Esercizio 3.11**

Given three distinct integers into a stack S, in random order,
write a piece of pseudo-code (with no loops or recursion) that uses only one comparison
and only one variable x, and that results in variable x storing the largest of the three integers.
(Hint: Pop the top integer, and remember it)

**Esercizio 3.12**

Given three nonempty stacks R, S, and T, describe a sequence of operations that results in S storing all elements
originally in T below all of S’s original elements, with both sets of elements in their original order.
For example, if R = [1,2,3], S = [4,5], and T = [6,7,8,9], initially,
the final configuration should have S = [6,7,8,9,4,5].
(Hint: Use R as temporary storage, without popping its original contents)

**Esercizio 3.13**

Descrivere come implementare una pila usando una sola coda come variabile di istanza, e solo memoria
locale costante.
(Suggerimento: ruotare gli elementi nella coda)

**Esercizio 3.14**

Descrivere come implementare una coda usando solo due pile come variabili di istanza.
(Suggerimento: usare una pila per collezionare gli elementi in ingresso, e l'altra come buffer per gli
 elementi in uscita)

**Esercizio 3.15**

Descrivere come implementare una coda a due vie usando due pile.
(Hint: usare una pile per ogni capo della deque)

**Esercizio 3.16**

Data una pila S che contiene \\(n)\\ elementi e una coda Q, inizialmente vuota,
mostrare come usare Q per scansionare S per vedere se contiene un certo elemento \\(x\\);
l'algoritmo deve matenere gli elementi di S nel loro ordine originario.

**Esercizio 3.17**

Implementare completamente ``ArrayDeque``.

**Esercizio 3.18**

Le pile possono essere usare per fornire il supporto per la funzione "undo" in applicazioni come un web browser
o un editor di testo. Questa funzionalità può essere realizzata con una pila illimitata, ma molte applicazioni
la supportano con una pila di capacità fissata, come segue:
se  ``push`` è chiamato su una pila completamente piena, il nuovo elemento è inserito al top, e l'elemento più vecchio
è eliminato dal fondo della pila.
Implementare questa versione di ``Stack``, usando un array circolare.