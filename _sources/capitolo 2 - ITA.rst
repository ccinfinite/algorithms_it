
Capitolo Due: Ricorsione
************************

Sezione 1. Introduzione
-----------------------

Cicli e iterazioni (come i cicli for- e while-) sono il modo comune di implementare il concetto di ripetizione 
di un compito in Python. Un modo alternativo, più aderente alla natura stessa delle funzione, è la ricorsione.

Il termine *ricorsivo* deriva dal verbo latino *recurrere*, che vuol dire *ricorrere*, *ritornare*.
Il più usato ed abusato esempio di definizione ricorsiva è quello della funzione fattoriale, che può essere
definita come \\(n! = n * (n-1)!\\), se \\(n > 1\\), e \\(0! = 1\\). 
Ad esempio, \\(4!=4*3!=4*3*2!=4*3*2*1!=4*3*2*1*0!=24\\).
I pattern frattali sono ricorsivi. 
Le bambole Matryoshka sono ricorsive. 
Ogni bambola è fatta di legno, oppure contiene un'altra bambola.

In generale, una funzione (o un programma) è ricorsivo se chiama se stesso nel suo corpo, 
restituendo il valore di questa chiamata.
Ogni chiamata deve essere appicata a una vrsione più piccola dell'input, 
fino a raggiungere un caso base (in cui la funzione può essere calcolata senza ulteriori chiamate ricorsive).
Nell'esempio precedente, quando il calcolo rggiunge in caso 
\\(0!\\), il valore \\(1\\) è restituito alla chiamata precedente.

La definizione del fattoriale in Python è la seguente::

 1  def factorial(n):
 2    if n == 0:
 3      return 1
 4    else:
 5      return n * factorial(n-1)

Come il lettore può osservare, ``factorial(n)`` chiama ``factorial(n-1)``, e così via, finché il caso base 
\\(0!\\) è raggiunto.
La funzione non usa cicli espliciti, e la ripetizione è garantita dalle chiamate ricorsive di ``factorial``.
Ogni volta che la funzione è invocata il suo argomento è decrementato, e quando il caso base è raggiunto l'intero
\\(1\\) è restituito.

Per seguire il comportamento della ricorsione aggiungiamo alcune ``print()`` al codice::

 1  def factorial(n):
 2    print("factorial is called with n = " + str(n))
 3    if n == 1:
 4      return 1
 5    else:
 6      res = n * factorial(n-1)
 7      print("intermediate result for ", n, " * factorial(" ,n-1,"): ",res)
 8      return res
 9    print(factorial(4))

Questo script ha il seguente risultato, per input 4:

| factorial is called with n = 4
| factorial is called with n = 3
| factorial is called with n = 2
| factorial is called with n = 1
| intermediate result for 2 * factorial( 1 ): 2
| intermediate result for 3 * factorial( 2 ): 6
| intermediate result for 4 * factorial( 3 ): 24
| 24


Il programma iterativo che calcola la stessa funzione è::

 1  def iterfactorial(n):
 2    res == 1:
 3    for i in range(2,n+1):
 4      res *= i
 5    return res

Alcuni linguaggi di programmazione (come Scheme o Smalltalk) non supportano i costrutti per i cicli, e si
affidano completamente alla ricorsione per esprimere la ripetizione. La maggior parte dei linguaggi supporta
la ricorsione usando lo stesso meccanismo usato per supportare la chiamata di funzione:
se la chiamata della funzione è ricorsiva, le chiamate precedenti sono sospese finché la chiamata corrente
è completata. 

In Python (e nella maggior parte dei linguaggi di programmazione), ogni volta che una funzione è invocata
è creata una struttura chiamata *record di attivazione* o *frame*, con lo scopo di immagazzinare
informazioni sulla chiamata stessa. Il record di attivazione tiene traccia dei parametri di chiamata,
delle variabili locali, e di quale comando nel corpo della funzione è attualmente eseguito.
Quando l'esecuzione di una funzione porta alla chiamata di un'altra funzione, l'esecuzione della prima
è sospesa, e il suo record di attivazione immagazzina il punto nel codice sorgente 
a cui il controllo deve tornare dopo l'esecuzione della seconda funzione. 
Questo procedimento è utilizzato sia nel caso comune di una funzione che ne invochi un'altra diversa, sia
nel caso ricosivo in cui una funzione invoca se stessa. 
Quindi esiste un diverso record di attivazione per ogni chiamata attiva della stessa funzione.

La ricorsione è di fondamentale importanza nello studio delle strutture dati e degli algoritmi.


Sezione 2. Alcuni esempi di algoritmi ricorsivi
-----------------------------------------------

Sezione 2.1 Ricerca binaria
===========================

La ricerca binaria è un algoritmo ricorsivo che trova un valore target all'interno di una sequenza ordinata 
di \\(n\\) elementi.
La sequenza può essere, in Python, una generica sequenza indicizzabile, come una lista o un array.
L'approccio banale per ricercare un valore all'interno di una sequenza *non* ordinata è quello di usare
l'algoritmo di *ricerca sequenziale*.
Un ciclo è usato per esaminare ogni elemento, finché il target è trovato o la sequenza è esaurita.
L'algoritmo è eseguito in tempo \\(O(n)\\) (linear time), poiché nel caso peggiore occorre esaminare ogni suo elemento.

Se una sequenza indicizzabile ``data`` è ordinata, allora la ricerca binaria è più efficiente.
Si osservi che, per ogni indice \\(j\\), tutti i valori memorizzati agli indici \\(0, \\ldots, j-1\\) 
sono minori o uguali del valore all'indice \\(j\\),
e che tutti i valori memorizzati agli indici \\(j+1, \\ldots ,n-1\\) sono maggiori o uguali
del valore all'indice \\(j\\).

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura21.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Un elemento della sequenza è un *candidato* se, al passo corrente, non si può decidere se questo elemento
sia uguale al target. L'algoritmo ha due parametri *low* e *high*, tali che tutti i potenziali candidati
devono avre indice compreso fra *low* e *high*.
Inizialmente \\(low = 0\\) e \\(high =n-1\\) (quindi tutti gli elementi della sequenza sono candidati).

Il target è confrontato con il valore mediano della sequenza, cioé il dato ``data[mid]`` con indice 
\\(mid = \\lfloor(low+high)/2 \\rfloor\\). Abbiamo tre casi:

- il target è uguale a ``data[mid]``, e quindi la ricerca ha avuto successo;
- il target è minore di ``data[mid]``, e quindi si ricorre sulla prima metà della sequenza 
  (sull'intervallo con indici da low a mid-1);
- il target è maggiore di ``data[mid]``, e qundi si ricorre sulla seconda metà della sequenza
  (sull'intervallo con indici da mid+1 a high).

La ricerca non ha successo se \\(low > high\\). Si osservi che mentre una ricerca sequenziale è
eseguita in tempo \\(O(n)\\), la ricerca binaria è eseguita in tempo\\(O(\log n)\\). 
Una implementazione in Python della ricerca binaria è fornita di seguito::

 1  def binary_search(data, target, low, high):
 2    # Return True if target is found in data, between low and high
 3    if low > high:
 4      return False               # no match found
 5    else:
 6      mid = (low + high) / 2
 7      if target == data[mid]:    # found a match
 8        return True
 9      elif target < data[mid]:
 10       return binary_search(data, target, low, mid - 1)   # call on the left portion
 11     else:
 12       return binary_search(data, target, mid + 1, high)  # call on the right portion

Nella figura seguente si ricerca il valore 18.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura22.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Sezione 2.2 Rovesciare una sequenza
===================================

Sia \\(A\\) una sequenza indicizzabile, e si consideri il problema di rovesciare la sequenza;
il primo elemento diventa l'ultimo, il penultimo diventa il secondo, e così via.
Ciò può essere ottenuto invertendo il primo e ultimo elemento, e rovesciando ricorsivamente
gli elementi rimanenti. Una implementazione in Python è fornit di seguito::

 1  def reverse(A, first, last):
 2    # Reverse elements in A, between first and last
 3    if first < last - 1:                           # if there are at least 2 elements
 4      A[first], A[last-1] = A[last-1], A[first]    # swap first and last
 5      reverse(A, first+1, last-1)

Se ``first == last``, non c'è nessuna sequenza da invertire; se ``first == last-1``,
la sequenza è formata da un solo elemento. In entrambi i casi abbiamo raggiunto il caso base della ricorsione.
Se esiste una sequenza da invertire, l'algoritmo terminerà dopo \\(1+\\lfloor n/2 \\rfloor\\) passi.
Poiché ogni chiamata richiede un numero costante di operazioni, l'algoritmo consuma tempo \\(O(n)\\).

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura23.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Sezione 2.3 Somma degli elementi di una sequenza
================================================

Sia \\(A\\) una sequenza indicizzabile di interi, e si consideri il problema di sommare gli elementi di \\(A\\).
Si osserva che la somma di tutti gli \\(n\\) interi in \\(A\\) è la somma dei primi \\(n-1\\) interi in \\(A\\),
più l'ultimo elemento.
Questa è chiaramente una definizione ricorsiva, ed è chiamata *ricorsione lineare*. 
Forniamo il codice Python di seguito::

 1  def linear_sum(A, n):
 2     # Returns the sum of the first n numbers of A
 3     if n == 0:
 4       return 0
 5     else:
 6       return A[n-1] + linear_sum(A, n-1)

Per un input di dimensione \\(n\\), l'algoritmo effettua \\(n+1\\) chiamate di funzione.
Poiché ogni chiamate richiede un numero costante di operazioni, l'algoritmo consuma tempo \\(O(n)\\).

Un altro approccio alla soluzione di questo problema può essere il seguente:
invece di sommare l'ultimo elemento di \\(A\\) con la somma dei rimanenti elementi,
potremmo calcolare la somma della prima metà di \\(A\\) e la somma della seconda metà di \\(A\\),
ricorsivamente, e poi sommare questi due numeri.
Questo è un caso di *ricorsione binaria*, in cui due chiamate ricorsive sono effettuate::

 1  def binary_sum(A, first, last):
 2    # Return the sum of the numbers in A between first and last
 3    if first <= last:     # no elements
 4      return 0
 5    elif first == last-1:     # one element
 6      return A[first]
 7    else:
 8      mid = (first + last) / 2
 9      return binary_sum(A, first, mid) + binary_sum(A, mid, last)

Per analizzare la complessità di questa versione della somma, consideriamo il caso in cui \\(n\\) 
è una potenza di 2.
Ad ogni chiamata ricorsiva la dimensione della sequenza è dimezzata, e quindi la profondità della
ricorsione è \\(1+ \\log n\\). Il tempo di esecuzione è ancora \\(O(n)\\), 
poiché ci sono  \\(2n-1\\) chianate ricorsive, ognuna richiedente tempo costante.
Lo stesso risultato vale per sequenze di lunghezza generica. 

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura24.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Sezione 3. La ricorsione può essere inefficiente
------------------------------------------------

In questa sezione esaminiamo alcuni problemi in cui un algoritmo ricorsivo provoca una drastica inefficienza.

Sezione 3.1 I numeri di Fibonacci
=================================

La sequenza di Fibonacci deve il suo nome al matematico Leonardo da Pisa, meglio noto come Fibonacci.
Nel suo libro del 1200, "Liber Abaci", egli introduce una sequenza come esercizio per descrivere una 
popolazione artificiale di conigli, che rispetta le seguenti condizioni:

- la popolazione iniziale consistein una coppia di conigli appena nati, uno maschio e uno femmina;
- i conigli si accoppiano dall'età di un mese;
- i conigli sono immortali;
- ogni coppia produce sempre una nuova coppia (uno maschio, uno femmina) ogni mese, a partire dal secondo mese.

I numeri di Fibonacci \\(F_n\\) sono i numeri di coppie di conigli dopo \\(n\\) mesi; 
ad esempio, dopo 10 mesi avremo \\( F_{10}\\) conigli.
Al di là dell'importanza matematica di questa sequenza, si osserva che essa può essere definita come
\\(F_0 =0\\), o \\(F_1 = 1\\), o \\(F_n = F_{n-2} + F_{n-1}\\).
Questo è un tipico esempio di ricorsione binaria, ed è possibile scrivere facilmente il suo codice Python::

 1  def fibonacci(n):
 2    # Returns the n-th Fibonacci number
 3    if n <= 1:
 4      return n
 5    else:
 6      return fibonacci(n-2) + fibonacci(n-1)

Sia \\(c_n\\) il numero di chiamate fatte nell'esecuzione di ``fibonacci(n)``.
Abbiamo i seguenti valori per \\(c_n\\):

| \\(c_0 = 1\\)
| \\(c_1 = 1\\)
| \\(c_2 = 1+c_0+c_1 = 1+1+1 = 3\\)
| \\(c_3 = 1+c_1+c_2 = 1+1+3 = 5\\)
| \\(c_4 = 1+c_2+c_3 = 1+3+5 = 9\\)
| \\(c_5 = 1+c_3+c_4 = 1+5+9 = 15\\)
| \\(c_6 = 1+c_4+c_5 = 1+9+15 = 25\\)
| \\(c_7 = 1+c_5+c_6 = 1+15+25 = 41\\)
| \\(c_8 = 1+c_6+c_7 = 1+25+41 = 67\\)

Osserviamo che l'implementazione naturale della formula di Fibonacci comporta una crescita quasi-esponenziale
del numero delle chiamate; ciò accade perchè il nostro algoritmo non ha memoria dei valori della sequenza calcolati in
precedenza, cioè esso deve ricalcolare valori che sono già stati calcolati. 
Ad esempio, nel calcolo di ``fibonacci(6)``,
abbiamo duechiamate a ``fibonacci(4)``, una fatta direttamente da ``fibonacci(6)``, 
e la seconda fatta da ``fibonacci(5)``.
Similmente, abbiamo tre chiamate a ``fibonacci(3)``: la prima, fatta da ``fibonacci(5)``;
la seconda e la terza fatte dalle due istanze di ``fibonacci(4)``.

Esistono modi più efficienti per ridefinire questo programma. Un primo approccio consiste nel definire un codice
che restituisca due numeri di Fibonacci consecutivi, \\( ( F_n ,F_{n-1} ) \\), cominciando con \\( F_{1}=0 \\).
Ciò consente di memorizzare il valore della funzione calconato in precedenza, e di passarlo al livello successivo
del calcolo, invece di ricalcolarlo::

 1  def good_fibonacci(n):
 2    # Returns the n-th and the (n-1)-th Fibonacci number
 3    if n <= 1:
 4      return (n,0)
 5    else:
 6      (a,b) = good_fibonacci(n-1)
 7      return (a+b,a)

La funzione ``good_fibonacci(n)`` è calcolata in tempo \\(O(n)\\).
Infatti, ogni chiamata ricorsiva decrementa l'argomento \\(n\\) di 1, provocando \\(n\\) chiamate di funzione.
Il lavoro non ricorsivo per ogni chiamata richiede tempo costante; quindi tutta la computazione richiede
tempo \\(O(n)\\).

Possiamo anche implementare una memoria usando un dizionario per salvare i valori calcolati in precedenza::

 1  memo = {0:0, 1:1}
 2  def fibm(n):
 3    if not n in memo:
 4      memo[n] = fibm(n-1) + fibm(n-2)
 5    return memo[n]

Un ultimo programma può essere definito usando il metodo ``__call__``::

 1  class Fibonacci:
 2    def __init__(self, a=0, b=1):
 3      self.memo = { 0:i1, 1:i2 }
 4    def __call__(self, n):
 5      if n not in self.memo:
 6        self.memo[n] = self.__call__(n-1) + self.__call__(n-2)
 7      return self.memo[n]
 8
 9  fib = Fibonacci()
 10 lucas = Fibonacci(2, 1)
 11
 12 for i in range(1, 16):
 13   print(i, fib(i), lucas(i))

Il programma restituisce il seguente output:

| 1 1 1
| 2 1 3
| 3 2 4
| 4 3 7
| 5 5 11
| 6 8 18
| 7 13 29
| 8 21 47
| 9 34 76
| 10 55 123
| 11 89 199
| 12 144 322
| 13 233 521
| 14 377 843
| 15 610 1364

I *numeri di Lucas* o la *serie di Lucas* sono una sequenza di interi che prendono il nome dal
matematico Francois Edouard Anatole Lucas (1842–91). Hanno la stessa regola di creazione dei numeri di Fibonacci,
ma i valori per 0 e 1 sono diversi.


Sezione 3.2 Unicità di un elemento
==================================

Data una sequenza \\(A\\) con \\(n\\) elementi, il problema della *unicità dell'elemento*,
o *element uniqueness problem* consiste nel decidere se tutti gli elementi della sequenza sono distinti.

la prima soluzione alproblema usa un algoritmo iterativo. In particolare, per ogni coppia di indici
\\(i\\) e \\(j\\), con \\(i<j\\), si verifica se \\(A[i]\\) e \\(A[j]\\) sono uguali::

 1  def unique1(A):
 2    # Returns True if there are no duplicate elements in A
 3    for i in range(len(A)):
 4      for j in range(i+1, len(A)):
 5        if A[i] == A[j]:
 6          return False       # there is a duplicate pair
 7    return True              # no duplicate elements

Come il lettore può osservare, ci sono due cicli annidati; le iterazioni del ciclo esterno causano 
\\(n-1, n-2, \\ldots, 2, 1\\) iterazione del ciclo interno, rispettivamente.
Ciò implica che nel caso peggiore (non esistono elementi duplicati) il programma compie 
\\((n-1)+(n-2)+ \\ldots +2+1\\) passi, cioé \\(O(n^2)\\) passi.

Un secondo algoritmo è basato sulla richiesta che la sequenza sia ordinata. 
Questo implica che se un elemento ha dei duplicati, questi saranno insieme nella sequenza.
Una singola scansione della sequenza è sufficiente, quando si confronta ogni elemento con il successivo.
Il codice Python di questo algoritmo è il seguente::

 1  def unique2(A):
 2    # Returns True if there are no duplicate elements in A
 3    temp = sorted(A)
 4    for i in range(1, len(temp)):
 5      if temp[i-1] == temp[i]:
 6        return False       # there is a duplicate pair
 7    return True            # no duplicate elements

Se la sequenza \\(A\\) è ordinata, l'algoritmo necessita di \\(O(n)\\) passi per decidere;
il tempo complessivo utilizzato è \\(O(n \\log n)\\), data la complessità dell'ordinamento.

Un algoritmo ricorsivo molto inefficiente è basato sulla seguente assunzione:
nel caso base, quando \\(n = 1\\), gli elementi sono ovviamente unici;
per \\(n > 2\\), gli elementi sono unici se e solo se i primi \\(n-1\\) elementi sono unici, 
gli ultimi \\(n-1\\) elementi sono unici, 
e il primo e l'ultimo elemento sono diversi. una implementazione ricorsiva è::

 1  def unique3(A, first, last):
 2    # Returns True if there are no duplicate elements in A[first:last]
 3    if last - first <= 1:
 4      return True               # there is only one item
 5    elif not unique3(A, first, last-1):
 6      return False              # the first part has a duplicate
 7    elif not unique3(a, first+1, last):
 8      return False              # the second part has duplicate
 9    else:
 10     return A[first] != A[last-1]       # first and last elements differ

Sia \\(n\\) il numero degli elementi della sequenza, cioé \\(n\\)= ``last-first``.
Se \\(n=1\\), il tempo di esecuzione di ``unique3`` è \\(O(1)\\). 
Nel caso generale, una chiamata a ``unique3`` provoca due chiamate ricorsive su problemi di 
dimensione \\(n-1\\).
Queste due chiamate risultano in quattro chiamate su problemi di dimensione \\(n-2\\), e così via.
Il numero totale delle chiamate di funzione è  \\(1+2+4+ \\ldots + 2^{n-1}\\),
che è uguale a \\(2^{n-1}\\). Il tempo di esecuzione di ``unique3`` è \\(O(2^n)\\).

Una soluzione migliore è nel seguente script::

 1  def unique(A, first, last):
 2    if last-first <= 1:                # there is only one item
 3      return True
 4    elif A[first] in A[first+1:]:      # if first element is in the rest of the list
 5      return False
 6    else:
 7      return unique(A, first+1, last)  # next element
