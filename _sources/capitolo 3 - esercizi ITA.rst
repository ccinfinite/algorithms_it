

Capitolo Tre: Sequenze basate su array - esercizi e sfide
*********************************************************


Sezione 1. Revisione della teoria
---------------------------------

**Esercizio 1.1**

Completare ed eseguire i seguenti frammenti di codice (che si trovano anche nel Capitolo Tre, Sezione 3);
confrontare i risultati ottenuti con il vostro sistema e quelli ottenuti nel Capitolo Tre
(anche se le velocità sono diverse, gli ordini di grandezza e la tendenza asintotica dovrebbero essere gli stessi).

Modificare il codice per dimostrare che la classe list di Python a volte contrae la dimensione dell'array sottostante
quando gli elementi sono estratti dalla lista.
(Suggerimento: costruire una lista grande a sufficienza prima di cominciare a rimuovere gli elementi).

.. activecode:: size-of-data
   :language: python
   :caption: Prints the size of a dynamic array wrt its elements

   import sys               # includes the function getsizeof

   data = [ ] 			    # empty list
   for k in range(n):
     a = len(data) 			    # a is the number of elements
     b = sys.getsizeof(data) 	# size of data in bytes
     print('Length: {0:3d}; Size in bytes: {1:4d}'.format(a, b))
     data.append(None) 		    # increase length by one

           # INSERT NEW CODE HERE


**Esercizio 1.2**

L'implementazione di ``insert`` per la classe ``DynamicArray`` è stata introdotta nel Capitolo Tre, Sezione 4.2.
Quando si verifica un ridimensionamento, questa operazione impiega tempo per copiare tutti gli elementi
dal vecchio array al nuovo, e successivamente il ciclo nel corpo di ``insert`` shifta molti degli elementi.
Modificare il seguente frammento di codice in modo che, in caso di ridimensionamento, gli elementi siano
shiftati nella loro posizione finale durante questa operazione, evitando così lo shift successivo.
(Suggerimento: usare due cicli non annidati)

.. activecode:: insert-for-dynamic-array
   :language: python
   :caption: Insert a value at position k in a dynamic array

   def insert(self, k, value):

     if self._n == self._capacity: 		# not enough room, resize the array
       self._resize(2*self._capacity)
     for j in range(self._n, k, −1): 	# shift to right, rightmost first
       self._A[j] = self._A[j−1]
     self._A[k] = value
     self. n += 1




Sezione 2. Un problema: costruire un gioco del tris
---------------------------------------------------

In questa sezione costruiremo un gioco del tris per due giocatori, che possono giocare dalla linea di comando.
Verificare il codice seguente: poi, completare ed eseguire l'ActiveCode alla fine della sezione.

Inizialmente costruiamo una tabella del gioco vuota (3 per 3) che è numerata come il tastierino numerico
della tastiera.
Un giocatore fa la sua mossa inserendo il numero corrispondente del tastierino. 
Per farlo, usiamo un dizionario, un dato primitivo di Python che immagazzina i dati nella forma “key: value”.
Creiamo un dizionario lungo 9, e ogni chiave (key) rappresenta un numero sulla tastiera;
il corrispondente valore (value) rappresenta la mossa fatta da un giocatore.
Creiamo una funzione ``printBoard()`` usata ogni volta che vogliamo stampare la tabella del gioco::

 theBoard = {'7': ' ' , '8': ' ' , '9': ' ' ,
             '4': ' ' , '5': ' ' , '6': ' ' ,
             '1': ' ' , '2': ' ' , '3': ' ' }

 def printBoard(board):
   print(board['7'] + '|' + board['8'] + '|' + board['9'])
   print('-+-+-')
   print(board['4'] + '|' + board['5'] + '|' + board['6'])
   print('-+-+-')
   print(board['1'] + '|' + board['2'] + '|' + board['3'])

Nella funzione principale accettiamo l'input daun giocatore e verifichiamo che sia una mossa valida.
Se è così, riempiremo il blocco corrispondente; altrimenti, chiederemo al giocatore di scegliere un altro blocco::

 def game():

   turn = 'X'
   count = 0

   for i in range(10):
     printBoard(theBoard)
     print("It's your turn," + turn + ".Move to which place?")

     move = input()
     if theBoard[move] == ' ':
       theBoard[move] = turn
       count += 1
     else:
       print("That place is already filled. Move to which place?")
       continue

Verifichiamo un totale di 8 condizioni, cercando quella vincente.
Dichiariamo vincente il giocatore che ha fatto l'ultima mossa;
altrimenti, se la tabella è piena e nessuna ha vinto, dichiareremo una partita patta::

 # check if player X or O has won, for every move after 5 moves.
 if count >= 5:
   if theBoard['7'] == theBoard['8'] == theBoard['9'] != ' ': # across the top
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['4'] == theBoard['5'] == theBoard['6'] != ' ': # across the middle
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['1'] == theBoard['2'] == theBoard['3'] != ' ': # across the bottom
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['1'] == theBoard['4'] == theBoard['7'] != ' ': # down the left side
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['2'] == theBoard['5'] == theBoard['8'] != ' ': # down the middle
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['3'] == theBoard['6'] == theBoard['9'] != ' ': # down the right side
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['7'] == theBoard['5'] == theBoard['3'] != ' ': # diagonal
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break
   elif theBoard['1'] == theBoard['5'] == theBoard['9'] != ' ': # diagonal
     printBoard(theBoard)
     print("\nGame Over.\n")
     print(" **** " +turn + " won. ****")
     break

 # If neither X nor O wins and the board is full, declare the result as a tie.
 if count == 9:
   print("\nGame Over.\n")
   print("It's a Tie!!")

 # change the player after every move.
   if turn =='X':
     turn = 'O'
   else:
       turn = 'X'

Infine, chiediamo ai giocatori se vogliono giocare ancora::

 board_keys = []
 for key in theBoard:
   board_keys.append(key)

 restart = input("Do want to play Again?(y/n)")
 if restart == "y" or restart == "Y":
   for key in board_keys:
     theBoard[key] = " "
   game()


Verificare e completare il seguente ActiveCode e eseguire il gioco del tris.

.. activecode:: tic-tac-toe
   :language: python
   :caption: Play the game!

   theBoard = {'7': ' ' , '8': ' ' , '9': ' ' ,
              '4': ' ' , '5': ' ' , '6': ' ' ,
              '1': ' ' , '2': ' ' , '3': ' ' }

   board_keys = []

   for key in theBoard:
     board_keys.append(key)

   def printBoard(board):
     print(board['7'] + '|' + board['8'] + '|' + board['9'])
     print('-+-+-')
     print(board['4'] + '|' + board['5'] + '|' + board['6'])
     print('-+-+-')
     print(board['1'] + '|' + board['2'] + '|' + board['3'])

   def game():
     turn = 'X'
     count = 0
     for i in range(10):
       printBoard(theBoard)
       print("It's your turn," + turn + ".Move to which place?")

       move = input()

       if theBoard[move] == ' ':
         theBoard[move] = turn
         count += 1
       else:
         print("That place is already filled.\nMove to which place?")
         continue

       if count >= 5:
         if theBoard['7'] == theBoard['8'] == theBoard['9'] != ' ': # across the top
           printBoard(theBoard)
           print("\nGame Over.\n")
           print(" **** " +turn + " won. ****")
           break
       elif theBoard['4'] == theBoard['5'] == theBoard['6'] != ' ': # across the middle
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['1'] == theBoard['2'] == theBoard['3'] != ' ': # across the bottom
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['1'] == theBoard['4'] == theBoard['7'] != ' ': # down the left side
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['2'] == theBoard['5'] == theBoard['8'] != ' ': # down the middle
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['3'] == theBoard['6'] == theBoard['9'] != ' ': # down the right side
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['7'] == theBoard['5'] == theBoard['3'] != ' ': # diagonal
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break
       elif theBoard['1'] == theBoard['5'] == theBoard['9'] != ' ': # diagonal
         printBoard(theBoard)
         print("\nGame Over.\n")
         print(" **** " +turn + " won. ****")
         break

       if count == 9:
         print("\nGame Over.\n")
         print("It's a Tie!!")

       if turn =='X':
         turn = 'O'
       else:
         turn = 'X'

     restart = input("Do want to play Again?(y/n)")
     if restart == "y" or restart == "Y":
       for key in board_keys:
         theBoard[key] = " "

       game()

   if __name__ == "__main__":
     game()








Sezione 3. Esercizi e autovalutazione
-------------------------------------

**Esercizio 3.1**

Sia \\(A\\) un array di dimensione \\(n \\geq 2\\) contenente interi da 1 a \\(n−1\\),
con esattamente una ripetizione. Descrivere un algoritmo veloce per trovare l'intero in \\(A\\) 
che è ripetuto.
(Suggerimento: non ordinare \\(A\\) )

**Esercizio 3.2**

Calcolare la somma di tutti i numeri in un insieme \\(n \\times n\\), rappresentato come una lista di liste,
usando le strutture di controll standard.
(Suggerimento: usare cicli annidati)

**Esercizio 3.3**

Descrivere come la funzione built-in ``sum`` può essere combinata con la comprehension syntax di Python per
calcolare la somma di tutti i numeri in un data set \\( n \\times n\\), rappresentato come una lista di liste.
(Suggerimento: costruire una lista di subtotali, uno per ogni lista annidata)

**Esercizio 3.4**

Il metodo ``shuffle`` del modulo ``random`` prende una lista e la riarrangia in modo casuale.
Costruire la vostra versione di questo metodo, usando la funzione ``randrange(n)`` del modulo ``random``,
che restituisce un numero casuale fra 0 e \\(n−1\\).

**Esercizio 3.5**

Considerare una implementazione di un array dinamico, ma invece di copiare gli elementi in un array di 
lunghezza doppia (cioé, da \\(N\\) a \\(2N\\) ) quando la capacità massima è raggiunta, copiare
gli elementi in un array con \\(N/4\\) lelle addizionali, passando quindi da \\(N\\) a \\(N+N/4\\) celle.
Dimostrare che eseguire una sequenza di \\(n\\) aggiunte all'array è ancora eseguito in tempo \\(O(n)\\).

**Esercizio 3.6**

Dato il seguente codice per la classe ``DynamicArray``, implementare un metodo ``pop`` 
che rimuove l'ultimo elemento dell'array, e che contrae la sua capacità \\(N\\) di metà ogni volta che 
il numero delgi elementi nell'array scende sotto \\(N/4\\).

.. activecode:: insert-for-dynamic-array-2
   :language: python
   :caption: Insert a value at position k in a dynamic array

   import ctypes					# provides low-level arrays

   class DynamicArray:
     def  __init__ (self):
       self._n = 0 				    # number of elements
       self._capacity = 1 			# initial capacity
       self._A = self._make_array(self._capacity) 			# define low-level array

     def __len__ (self):
       return self._n

     def __getitem__ (self, k):
       if not 0 <= k < self._n:
         raise IndexError('invalid index')
       return self._A[k]

     def append(self, obj):
       if self._n == self._capacity:			# full array
         self._resize(2*self._capacity) 		# double capacity
       self._A[self._n] = obj
       self._n += 1

     def _resize(self, c):
       B = self._make_array(c) 			  # new array
       for k in range(self._n):
         B[k] = self._A[k]
       self._A = B 						  # A is the new array
       self._capacity = c

     def _make_array(self, c):
       return (c*ctypes.py_object)( )


**Esercizio 3.7**

Dimostrare che quando si usa un array dinamico che cresce e si contrae come nel precedente esercizio,
la seguente serie di \\(2n\\) operazioni usa tempo \\(O(n)\\) :
\\(n\\) aggiunte con un array inizialmente vuoto, seguite da \\(n\\) pop.

**Esercizio 3.8**

``data.remove(value)`` rimuove la prima occorrenza dell'elemento ``value`` dalla lista ``data``.
Scrivere la funzione ``removeall(data, value)``, che rimuove tutte le occorrenze di ``value`` dalla lista,
tale che il caso perggiore dell'esecuzione della funzione è \\(O(n)\\), su una lista di \\(n\\) elementi.

**Esercizio 3.9**

Sia \\(B\\) un array di dimensione \\(n \\geq 6\\) contenente interi da 1 a \\(n−5\\), 
con esattamente cinque di essi ripetuti. Scrivere un algoritmo per trovare i cinque interi.


**Esercizio 3.10**

Descrivere un modo per usare la ricorsione per sommare tutti inumeriin un data set \\(n \\times n\\),
rappresentato come una lista di lista.

**Esercizio 3.11**

Scrivere un programma Python per una classe matrice che possa sommare e moltiplicare array bidimensionali di interi,
assumendo che le dimensioni siano compatibili.