 

Capitolo Cinque: Liste concatenate - esercizi e sfide
*****************************************************

Sezione 1. Revisione della teoria
---------------------------------

**Esercizio 1.1**

Completare ed eseguire i seguenti frammenti di codice (che possono essere trovati nel Capitolo Cinque),
in cui un ADT pila è implementato per mezzo di una lista concatenata singolarmente.
Ogni istanza della pila mantiene due variabili: ``_head`` (un riferimento al nodo in testa alla lista)
e ``_size`` (che tiene traccia del numero corrente degli elementi).
Aggungere alcuni esempi del funzionamento della pila, usando le chiamate ai metodi ``pop``, ``push``, o ``top``.

.. activecode:: stack-as-a-singly-linked-list
   :language: python
   :caption: Implementation of a stack by a singly linked list

   class LinkedStack:
   # Stack implementation with a singly linked list

     # Node class nested in stack
     class Node:
       __slots__ = '_element' , '_next'
       def __init__(self, element, next):
         self._element = element
         self._next = next

     # Stack methods
     def __init__(self):
       self._head = None
       self._size = 0

     def __len__(self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def push(self, e):
       self._head = self._Node(e, self._head)
       self._size += 1

     def top(self):
       if self.is_empty( ):
         raise Empty('Stack is empty')
       return self._head._element

     def pop(self):
       if self.is_empty( ):
         raise Empty('Stack is empty')
       answer = self._head._element
       self._head = self._head._next
       self._size −= 1
       return answer

           # INSERT NEW CODE HERE


**Esercizio 1.2**

Completare ed eseguire i seguenti frammenti di codice (che possono essere trovati nel Capitolo Cinque),
in cui un ADT coda è implementato per mezzo di una lista concatenata singolarmente.
Possiamo effettuare operazioni ad entrambi i capi della coda, e ciò implica che occorre mantenere due 
riferimenti, ``_head`` e a ``_tail``, come variabili di istanza per ogni coda, insieme a ``_size``.
L'inizio della coda è allineata con la testa della lista, e il fondo della coda è allineato con il fondo
della lista, consentendoci di inserire elementi al fondo e eliminarli dall'inizio.
Aggiungere esempio di funzionamento della coda, usando i metodi ``first``, ``enqueue``, o ``dequeue``.

.. activecode:: queue-as-a-singly-linked-list
   :language: python
   :caption: Implementation of a queue by a singly linked list

   class LinkedQueue:
   # Queue implementation with a singly linked list

     class Node:
       __slots__ = '_element' , '_next'
       def __init__(self, element, next):
         self._element = element
         self._next = next

     def __init__ (self):
       self._head = None
       self._tail = None
       self._size = 0

     def __len__ (self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def first(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       return self._head._element

     def dequeue(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       answer = self._head._element
       self._head = self._head._next
       self._size −= 1
       if self.is_empty( ):       # special case if queue is empty
         self._tail = None        # removed head had been the tail
       return answer

     def enqueue(self, e):
       newnode = self._Node(e, None)
       if self.is_empty( ):
         self._head = newnode
       else:
         self._tail._next = newnode
       self._tail = newnode
       self._size += 1

              # INSERT NEW CODE HERE



**Esercizio 1.3**

Completare ed eseguire i seguenti frammenti di codice (che possono essere trovati nel Capitolo Cinque),
in cui un ADT coda è implementato per mezzo di una lista concatenata circolare.
La cosa ha head e tail, con il riferimento next di tail collegato a head.
Occorre mantenere le variabili di istanza ``_tail`` (un riferimento al nodo finale della coda)
e ``_size`` (il numero degli elementi nella coda); la testa della coda è trovata seguendo il riferimento next di tail,
cioé ``self._tail._next``.
Con il metodo ``rotate``, la vecchia testa diventa la nuova fine della coda, 
e il nodo successivo la vecchia testa diventa la nuova testa.
Aggiungere esempi di funzionamento della coda, usando i metodi ``first``, ``enqueue``, o ``dequeue``.


.. activecode:: queue-as-a-circularly-list
   :language: python
   :caption: Implementation of a queue by means of a circularly list

   class CircularQueue:
   # Queue implementation using circularly linked list

     class _Node:
       __slots__ = '_element' , '_next'
       def __init__(self, element, next):
         self._element = element
         self._next = next

     def __init__(self):
       self._tail = None
       self._size = 0

     def __len__ (self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def first(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       head = self._tail._next
       return head._element

     def dequeue(self):
       if self.is_empty( ):
         raise Empty('Queue is empty')
       oldhead = self._tail._next
       if self._size == 1:
         self._tail = None
       else:
         self._tail._next = oldhead._next
       self._size −= 1
       return oldhead._element

     def enqueue(self, e):
       newnode = self._Node(e, None)
       if self.is_empty( ):
         newnode._next = newnode
       else:
         newnode._next = self._tail._next
         self._tail._next = newnode
       self._tail = newnode
       self._size += 1

     def rotate(self):
       if self._size > 0:
         self._tail = self._tail._next


      # INSERT NEW CODE HERE


**Esercizio 1.4**

Completare ed eseguire i seguenti frammenti di codice (che possono essere trovati nel Capitolo Cinque),
in cui un ADT deque è implementato per mezzo di una lista doppiamente concatenata.
La classe ``LinkedDeque`` eredita dalla classe ``_DoublyLinkedBase``,
usando i suoi metodi per inizializzare una nuova istanza per inserire un elemento all'inizio
o alla fine, e per cancellare un elemento.
Il primo elemento della deque è immagazzinato dopo lo header, 
e l'ultimo elemento della deque è immagazzinato prima del trailer.
Aggiungere esempi del funzionamento della deque, usando i metodi ``insert_first``, ``insert_last``,
``delete_first``, e ``delete_last``.

.. activecode:: deque-as-a-doubly_linked-list
   :language: python
   :caption: Implementation of a deque by means of a doubly linked list

   class _DoublyLinkedBase:

     class Node:
       __slots__ = '_element' , '_prev' , '_next'
       def __init__(self, element, prev, next):
         self._element = element
         self._prev = prev                # previous node reference
         self._next = next                # next node reference

     def __init__(self):
       self._header = self._Node(None, None, None)
       self._trailer = self._Node(None, None, None)
       self._header._next = self._trailer        # trailer is after header
       self._trailer._prev = self._header        # header is before trailer
       self._size = 0

     def __len__(self):
       return self._size

     def is_empty(self):
       return self._size == 0

     def _insert_between(self, e, predecessor, successor):
       newnode = self._Node(e, predecessor, successor)
       predecessor._next = newnode
       successor._prev = newnode
       self._size += 1
       return newnode

     def _delete_node(self, node):
       predecessor = node._prev
       successor = node._next
       predecessor._next = successor
       successor._prev = predecessor
       self._size −= 1
       element = node._element                             # record deleted element
       node._prev = node._next = node._element = None      # useful for the garbage collection
       return element                                      # return deleted element

   class LinkedDeque(_DoublyLinkedBase):

     def first(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._header._next._element

     def last(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._trailer._prev._element

     def insert_first(self, e):
       self._insert_between(e, self._header, self._header._next)

     def insert_last(self, e):
       self._insert_between(e, self._trailer._prev, self._trailer)

     def delete_first(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._delete_node(self._header._next)

     def delete_last(self):
       if self.is_empty( ):
         raise Empty("Deque is empty")
       return self._delete_node(self._trailer._prev)




Sezione 2. Problemi sulle liste concatenate
-------------------------------------------

In questa sezione affronteremo alcuni problemi classici che possonoessere risolti usando
le lista concatenate. Stidiare ogni esercizio,verificare e completare (se necessario)
gli ActiveCode forniti.

**Esercizio 2.1**

Scrivere una funzione che conti il numero di nodi in una lista concatenata singolarmente, in modo
iterativo e ricorsivo. La soluzione iterativa è data nel seguente pseudocodice::

 initialize count as 0
 initialize a node pointer, current = head.
 do following while current is not NULL
   current = current -> next
   count++;
 return count

Verificare, completare e eseguire il seguente ActiveCode.

.. activecode:: number-of-nodes-list-iterative
   :language: python
   :caption: Returns the number of nodes in a list, with an iteration

   class Node:
     def __init__(self, data):
       self.data = data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def getCount(self):
       temp = self.head
       count = 0
       while (temp):
         count += 1
         temp = temp.next
       return count

   if __name__=='__main__':
     llist = LinkedList()
     llist.push(1)
     llist.push(3)
     llist.push(1)
     llist.push(2)
     llist.push(1)
     print ("Count of nodes is :",llist.getCount())


La soluzione ricorsiva è data nel seguente pseudocodice::

 int getCount(head)
   if head is NULL, return 0
   else return 1 + getCount(head->next)

Verificare, completare e eseguire il seguente ActiveCode.

.. activecode:: number-of-nodes-list-iterative-2
   :language: python
   :caption: Returns the number of nodes in a list, with an iteration

   class Node:
     def __init__(self, data):
       self.data = data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def getCountRec(self, node):
        if (not node):
          return 0
        else:
          return 1 + self.getCountRec(node.next)

     def getCount(self):
       return self.getCountRec(self.head)

   if __name__=='__main__':
     llist = LinkedList()
     llist.push(1)
     llist.push(3)
     llist.push(1)
     llist.push(2)
     llist.push(1)
     print 'Count of nodes is :',llist.getCount()


**Esercizio 2.2**

Data una lista concatenata e un numero \\(n\\), scrivere una funzione che restituisca il valore
dell'\\(n\\)-mo nodo della lista, a partire dal fondo.
Una semplice soluzione consiste nel calcolare la lunghezza *len* della lista, e 
stampare il *len-n+1*-mo nodo dall'inizio della lista.
L'ActiveCode è dato di seguito.

.. activecode:: Nth-node-from-the-end
   :language: python
   :caption: Returns the n-th node from the and of the list

   class Node:
     def __init__(self, new_data):
       self.data = new_data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def printNthFromLast(self, n):
       temp = self.head
       length = 0
       while temp is not None:
         temp = temp.next
         length += 1

        if n > length:
          print('Location is greater than the length of the list')
          return
        temp = self.head
        for i in range(0, length - n):
          temp = temp.next
        print(temp.data)

   llist = LinkedList()
   llist.push(20)
   llist.push(4)
   llist.push(15)
   llist.push(35)
   llist.printNthFromLast(4)

Un'altra soluzione interessante consiste nel mantenere due puntatori,il *reference* e il *main*.
Inizialmente essi puntano entrambi all'inizio della lista, e il puntatore reference è mosso di
\\(n\\) nodei dall'inizio.
Successivamente, entrambi i puntatori sono mossi di una posizione verso destra, fino a che il
puntatore reference non raggiunga la fine della lista.
Main punta in questo momento al nodo \\(n\\)-mo dalla fine.
L'ActiveCode è dato di seguito.

.. activecode:: Nth-node-from-the-end-2
   :language: python
   :caption: Returns the n-th node from the and of the list

   class Node:
     def __init__(self, new_data):
       self.data = new_data
       self.next = None

   class LinkedList:
     def __init__(self):
       self.head = None

     def push(self, new_data):
       new_node = Node(new_data)
       new_node.next = self.head
       self.head = new_node

     def printNthFromLast(self, n):
       main_ptr = self.head
       ref_ptr = self.head

       count = 0
       if(self.head is not None):
         while(count < n ):
           if(ref_ptr is None ):
             print('Location is greater than the length of the list')
             return
           ref_ptr = ref_ptr.next
           count += 1

       if(ref_ptr is None):
         self.head = self.head.next
         if(self.head is not None):
           print('The node no. %d from last is %d', n, main_ptr.data)
      else:
        while(ref_ptr is not None):
          main_ptr = main_ptr.next
          ref_ptr = ref_ptr.next
        print('The node no. %d from last is %d', n, main_ptr.data)

   llist = LinkedList()
   llist.push(20)
   llist.push(4)
   llist.push(15)
   llist.push(35)
   llist.printNthFromLast(4)


**Esercizio 2.3**

Questo problema deve il suo nome a Flavius Josephus, uno storico ebreo che lottò contro i romani.
Lui e il suo gruppo di soldati erano assediati dai romani in una caverna, e decisero di non arrendersi vivi.
Tutti i soldati sedettore in cerchio e, a partire dal soldato seduto nella prima posizione, ogni soldato avrebbe
ucciso il soldato seduto vicino a lui.
Supposto di avere 5 soldati seduti in cerchio nelle posizione numerate con 1, 2, 3, 4, 5,
il soldato 1 uccide 2, poi 3 ucide 4, poi 5 uccide 1, poi 3 uccide 5; poichè 3 è l'ultimo soldato rimasto vivo,
egli commette suicidio. Josephus non voleva essere ucciso o suicidarsi, e quindi dovette capire quale posizione
occupare nel cerchio per essere l'ultimo uomo sopravvissuto (e arrendersi ai romani).
Esistono varie soluzioni al problema (in generale, per \\(n=2^a +k\\) soldati, il sopravvissuto è il soldato
\\( 2k+1 \\). Mostriamo una soluzione che usa una lista circolare.

.. activecode:: josephus-problem
   :language: python
   :caption: Josephus problem with a circular linked list

   class Node:
     def __init__(self, x):
       self.data = x
       self.next = None

   def getJosephusPosition(m, n):
     # Create a circular linked list of size n.
    head = Node(1)
    prev = head
    for i in range(2, n + 1):
      prev.next = Node(i)
      prev = prev.next
    prev.next = head

     # while only one node is left in the linked list
     ptr1 = head
     ptr2 = head
     while (ptr1.next != ptr1):
       count = 1
       while (count != m):
         ptr2 = ptr1
         ptr1 = ptr1.next
         count += 1
       ptr2.next = ptr1.next
       ptr1 = ptr2.next

     print("Last person left standing (Josephus Position) is ", ptr1.data)

   if __name__ == '__main__':
     n = 14
     m = 2
     getJosephusPosition(m, n)


**Esercizio 2.4**

Il compito è di creare una lista doppiamente concatenata, con puntatori head e tail, inserendo i nodi
in modo che la lista rimanga in ordice crescente quando stampata da sinistra a destra.
Lo pseudocodice è il seguente::

 if the list is empty, then
   make left and right pointers point to the node to be inserted
   make its previous and next field point to NULL
 if node to be inserted has value lower than the value of first node of the list, then
   connect that node from previous field of first node
 if node to be inserted has value higher than the value of last node of the list, then
   connect that node from next field of last node
  if node to be inserted has value in between the value of first and last node, then
    check for appropriate position and make connections

Verificare, completare e eseguire il seguente ActiveCode.

.. activecode:: insert-in-sorted-list
   :language: python
   :caption: Insert a new value in a sorted list

   class Node:
   def __init__(self, data):
     self.info = data
     self.next = None
     self.prev = None

   head = None
   tail = None

   def nodeInsert( key) :
     global head
     global tail

     p = Node(0)
     p.info = key
     p.next = None

     # if first node to be inserted in doubly linked list
     if ((head) == None):
       (head) = p
       (tail) = p
       (head).prev = None
       return

     # if node to be inserted has value lower than first node
     if ((p.info) < ((head).info)):
       p.prev = None
       (head).prev = p
       p.next = (head)
       (head) = p
       return

     # if node to be inserted has value higher than last node
     if ((p.info) > ((tail).info)):
       p.prev = (tail)
       (tail).next = p
       (tail) = p
       return

     # find the node before which we need to insert p.
     temp = (head).next
     while ((temp.info) < (p.info)):
       temp = temp.next

     # insert new node before temp
     (temp.prev).next = p
     p.prev = temp.prev
     temp.prev = p
     p.next = temp

   # print nodes from left to right
   def printList(temp) :
     while (temp != None):
       print( temp.info, end = " ")
       temp = temp.next

   nodeInsert( 30)
   nodeInsert( 50)
   nodeInsert( 90)
   nodeInsert( 10)
   nodeInsert( 40)
   nodeInsert( 110)
   nodeInsert( 60)
   nodeInsert( 95)

   print("Doubly linked list on printing from left to right\n" )
   printList(head)









Section 3. Esercizi e autovalutazione
-------------------------------------

**Esercizio 3.1**

Scrivere un algoritmo per trovare il penultimo nodo in una lista concatenata singolarmente
in cui l'ultimo nodo ha il riferimento next uguale a None.

**Esercizio 3.2**

Scrivere un algoritmo per concatenare due liste concatenate singolarmente L e M 
(dati esclusivamente i riferimenti al primo nodo di ogni lista) in una lista che contenga i
nodi di L e i nodi di M.
(Suggerimento: la concatenazione non deve esaminare tutti gli elementi delle due liste)

**Esercizio 3.3**

Scrivere un algoritmo ricorsivo che conti il numero di nodi in una lista concatenata.
(Suggerimento: usare il puntatore ``next`` come parametro per la chiamata della funzione)

**Esercizio 3.4**

Scrivere una funzione che conti il numero di nodi in una lista circolare concatenata.
(Suggerimento: tenere traccia del punto di partenza, e fermarsi quando lo si raggiunge)

**Esercizio 3.5**

x e y sono riferimenti a nodi di una lista circolare concatenata, ma non necessariamente nella stessa lista.
Scrivere un algoritmo per decidere se x e y appartengono alla stessa lista.
(Suggerimento: scandire una delle due liste solo una volta)

**Esercizio 3.6**

Nel Capitolo Cinque, Sezione 2.1, può essere trovata una implementazione di una coda tramite una lista
circolare concatenata. 
La classe ``CircularQueue`` fornisce un metodo ``rotate()``, la cui semantica è equivalente a Q.enqueue(Q.dequeue()); 
l'inizio della coda diventa la nuova fine, e il nodo successivo al vecchio inizio diventa il nuovo inizio.
Scrivere lo stesso metodo per la classe ``LinkedQueue`` della Sezione 1.2, in cui una coda è implementata
con una lista concatenata singolarmente.
(Suggerimento: gestire i collegamenti in modo che il primo nodo sia spostato alla fine della lista)

**Esercizio 3.7**

Scrivere un metodo per trovare il nodo di mezzo di una lista doppiamente concatenata con header e trailer,
usando il link hopping (senza contatori).
Nel caso di un numero pari di nodi, restituire il nodo a sinistra del centro.
(Suggerimento: muoversi nella lista da entrambi i capi)

**Esercizio 3.8**

Scrivere un algoritmo veloce per concatenare due liste doppiamente concatenate L e M,
con header e trailer, in una singola lista.
(Suggerimento: collegare la fine di L e l'inizio di M)

**Esercizio 3.9**

Scrivere un metodo ``max(L)`` che restituisca l'elemento massimo di una ``PositionalList`` L
contenente interi.

**Esercizio 3.10**

Modificare la classe ``PositionalList`` del Capitolo Cinque, Sezione 4.2, per supportare un metodo
addizionale ``find(e)``, che restituisca la posizione della prima occorrenza dell'elemento e nella lista.

**Esercizio 3.11**

Descrivere una implementazione dei metodi ``add_last`` e ``add_before`` di ``PositionalList``,
usando solo i metodi ``is_empty``, ``first``, ``last``, ``prev``, ``next``, ``add_after``, e ``add_first``.

**Esercizio 3.12**

Scrivere una implmentazione completa dell'ADT pila e dell'ADT coda tramite una lista concatenata singolarmente
che usi uno header.

**Esercizio 3.13**

Scrivere un metodo ``concatenate(Q2)`` per la classe ``LinkedQueue`` che prende tutti gli elementi di Q2
e li aggiunge alla fine della coda originaria.
(Suggerimento: lavorare su inizio e fine delle code)

**Esercizio 3.14**

Scrivere una implementazione ricorsiva di una classe lista concatenata cingolarmente,
tale che una istanza di una lista non vuota immagazzina il suo primo elemento e un riferimento
a una lista di elementi rimanenti. Poi, scrivere un algoritmo ricorsivo per rovesciare la lista.
(Suggerimento: la catena di nodi che seguono il primo è anch'essa una lista; per rovesciarla,
ricorrere sulle prime \\(n−1\\) posizioni)

**Esercizio 3.15**

Implementare una funzione ``bubble-sort`` che prende una lista L come parametro.
(Suggerimento: bubble-sort scandisce la lista \\(n−1\\) times; per ogni scansione, confronta l'elemento corrente
con il successivo, e li scambia se non sono in ordine. Una funzione ``swap`` sarebbe di aiuto)

**Esercizio 3.16**

Un array \\(A\\) è *sparso* se molti dei suoi elementi sono vuoti.
Una lista L può essere usata per implementare questo tipo di array efficientemente.
Per ogni cella non vuota \\(A[i]\\), possiamo immagazzinare un elemento \\((i,e)\\) in L, con e
l'elemento in \\(A[i]\\).
Ciò consente di rappresentare \\(A\\) usando \\(O(m)\\) spazio, con \\(m\\) 
il numero di elementi non nulli di \\(A\\).
Scrivere la classe ``SparseArray`` con i metodi ``getitem(j)`` e ``setitem (j,e)``.
