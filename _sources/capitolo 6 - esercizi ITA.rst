
Capitolo Sei: Ordinamenti - esercizi e sfide
********************************************

Sezione 1. Revisione della teoria
---------------------------------

Completare ed eseguire i frammenti di codice delle seguenti sezioni (i codici possono essere trovati
anche nel Capitolo Sei).

**Esercizio 1.1**

Il *bubble sort* confronta coppie di valori adiacenti in una lista, scambiandoli di posto se essi non sono
nell'aordine corretto. Effettua multiplescansioni della lista; per ogni passo, il valore più grande
è fatto migrare verso la sua posizione corretta.
Il codice Python del bubble sort è nel seguente ActiveCode.

.. activecode:: bubble-sort
   :language: python
   :caption: Function for the bubble sort

   def bubbleSort(alist):
     for passnum in range(len(alist)-1,0,-1):
       for i in range(passnum):
         if alist[i] > alist[i+1]:
           temp = alist[i]
           alist[i] = alist[i+1]
           alist[i+1] = temp

        # INSERT NEW CODE HERE


Possiamo scrivere un *short bubble sort*, che si arresta se trova che la lista è già ordinata.

.. activecode:: short-bubble-sort
   :language: python
   :caption: Function for the short bubble sort

   def shortbubbleSort(alist):
     exchanges = True
     passnum = len(alist)-1
     while passnum > 0 and exchanges:
       exchanges = False
       for i in range(passnum):
         if alist[i] > alist[i+1]:
           exchanges = True
           temp = alist[i]
           alist[i] = alist[i+1]
           alist[i+1] = temp
       passnum = passnum-1

        # INSERT NEW CODE HERE


**Esercizio 1.2**

Il *selection sort* effettua scasioni multiple della lista, alla ricerca del valori più grande, per spostarlo
nella sua posizione corretta; ciò è realizzato con un solo scambio per ogni scansione.
Dopo la prima scansione, l'elemento più grande è nella posizione corretta; 
dopo la seconda, il più grande dei rimanenti è nella posizione corretta.
Il codice Python per il selection sort è nel seguente ActiveCode.

.. activecode:: selection-sort
   :language: python
   :caption: Function for the selection sort

   def selectionSort(alist):
     for fillslot in range(len(alist)-1,0,-1):
       positionOfMax=0
       for location in range(1,fillslot+1):
         if alist[location] > alist[positionOfMax]:
           positionOfMax = location
       temp = alist[fillslot]
       alist[fillslot] = alist[positionOfMax]
       alist[positionOfMax] = temp

        # INSERT NEW CODE HERE


**Esercizio 1.3**

L'*insertion sort* crea una sottolista ordinata a partire dalla prima posizione della lista,
e inserisce in questa sottolista un nuovo oggetto, producendo una nuova sottolista ordinata.
Pertendo con una lista con un solo elemento, ogni elemento a partire dal secondo fine all'ultimo
è confrontato che quelli già nella sottolista ordinata;
gli oggetti più grandi sono shiftati a destra, e l'elemento corrente è inserito nella sottolista.
Il codice Python per l'insertion sort è nel seguente ActiveCode.


.. activecode:: insertion-sort
   :language: python
   :caption: Function for the insertion sort

   def insertionSort(alist):
     for index in range(1,len(alist)):
       currentvalue = alist[index]
       position = index
       while position > 0 and alist[position-1] > currentvalue:
         alist[position]=alist[position-1]
         position = position-1
       alist[position]=currentvalue

        # INSERT NEW CODE HERE


**Esercizio 1.4**

Il *merge sort* è un algoritmo ricorsivo.
Se la lista è vuota o ha un solo elemento, essa è ordinata; se la lista ha più di un elemento,
essa è divisa è il merge sort è invicato ricorsivamente sulle due metà.
Quando esse sono ordinate, una fuzione è effettuata.
Il codice Python per il merge sort è nel seguente ActiveCode.

.. activecode:: merge-sort
   :language: python
   :caption: Function for the merge sort

   def mergeSort(alist):
     print("Splitting ",alist)
     if len(alist) > 1:
       mid = len(alist)//2
       lefthalf = alist[:mid]
       righthalf = alist[mid:]

       mergeSort(lefthalf)
       mergeSort(righthalf)

       i=0
       j=0
       k=0
       while i < len(lefthalf) and j$< len(righthalf):
         if lefthalf[i] <= righthalf[j]:
           alist[k] = lefthalf[i]
           i=i+1
         else:
           alist[k] = righthalf[j]
           j=j+1
         k=k+1

       while i < len(lefthalf):
         alist[k]=lefthalf[i]
         i=i+1
         k=k+1

       while j <$len(righthalf):
         alist[k]=righthalf[j]
         j=j+1
         k=k+1
     print("Merging ",alist)

        # INSERT NEW CODE HERE


**Esercizio 1.5**

Il *quick sort* comincia selezionando un valore, il *pivot*, che usualmente è il primo elemento nella lista;
la sua corretta posizione è trovata, e questa posizione (lo *split point*) è usata nel
processo di divisione della lista in due sottoliste.
Il quick sort è applicato ricorsivamente alle due sottoliste.

Due marcatori, *leftmark* e *rightmark*, sono definiti come la prima e l'ultima posizione degli elementi 
restanti nella lista, rispettivamente. Leftmark è incrementato finché non è trovato un valore maggiore del pivot;
rightmark è decrementato finché non è trovato un valore minore del pivot.
I due valori sono fuori posto rispetto allo split point, e quindi sono scambiati.

La procedura continua finché rightmark diventa minore di leftmark; ciò indica che è stato trovato lo
split point. IL valore del pivot è scambiato con quello dello split point, e quindi il pivot è
nella corretta posizione.
Il quick sort è chiamato ricorsivamente sulle due sottoliste. La sua definizione è la seguente.

.. activecode:: quick-sort
   :language: python
   :caption: Function for the quick sort

   def quickSort(alist,first,last):
     if first < last:
       splitpoint = partition(alist,first,last)
       quickSort(alist,first,splitpoint-1)
       quickSort(alist,splitpoint+1,last)

   def partition(alist,first,last):
     pivotvalue = alist[first]
     leftmark = first+1
     rightmark = last

     done = False
     while not done:
       while leftmark <= rightmark and alist[leftmark] <= pivotvalue:
         leftmark = leftmark + 1
       while leftmark <= rightmark and alist[rightmark] >= pivotvalue:
         rightmark = rightmark -1

       if rightmark < leftmark:
         done = True
       else:
         temp = alist[leftmark]
         alist[leftmark] = alist[rightmark]
         alist[rightmark] = temp

     temp = alist[first]
     alist[first] = alist[rightmark]
     alist[rightmark] = temp
     return rightmark

        # INSERT NEW CODE HERE


**Esercizio 1.6**

La *selezione* è il problema di trovare il \\(k\\)-mo più piccolo elemento in una collezione non ordinata di
\\(n\\) elementi.

Data una sequenza non ordinata \\(S\\) di \\(n\\) elementi confrontabili, e un intero \\(k\\) in \\([1,n]\\),
il *randomized quick-select* prende un elemento casuale da \\(S\\), e lo usa per dividere \\(S\\) in
tre sottosequenze \\(L\\), \\(E\\), e \\(G\\), che contengono gli elementi di \\(S\\) minori di, uguali a,
e maggiori del pivot, rispettivamente.
Poi, viene verificato quale dei tre sottoinsiemi contiene l'elemento desiderato, basandosi sul valore di
\\(k\\) e sulla dimensione dei sottoinsiemi.
La ricorsione è applicata sul sottoinsieme appropriato. 
Una implementazione del randomized quick-select è mostrata di seguito.

.. activecode:: quick-sort-2
   :language: python
   :caption: Function for the quick sort

   def quickselect(S, k):
     if len(S) == 1:
       return S[0]
     pivot = random.choice(S)
     L = [x for x in S if x < pivot]     # elements less than pivot
     E = [x for x in S if x == pivot]    # elements equal to pivot
     G = [x for x in S if pivot < x]     # elements greater than pivot
     if k <= len(L):
       return quickselect(L, k)     # the k-th smallest is in L
     elif k <= len(L) + len(E):
       return pivot                 # the k-th smallest is equal to pivot
     else:
       j = k-len(L)-len(E)
       return quickselect(G, j)     # the k-th smallest is the j-th in G

        # INSERT NEW CODE HERE



Section 2. Problems on sorting
------------------------------

In questa sezione proporremo alcuni algoritmi classici per l'ordinamento; verificare, completare e eseguire
gli ActiveCode in ciascuna sottosezione.

**Esercizio 2.1**

Il *pigeonhole sort* è un algoritmo per ordinare sequenze in cui il numero di elementi e il numero dei possibili
valori sono irca gli stessi. Richiede tempo \\( O(n+R) \\), con \\(n\\) il numero di elementi nella sequenza
ed \\(R\\), il *range*, il numero di possibili valori.

L'algortmo trova il minimo e il massimo nella sequenza (*min* e *max*, rispettivamente),
e il range \\( R=max-min+1 \\).
Un nuovo array di *pigeonholes* è creato, inizialmente vuoto; la sua dimensione è uguale al range \\( R \\).

Ogni elemento \\(arr[i]\\) dell'array iniziale è messo in un buco di indice \\(arr[i] – min\\).
Poi, gli elementi dell'array pigeonhole non vuoti, sono ritrasferiti nell'array originario.
Il pigeonhole sort ha uso limitato, date le caratteristiche richieste. 
Per array il cui range è molto più grande di \\(n\\), il bucket sort è una generalizzazione più
efficiente in termini di tempo e spazio di esecuzione.
Una implementazione del pigeonhole sort è mostrata nel seguente ActiveCode.

.. activecode:: pigeonhole-sort
   :language: python
   :caption: Function for the pigeonhole sort

   def pigeonhole_sort(a):
     # building the pigeonholes
     my_min = min(a)
     my_max = max(a)
     size = my_max - my_min + 1
     holes = [0] * size

     for x in a:
       holes[x - my_min] += 1

     i = 0
     for count in range(size):
       while holes[count] > 0:
         holes[count] -= 1
         a[i] = count + my_min
         i += 1

        # INSERT NEW CODE HERE


**Esercizio 2.2**

Il *comb sort* è un miglioramente del bubble sort. Questo confronta valori adiacenti e rimuove tutte le inversioni.
Il comb sort usa gap di dimensione maggiore di 1. Si comincia con gap grandi, che sono ridotti di un fattore 1.3
ad ogni iterazione, finchè il gap non raggiunge valore 1. In questo modo il coum sort rimuove
più di un *inversion count* con una passata.

Il fattore di riduzione è stato determinato empiricamente; nel caso medio questo algoritmo ha performance migliore
del bubble sort. La complessità del caso medio è \\( \\Omega(n^2/2^p) \\), con \\(p\\) il numero degli incrementi.
Il caso peggiore è \\(O(n^2)\\) e il caso migliore è \\(O(n \\log n)\\).
Di seguito l'implementazione.

.. activecode:: comb-sort
   :language: python
   :caption: Function for the comb sort

   # find next gap from current
   def getNextGap(gap):
     gap = (gap * 10) // 13
     if gap < 1:
       return 1
     return gap

   # sort arr[] using Comb Sort
   def combSort(arr):
     n = len(arr)
     gap = n

     swapped = True
     while gap !=1 or swapped == 1:
       gap = getNextGap(gap)        # find next gap
       swapped = False               # initialize swapped as false, to check if a swap happens
       for i in range(0, n-gap):
         if arr[i] > arr[i + gap]:
           arr[i], arr[i + gap]=arr[i + gap], arr[i]
           swapped = True

         # INSERT NEW CODE HERE



**Esercizio 2.3**

Il *Jump search* è un algoritmo di ricerca per array ordinati.
Verifica meno elementi (della ricerca lineare) saltando di passi fissati fra gli elementi, invece di scandirli tutti.
Supponiamo di avere un array \\(arr[]\\) di dimensione \\(n\\) e blocchi (da saltare) di dimensione \\(m\\).
Cerchiamo agli indici \\(arr[0] , arr[m] , arr[2m], \\ldots , arr[km] , \\ldots \\), e così via.
Una volta trovato l'intervallo \\( arr[km] < x < arr[(k+1)m] \\), è fatta una ricerca lineare dall'indice 
\\(km\\) per trovare l'elemento \\(x\\).

Nel caso peggiore occorre fare \\(n/m\\) salti, e se l'ultimo valore verificato è più grande dell'elemento
da cercare, si fanno \\(m-1\\) confronti per la ricerca lineare.
Quindi il numero totale di confronti nel caso peggiore sarà \\( ((n/m) + m-1) \\).
La funzione \\( f(m)=(n/m) + m-1 \\) è minima quando \\( m = \\sqrt n \\); 
quindi, la miglior grandezza per il passo è \\( m = \\sqrt n \\).
Di seguito il codice che implementa il jump search.

.. activecode:: jump-search
   :language: python
   :caption: Function for the jump search

   import math

   def jumpSearch(arr , x):
     n = len(arr)
     step = math.sqrt(n)

     # finding the block where the element is
     prev = 0
     while arr[int(min(step, n)-1)] < x:
       prev = step
       step += math.sqrt(n)
       if prev >= n:
         return -1

     # linear search for x in the block beginning with prev
     while arr[int(prev)] < x:
       prev += 1
       if prev == min(step, n):   # if we reach next block or end of array, the element is not present
         return -1

     # if element is found
     if arr[int(prev)] == x:
       return int(prev)
     return -1

        # INSERT NEW CODE HERE



**Esercizio 2.4**

L'*exponential search* su un arry ordinato comporta due passi::

 Find range where element x is present
 Do Binary Search in that range

Per trovare il range si comincia con un subarray di dimensione 1, e si confronta il suo ultimo elemento con \\(x\\);
poi si prova con la dimensione 2, 3, 4 e così via fino a che l'ultimo elemento di un subarray 
non sia maggiore di \\(x\\).
Trovato un indice \\(i\\), sappiamo che l'elemento deve essere presente fra \\(i/2\\) e \\(i\\).
La complessità temporale è \\( O(\\log n) \\), e lo spazio richiesto dalla ricerca binaria è \\( O(\\log n) \\).
Se la ricerca binaria è iterativa, lo spazio richiesto è solo \\( O(1)\\).

.. activecode:: jump-search-2
   :language: python
   :caption: Function for the jump search

   def binarySearch(arr, l, r, x):
   # binary search function that returns the location of x in given array arr[l..r]
     if r >= l:
       mid = l + ( r-l ) // 2
       if arr[mid] == x:
         return mid
       if arr[mid] > x:
         return binarySearch(arr, l, mid - 1, x)
       return binarySearch(arr, mid + 1, r, x)
     return -1

   def exponentialSearch(arr, x):
   # returns the position of first occurrence of x in array
     n = len(arr)
     if arr[0] == x:
       return 0

     # find range for binary search by repeated doubling
     i = 1
     while i < n and arr[i] <= x:
       i = i * 2

     # call binary search for the range
     return binarySearch(arr, i // 2, min(i, n-1), x)

        # INSERT NEW CODE HERE


Sezione 3. Esercizi e autovalutazione
-------------------------------------

**Esercizio 3.1**

Data la seguente lista di numeri da ordinare: [19, 1, 9, 7, 3, 10, 13, 15, 8, 12];
quale lista si ottiene dopo tre cicli completi del bubble sort?

**Esercizio 3.2**

Data la seguente lista di numeri da ordinare: [11, 7, 12, 14, 19, 1, 6, 18, 8, 20];
quale lista si ottiene dopo tre cicli completi del selection sort?

**Esercizio 3.3**

Data la seguente lista di numeri da ordinare: [15, 5, 4, 18, 12, 19, 14, 10, 8, 20];
quale lista si ottiene dopo tre cicli completi dell'insertion sort?

**Esercizio 3.4**

Data la seguente lista di numeri: [21, 1, 26, 45, 29, 28, 2, 9, 16, 49, 39, 27, 43, 34, 46, 40];
quel è la lista da ordinare dopo tre chiamate ricorsive del mergesort?

**Esercizio 3.5**

Data la seguente lista di numeri: [21, 1, 26, 45, 29, 28, 2, 9, 16, 49, 39, 27, 43, 34, 46, 40];
quali sono le prime due liste da fondere?

**Esercizio 3.6**

Data la seguente lista di numeri: [14, 17, 13, 15, 19, 10, 3, 16, 9, 12]:
qual è la lista dopo il secondo partizionamento del quicksort?

**Esercizio 3.7**

Quale fra shell-, quick-, merge-, o insertion sort è garantito essere \\(O(n \\log n)\\) nel caso peggiore?

**Esercizio 3.8**

Un algoritmo che ordina elementi chiave-valore in base alla chiave è *straggling*
se ogni volta che due elementi \\(e_i\\) ed \\(e_j\\) hanno la stessa chiave, 
ma \\(e_i\\) appare prima di \\(e_j\\) nell'input, 
allora l'algoritmo colloca \\(e_i\\) dopo \\(e_j\\) nell'output.
Descrivere una modifica del merge sort per renderlo straggling.

**Esercizio 3.9**

Supporre di avere due sequenze ordinate di \\(n\\) elementi, \\(A\\) e \\(B\\), ciascuna con elementi distinti,
ma con qualche elemento in entrambe le sequenze.
Trovare un metodo che sia eseguito in tempo \\(O(n)\\) per calcolare una sequenza che rappresenti
l'unione di \\(A\\) and \\(B\\) (senza duplicati), ordinata.

**Esercizio 3.10**

Degli \\(n!\\) possibili input di un dato algoritmo di ordinamento basato su confronti,
qual è il massimo numero di input che possono essere ordinati correttamente con solo \\(n\\) confronti?

**Esercizio 3.11**

Sia \\(S\\) una sequenza di \\(n\\) valori, ognuno uguale a 0 o 1.
Quanto tempi impiega il merge sort per ordinare \\(S\\)? e il quick sort?
