Capitolo Quattro: pile, code, dequeue
*************************************


Sezione 1. Il tipo di dato astratto pila
----------------------------------------

Una *pila* è una collezione di oggetti inseriti e rimossi secondo il principio *last-in-first-out* 
(LIFO, l'ultimo entrato è il primo a uscire).
Un utente può accedere alla pila, aggiungendo un elemento o rimuovendo quello inserito più recentemente.
Questo oggetto è chiamato il *top* della pila.
Quindi le operazioni fondamentali sulla pila riguardamo il *pushing* di un oggetto nella pila, 
o il *popping* di un oggetto dalla pila.
L'esempio banale è la pila dei piatti in un distributore automatico. 

La pila è da considerare una delle più importanti strutture dati, ed è usata in molte applicazioni.
Ad esempio, la lista dei siti visitati più di recente da un browser è implementata come una pila;
ogni volta che un sito è visitato il suo indirizzo è inserito nello stack, e ogni volta che il pulsante 
"back" è usato, il sito visitato è estratto dalla pila.
Un altro esempio è il meccanismo di "undo" degli editor di testo.  

Formalmente, uno stack \\(S\\) è un tipo di dato astratto che supparta due metodi 
(ogni volta che \\(S\\) è vuoto si verifica un errore):

- **S.push(e)**: aggiunge l'elemento e al top di S;
- **S.pop()**: restituisce l'elemento al top di S e lo rimuove.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura41.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Altri metodi accessori sono definiti:

- **S.top()**: restituisce l'elemento al top di S, senza rimuoverlo;
- **S.is_empty()**: restituisce True se S non contiene elementi;
- **len(S)**: restituisce il numero degli elementi in S.

Una nuova pila è creata vuota, e non c'è limite alla sua capacità. Gli elementi di una pila
possono avere tipo arbitrario.

Sezione 1.1 Implementazione con una lista di Python
===================================================

Una pila può essere implementata con una lista di Python.
La classe  ``list`` supporta l'aggiunta di un elemento alla sua fine (con il metodo ``append``), 
e la rimozione del suo ultimo elemento (con il metodo ``pop``);
se definiamo il top della pila come la fine della lista, i metodi della pila possono essere
definiti semplicemente.
Appare naturale pensare che la classe ``list`` potrebbe essere usata direttamente come una pila,
senza definire la classe  ``stack``;
ma ciò violerebbe l'astrazione del nuovo ADT, dato che la classe ``list`` 
include più metodi rispetto a quelli di una pila. 

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura42.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Di seguito mostriamo come usare una lista per implementare una pila.
In particolare, definiamo una nuova classe con le stesse funzionalità di una classe già esistente,
riarrangiate in modo più conveniente. 
Definiamo la classe  ``stack`` in moda tale da contenere una istanza della classe 
``list`` come un campo nascosto, e implementiamo ogni metodo della nuova classe usando
i metodi della variabile di istanza nascosta.
L'adattamento è evidenziato nella seguente tabella.

============= ==========================
Metodi pila    Realizzazione con la lista
============= ==========================
S.push(e)      L.append(e)
S.pop( )       L.pop( )
S.top()        L[−1]
S.is empty( )  len(L) == 0
len(S)         len(L)
============= ==========================

La definizione formale è nel seguente frammento di codice::

 1  class ArrayStack:
 2    # LIFO Stack implementation using a Python list as storage
 3
 4    # Creates an empty stack
 5    def __ init __ (self):
 6      self._data = [ ]
 7
 8    # Returns the number of elements in the stack
 9    def __ len __ (self):
 10     return len(self._data)
 11
 12   # Returns True if the stack is empty
 13   def is_empty(self):
 14     return len(self._data) == 0
 15
 16   # Adds element e to the top of the stack
 17   def push(self, e):
 18     self._data.append(e)
 19
 20   # Returns (but don't remove) the element at the top of the stack
 21   # raising an exception if the stack is empty
 22   def top(self):
 23     if self.is_empty( ):
 24       raise Empty( 'Stack is empty' )
 25     return self._data[−1]
 26
 27   # Remove and return the element from the top of the stack
 28   # raising an exception if the stack is empty
 29   def pop(self):
 30     if self.is_empty( ):
 31       raise Empty( 'Stack is empty' )
 32     return self._data.pop( )

Si osservi che quando l'utente chiama ``pop`` o ``top`` e la pila è vuota, deve verificarsi un errore.
Per ottenere questo si definisce una eccezione ``Empty`` come segue::

 1  # Error when trying to access an element from an empty list
 2  class Empty(Exception):
 3    pass

Mostriamo un esempio di uso della classe pila, con operazioni, contenuto e output, rispettivamente.

==================== ==================== =======
Operazioni           Contenuto della pila Output
==================== ==================== =======
S=Stack( )           [ ]
S.push(5)            [5]
S.push(3)            [5, 3]
print(len(S))        [5, 3]                2
print(S.pop( ))      [5]                   3
print(S.is empty())  [5]                   False
print(S.pop( ))      [ ]                   5
print(S.is empty())  [ ]                   True
S.push(7)            [7]
S.push(9)            [7, 9]
print(S.top( ))      [7, 9]                9
S.push(4)            [7, 9, 4]
print(len(S))        [7, 9, 4]             3
==================== ===================== =======

L'analisi della precedente implementazione della pila (basata su array) rispecchia l'analisi della classe lista.
``top``, ``is_empty``, e ``len`` usano tempo costante, nel caso peggiore;
una chiamata a ``push`` o ``pop`` usa tempo costante, ma un caso peggiore \\(O(n)\\)
è possibile (\\(n\\) è il numero degli elementi nella pila), quando una operazione provoca il ridimensionamento
dell'array sottostante alla lista. Lo spazio usato da una pila è \\(O(n)\\).

La pila del precedente frammento di codice comincia con una lista vuota ed è espansa, se necessario;
una implementazione più efficiente può essere ottenuta costruendo una lista con lunghezza iniziale \\(n\\) 
se, per qualche motivo, sappiamo già che la pila raggiungerà quella dimensione. 
In questo caso il costruttore potrebbe accettare un parametro che specifica la capacità massima dalla pila,
e potrebbe costruire la lista iniziale di quella lunghezza. Ciò implica che la dimensione della pila
non è più sinonimo della lunghezza della lista, e le pop e le push non richiedono più alcun
cambiamento di questa lunghezza.


Sezione 1.2 Esempio: rovesciare una pila
========================================

Una pila può essere usata come strumento per rovesciare una sequenza di dati.
Se i valori 1, 2, e 3 sono inseriti nella pila in questo ordine, allora saranno estratti nell'ordine 3, 2, e 1.
Questo comportamento può essere usato in una grande varietà di casi.
Ad esempio, supponiamo di voler stampare le linee di un file in ordine inverso;
possiamo leggere ogni linea e inserirla in una pila, e poi stampare le linee
nell'ordine in cui sono estratte. Una implementazione di questo processo è nel codice seguente::

 1  def reverse_file(filename):
 2    # Overwrite a file with its contents line-by-line reversed
 3    S = ArrayStack()
 4    original = open(filename)
 5    for line in original:
 6      S.push(line.rstrip( '\n' ))   # re-insert newlines when writing
 7    original.close( )
 8
 9    # overwrite with contents in LIFO order
 10   output = open(filename,'w' )       # reopening file overwrites original
 11   while not S.is_empty( ):
 12     output.write(S.pop( ) + '\n' )       # reinsert newline characters
 13   output.close( )

Sezione 1.3 Match di parentesi
==============================

Due utili applicazione della pila sono relative al test delle coppie di delimitatori, come le parentesi o i tag HTML.
Consideriamo le espressioni aritmetiche che possono contenere differenti tipi di simboli di raggruppamento,
come le parentesi tonde "(" e ")", le graffe "{" and "}", e le quadre "[" and "]",
in cui ogni parentesi aperta deve avere un corretto match con una corrispondente parentesi chiusa.
Ad esempio, le stringhe  ( )(( )){([( )])} e ((( )(( )){([( )])})) sono corrette, 
mentre )(( )){([( )])}, ({[ ])}, e ( non lo sono.

Un compito importante è essere certi che il matching fra le parentesi sia corretto.
Nel codice seguente il lettore può trovare una implementazioni in Python di questo algoritmo:: 

 1  def ismatched(expr):
 2    # Returns True if all delimiters match properly; False otherwise
 3    lefty = '({['           # opening delimiters
 4    righty = ')}]'          # closing delimiters
 5    S = ArrayStack()
 6    for c in expr:
 7      if c in lefty:
 8        S.push(c)           # push left delimiter on stack
 9      elif c in righty:
 10       if S.is_empty( ):
 11         return False          # nothing to match with
 12       if righty.index(c) != lefty.index(S.pop( )):
 13         return False          # mismatched
 14    return S.is_empty( )       # all symbols matched

Data una sequenza di caratteri in input, viene fatta una scansione da sinistra a destra.
Ogni volta che si incontra un simbolo aperto, esso è inserito nella pila \\(S\\);
ogni volta che si incontra un simbolo chiuso, si effettua una pop da \\(S\\) (assumendo \\(S\\) non vuoto)
e si verifica che i due simboli siano una coppia valida.
Se si raggiunge la fine della espressione e lo stack è vuoto, allora l'espressione originaria era corretta.
Altrimenti, ci deve essere un simbolo aperto sulla pila senza un match con un simbolo chiuso.

Se la lunghezza dell'espressione originaria è \\(n\\), l'algoritmo farà al più \\(n\\) 
push e \\(n\\) pop, con tempo di esecuzione in \\(O(n)\\).

Sezione 1.4 Match di tag HTML 
=============================

Un'altra applicazione utile è la verifica dei delimitatori in linguaggi come HTML o XML.
HTML è un linguaggio di markup, e rappresenta il formato standard per i documenti in Internet;
XML è un extensible markup language.

In un documento HTML le prozioni di testo sono delimitate da tag.
Un tag di apertura è scritto come <name>, mentre il corrispondente tag di chiusura è scritto come </name>.
Tag comuni sono <body> (il corpo del documento), <h1> (un header di sezione), <center> (testo centrato),
<p> (un paragrafo), insieme ad altri tag per le liste ordinate o numerate.
Ogni tag di apertura deve avere un match con il relativo tag di chiusura, come nel precedente 
problema del matching fra parentesi. Il seguente codice verifica che un documento HTML ha un corretto matching
dei tag; i tag di apertura sono inseriti nella pila, e viene fatto il match con i tag di chiusura
via via che essi sono estratti dalla pila.

L'algoritmo è eseguito in tempo \\(O(n)\\), con \\(n\\) il numero di caratteri nel codice HTML::

 1  def is_matched_html(raw):
 2    # Returns True if all HTML tags match properly; False otherwise
 3    S = ArrayStack()
 4    j = raw.find('<')                 # find first < character
 5    while j != −1:
 6      k = raw.find('>' , j+1)         # find next > character
 7      if k == −1:
 8        return False                  # invalid tag
 9      tag = raw[j+1:k]                # strip away < >
 10     if not tag.startswith( '/' ):   # this is an opening tag
 11       S.push(tag)
 12     else:                           # this is a closing tag
 13       if S.is_empty( ):
 14         return False                # nothing to match with
 15       if tag[1:] != S.pop():
 16         return False                # mismatched delimiter
 17     j = raw.find( '<' , k+1)          # find next < character (if any)
 18   return S.is empty( )              # were all opening tags matched?



Sezione 2 Il tipo di dato astratto coda
---------------------------------------

Una *coda* è una collezione di oggetti che sono inseriti e rimossi secondo il principio *first-in first-out* 
(FIFO, il primo a entrare è il primo a uscire). Quindi, gli elementi entrano nella coda dal fondo e 
sono rimossi dalla fronte.
A parte le ovvie complicazioni implementative, le code sono usate da molti dispositivi, come stampanti,
web server o sistemi operativi.

Per applicare il proncipio FIFO, il tipo di dato astratto coda definisce una collezione di oggetti
in cui l'accesso e la cancellazione sono consentite sul primo elemento della coda stessa, 
mentre l'inserimento è consentito solo al fondo. 

Formalmente una coda \\(Q\\) è una struttura di dati astratta che supporta due metodi
(se \\(Q\\) è vuota si verifica un errore):

- **Q.enqueue(e)**: aggiunge l'elemento o in fondo alla coda Q;
- **Q.dequeue()**: rimuove e restituisce il primo elemento dalla coda Q.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura43.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

L'ADT coda include i seguenti metodi di supporto:

- **Q.first()**: restituisce il primo elemento di Q, senza rimuoverlo;
- **Q.is_empty( )**: restituisce True se la coda Q non contiene elementi;
- **len(Q)**: restituisce il numero degli elementi di Q.

Una nuova coda è creata vuota, e non c'è limite alla sua capacità. 
Gli elementi di una coda possono avere tipo arbitrario.

Sezione 2.1 Implementazione con una lista di Python
===================================================

L'approccio più semplice alla implementazione di una coda sarebbe quello di adattare una classe ``list`` 
di Python, analogamente a quanto fatto per la pila. 
Potremmo accodare un elemento e chiamando  ``append(e)`` per aggiungerlo alla fine della lista;
e potremmo usare la sintassi ``pop(0)`` (e non ``pop()``) per rimuovere il primo elemento della lista.
Tuttavia, questa soluzione è altamente inefficiente; quando ``pop`` è invocato su una lista con un indice 
non di default, tutti gli elementi oltre quell'indice devono essere shiftati a sinistra, per riempire
il vuoto nella sequenza causato dalla pop. 
Una chiamata a ``pop(0)`` causa sempre lo scenario peggiore, in quanto l'intera lista deve essere
shiftata (in tempo \\(O(n)\\) ).

Un'altra soluzione consente di evitare la chiamata a ``pop(0)``.
Potremmo rimpiazzare l'elemento eliminato dall'array con un riferimento a ``None`` 
(evitando lo shifting dannoso), e immagazzinare l'indice dell'elemento che è effettivamente
in testa alla coda in una variabile. Questo algoritmo ha tempo di esecuzione \\(O(1)\\).

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura44.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Una controindicazione a questo approccio consiste nel fatto che la lunghezza della lista sottostante 
può diventare arbitrariamente grande, anche se gli elementi effettivamente in coda sono pochi.
Ad esempio, si consideri il caso di sequenze ripetute di enqueue e dequeue di elementi
(cioè, la coda ha una piccola dimensione, ma è usata per molto tempo);
col passare del tempo la dimensione della lista sottostante cresce come \\(O(m)\\),
con \\(m\\) il numero totale delle operazioni di enqueue a partire dalla crazione della lista,
piuttosto che come il numero effetivo di elementi in coda.

Section 2.2 Implementazione con un array circolare
==================================================

Una implementazione efficiente può essere creata unsando un *array circolare*.
Si assuma che l'array sottostante abbia lunghezza fissata \\(N\\), più grande del numero
di elementi nella coda. 
Consentiamo alla testa della coda di spostarsi verso destra (come nella precedente implementazione),
ma consentiamo anche che un nuovo elemento sia messo in coda nel primo elemento libero dell'array,
partendo dalla testa fino all'indice \\(N−1\\), e continuando con l'indice 0, poi 1, se gli elementi
corrispondenti sono liberi.

La sola richiesta consiste in calcolare l'indice della testa \\(f\\) quando un elemento è eliminato dalla coda.
tale valore è \\((f+1)\\% N\\) , con \\(a\\% b\\) il resto di \\(a\\) diviso \\(b\\).
Ad esempio, se la lista ha lunghezza pari a 10, e indice della testa 7, il nuovo indice della testa 
è \\((7+1) \\% 10\\), pari a 8, come atteso. 
Se l'indice della testa è 9 (l'ultimo dell'array) il nuovo indice sarà pari a 0,
e quindi dovremo accodare il nuovo elemento nella prima posizione dell'array.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura45.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Una implementazione dell'ADT ``queue`` che usa una lista circolare di Python è nel codice che segue.
La nuova classe conserva tre variabili di istanza:

- _data: un riferimento a una istanza di lista con capacità fissata;
- _size: il numero di elementi effettivamente in coda;
- _front: l'indice del primo elemento della coda in _data.

Sebbene la coda abbia inizialmente dimensione zero, riserviamo una certa dimensione per
memorizzare i dati, con l'indice della testa pari a zero.
Una eccezione è sollevata quando ``front`` o ``dequeue`` sono chiamate su una coda vuota.
Nel codice seguente forniamo una implementazione di una coda che sfrutta le idee precedenti::

 1  class ArrayQueue:
 2    # queue implemented with a list
 3    DEFAULT = 10          # capacity of all queues
 4
 5    def __init__(self):
 6      self._data = [None]*ArrayQueue.DEFAULT
 7      self._size = 0
 8      self._front = 0
 9
 10   def __len__(self):
 11     return self._size
 12
 13   def is_empty(self):
 14     return self._size == 0
 15
 16   def first(self):
 17     if self.is_empty( ):
 18       raise Empty('Queue is empty')
 19     return self._data[self._front]
 20
 21   def dequeue(self):
 22     if self.is_empty( ):
 23       raise Empty('Queue is empty')
 24     element = self._data[self._front]
 25     self._data[self._front] = None
 26     self._front = (self._front + 1) % len(self._data)
 27     self._size −= 1
 28     return element
 29
 30   def enqueue(self, e):
 31     if self._size == len(self._data):
 32       self._resize(2*len(self.data))           # double the array size
 33     position = (self._front + self._size) % len(self._data)
 34     self._data[position] = e
 35     self._size += 1
 36
 37   def _resize(self, capacity):
 38     old = self._data                   # keeps track of existing list
 39     self._data = [None]*capacity       # allocate list with new capacity
 40     walk = self._front
 41     for k in range(self._size):
 42       self._data[k] = old[walk]        # shifts the indices
 43       walk = (1 + walk) % len(old)     # use old size
 44     self._front = 0                    # realigs front

Il metodo ``enqueue`` aggiunge un nuovo elemento alla coda.
Per farlo, l'indice appropriato è calcolato con ``position = (self._front + self._size) % len(self._data)``.
Ad esempio, considerata una coda con capacità 10, dimensione effettiva 3, e primo elemento all'indice 5,
il nuovo elemento sarà inserito all'indice (5+3) % 10 = 8.
se la coda ha 3 elementi con il primo di essi all'indice 8, il nuovo elemento è inserito all'indice
(8+3) % 10 = 1, poiché i tre elementi occupano le posizioni 8, 9 e 10

Se è invocato il metodo ``dequeue``, l'elemento ``element = self._data[self._front]`` sarà restituito e rimosso,
e ``self._front`` è lindice di tale valore. 
Il valore di ``_front`` è aggiornato per riflettere la rimozione dell'elemento, e il secondo elemento 
diventa il nuovo primo; a causa della configurazione circolare questo valore è calcolato via aritmetica modulare.

Il metodo ``resize`` è invocato quando la dimensione della coda e quella della lista sottostante
(\\(N\\)) sono uguali, raddoppiando la lughezza della lista.
Ciò è realizzato creando un riferimento temporaneo alla vecchia lista, allocando una nuova lista con dimensione 
doppia rispetto alla vecchia, e copiando i riferimenti dalla vecchia alla nuova lista.
Durante la copia la testa della coda è riallineata con l'indice 0 del nuovo array.

Un miglioramento del codice precedente si ottiene riducendo la dimensione dell'array di metà quando il numero
di elementi è uguale o minore di un quarto della capacità della lista. 
Infatti, l'implementazione fornita espande l'array ogni volta che la piena capacità è raggiunta,
ma non lo riduce mai. Ciò implica che la capacità dell'array sottostante è proporzionale al numero massimo
di elementi che sono stati immagazzinati nella coda, e non al numero effettivo degli elementi in coda.

Per ottenere questa proprietà è possibile aggiungere le seguenti due linee di codice nel metodo
``dequeue``, prima della linea ``return element``::

 1  if 0 < self._size < len(self._data) / 4:
 2    self._resize(len(self._data) / 2)

Infine, osserviamo che tutti i metodi si basano su un numero costante di statement che riguardano operazioni
aritmetiche, confronti e assegnamenti. Ogni metodo è eseguito, nel caso peggiore, in tempo \\(O(1)\\), 
tranne per la utility ``resize``.


Sezione 3 Code double-ended
---------------------------

Una  *coda double-ended* (double-ended queue, o deque) è una struttura dati che supporta l'inserimento e la rimozione
a entrambi i capi della coda. Questo ADT è più generale della pila e della coda.

Formalmente, l'ADT ``deque`` è definito in modo da supportare i seguenti metodi
(se la deque è vuota si verifica un errore):

- **D.add_first(e)**: aggiunge l'elemento e in fronte alla deque D;
- **D.add_last(e)**: aggiunge l'elemento e in fondo alla deque D;
- **D.delete_first( )**: rimuove e restituisce il primo elemento dalla deque D;
- **D.delete_last( )**: rimuove e restituisce l'ultimo elemento dalla deque D.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura46.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

La deque include i seguenti metodi accessori:

- **D.first()**: restituisce (ma non rimuove) il primo elemento di D;
- **D.last()**: restituisce (ma non rimuove) l'ultimo elemento di D;
- **D.is_empty( )**: restituisce True se la deque D non contiene elementi;
- **len(D)**: restituisce il numero degli elementi in D.

Sezione 3.1 Implementazione con array circolare
===============================================

L'ADT ``deque`` può essere implementato in modo analogo alla coda fornita in precedenza; 
tre variabili di istanza devono essere conservate: ``_data``, ``_size``, and ``_front``.
L'indice del fondo della deque può essere calcolato via aritmetica modulare.
Ad esempio, l'inplementazione del metodo ``last()`` usa l'indice 
``back = (self._front + self._size − 1) % len(self._data)``.
La nuova implementazione di ``deque.add_last`` è essenzialmente la stessa di ``queue.enqueue``,
inclusa la utility ``resize``.
Similarmente, l'implementazione del metodo ``deque.delete_first`` è la stessa di ``queue.dequeue``.

L'implementazione di ``add_first`` e ``delete_last`` usa tecniche simili.
Una chiamata a ``add_first`` usa l'aritmetica modulare per decrementare l'indice, con 
``self._front = (self._front − 1) % len(self._data)``.
L'efficienza di ``deque`` è simile a quella di ``queue``;
tutte le operazioni hanno tempo di esecuzione \\(O(1)\\), con limite ammortizzato
per le operazione che possono cambiare la dimensione della lista sottostante.