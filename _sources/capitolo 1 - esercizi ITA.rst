

Capitolo Uno: Analisi degli algoritmi - esercizi e sfide
********************************************************


Sezione 1. Revisione della teoria
---------------------------------

Completare i seguenti frammenti di codice (psso essere trovati anche nel Capitolo Uno), ed eseguire i programmi.
Per ogni programma fornire (1) un'analisi asintotica e (2) un'analisi sperimentale del tempo consumato,
usando la funzione ``time()`` del modulo ``time``, che restituiscei secondi trascorsi da un dato istante;
eseguire ``time()`` prima e dopo ogni algoritmo, per misurare il tempo passato cone differenza dei due valori.
Si usin Codelens per analizzare ogni passo del codice.

Il primo programma ``sumofn`` è un esempio di come fare questo; 
stampa dieci volte il risultato (la somma dei primi 1,000 interi) e la quantità di tempo
richiesta per il calcolo. Verificare cosa accade se è calcolata la somma del primi 10,000 interi,
dal punto di vista dell'analisi asintotica (il tempo di ogni esecuzione dovrebbe richiedere 10 volte
più secondi).

.. activecode:: sum-of-n2
   :language: python
   :caption: Prints the sum of the first n integers

   import time

   def sumofn(n):
     start = time.time()
     sum = 0
     for i in range(1,n+1):
       sum = sum + i
     end = time.time()
     return sum,end-start

   for i in range(10):
     print("Sum is %d required %10.7f seconds"%sumofn(1000))



.. activecode:: max-list
   :language: python
   :caption: Returns the maximun element from a list

   def find_max(data):
     # Return the maximum element from a list
     biggest = data[0]
       for val in data:
         if val > biggest:
           biggest = val
       return biggest

      # INSERT NEW CODE HERE



.. tabbed:: prefix-average

   .. tab:: Quadratic time

      .. activecode:: prefix-average1
         :language: python
         :caption: Quadratic time solution of the prefix average problem

         def prefix_average(S):
           n = len(S)
           A = [0]*n                # list of n zeros
           for j in range(n):
             total = 0
             for i in range(j + 1):
                total += S[i]
              A[j] = total / (j+1)    # the j-th average
           return A

          # INSERT NEW CODE HERE


   .. tab:: Linear time

       .. activecode:: prefix-average2
         :language: python
         :caption: Linear time solution of the prefix average problem

         def prefix_average(S):
           n = len(S)
           A = [0]*n                # list of n zeros
           for j in range(n):
             total = 0
             for i in range(j + 1):
                total += S[i]
              A[j] = total / (j+1)    # the j-th average
           return A

          # INSERT NEW CODE HERE



.. tabbed:: element-uniqueness

   .. tab:: Quadratic time

      .. activecode:: element-uniqueness1
         :language: python
         :caption: Quadratic time solution of the element uniqueness problem

         def unique1(S):
           for j in range(len(S)):
             for k in range(j+1, len(S)):
               if S[j] == S[k]:
                 return False        # there is a duplicate pair
           return True               # all elements are distinct

            # INSERT NEW CODE HERE

   .. tab:: Logaritmic time

       .. activecode:: element-uniqueness2
         :language: python
         :caption: nlogn time solution of the element uniqueness problem

         def unique2(S):
           temp = sorted(S)            # sorting of S
           for j in range(1, len(temp)):
             if temp[j-1] == temp[j]:
               return False            # there is duplicate pair
           return True                 # all elements are distinct

                       # INSERT NEW CODE HERE




Sezione 2. Il problema dell'anagramma
-------------------------------------

Date due stringhe di uguale lunghezza, la solutione del *problema dell'anagramma* consiste nello scrivere
una funzione che restituisce ``true`` se la prima stringa è un ri-arrangiamento della seconda, 
``false`` altrimenti.
Ci sono numerose soluzioni al problema, con differenti complessità temporali.
Di seguito ne introduciamo alcune, provando a intuire il relativo algoritmo.

Sezione 2.1 Marcare i caratteri
==================================

Se le stringhe hanno uguale lunghezza e se ogni carattere della prima è presente nella seconda,
allora le due stringhe sono anagrammi.
Ogni volta che un carattere della prima stringa è trovato nella seconda, deve essere *marcato*, nel senso che
sarà rimpiazzato con ``None`` nella seconda stringa.
Se tutti i carattei sono marcati, allora la funzione restituisce ``True``.
La funzione è nel seguente ActiveCode; provare a cambiare le stringhe 
per esaminare il comportamento della funzione.

.. reveal:: check-off
    :showtitle: Show solution
    :hidetitle: Hide solution

    .. activecode:: checking-off
       :language: python
       :caption: Detects anagrams checking-off the characters

       def anagramdetection1(s1,s2):
         stillgood = True
         if len(s1) != len(s2):
           stillgood = False
         alist = list(s2)

         pos1 = 0
         while pos1 < len(s1) and stillgood:
           pos2 = 0
           found = False
           while pos2 < len(alist) and not found:
               if s1[pos1] == alist[pos2]:
                 found = True
               else:
                 pos2 = pos2 + 1

           if found:
             alist[pos2] = None
           else:
             stillgood = False
           pos1 = pos1 + 1
       return stillgood

       print(anagramSolution1('ajcd','dcba'))


Si noti che per l'\\(i\\)-mo carattere nella lista ``s1`` ci saranno \\(i\\) visite in ``s2``, per \\(i=0 \\ldots n\\).
Così, il numero totale delle visite è la somma degli interi da 1 a \\(n\\), e quindi il consumo di tempo
è \\(O(n^2)\\).


Sezione 2.2 Ordinare le stringhe
================================

Un'altra soluzione consiste nell'ordinare le due stringhe alfabeticamente, convertendo ogni stringa in una
lista e usando il metodo built-in ``sort``; Se sono anagrammi, saranno la stessa stringa dopo l'ordinamento.
IL seguente ActiveCode mostra la soluzione.
Provare a concepire l'algoritmo prima di vedere la soluzione. 
Provare a cambiare le stringhe per esaminare il comportamento della funzione.

.. reveal:: sorting
    :showtitle: Show solution
    :hidetitle: Hide solution

    .. activecode:: sorting-the-strings
       :language: python
       :caption: Detects anagrams sorting the strings

       def anagramdetection2(s1,s2):
         alist1 = list(s1)
         alist2 = list(s2)

         alist1.sort()
         alist2.sort()

         pos = 0
         matches = True

         while pos < len(s1) and matches:
           if alist1[pos]==alist2[pos]:
              pos = pos + 1
           else:
              matches = False

         return matches

         print(anagramSolution2('abcde','edcba'))

La sezione ``while`` del programma precedente impiega tempo \\(O(n)\\) per essere eseguita,
poiché ci sono  \\(n\\) confronti fra i carattri dopo l'ordinemento.
Un generico ordinamento richiede tempo \\(O(n^2)\\) or \\(O(n logn)\\), e quindi questa operazione
è dominante rispetto all'iterazione.


Sezione 2.3 Contare e confrontare
=================================

Se ledue stringhe sono anagrammi, avranno lo stesso numero di a, b, c, e così via.
Potremmo innanzi tutto contare il numero di volte che ogni carattere è presente nella stringa,
usando una lista di 26 contatori, uno per ogni possibile carattere.
Ogni volta che si incontra un carattere, il contatore nella posizione appropriata è incrementato.
Se le due liste di contatori sono uguali, allora le stringhe sono anangrammi.

.. reveal:: counting
    :showtitle: Show solution
    :hidetitle: Hide solution

    .. activecode:: counting-the-characters
       :language: python
       :caption: Detects anagrams counting the characters

       def anagramSolution4(s1,s2):
         c1 = [0]*26
         c2 = [0]*26

         for i in range(len(s1)):
           pos = ord(s1[i])-ord('a')
           c1[pos] = c1[pos] + 1

         for i in range(len(s2)):
           pos = ord(s2[i])-ord('a')
           c2[pos] = c2[pos] + 1

         j = 0
         stillOK = True
         while j<26 and stillOK:
           if c1[j]==c2[j]:
              j = j + 1
           else:
              stillOK = False

         return stillOK

       print(anagramSolution4('ajcd','dcba'))

Le prime due iterazioni, usate per contare i caratteri, sono basate sulla lunghezza di ogni stringa;
la terza iterazione è formata da 26 passi. Il consumo di tempo totale è \\(O(n)\\), cioè linear-time.
Si noti che anche se questa è una soluzione ottimale, il risultato è ottenuto usando due piccole liste
addizionali. Occorre quindi accettare un trade-off fra tempo e spazio utilizzati.


Sezione 2.4 Trovare tutti i possibili anangrammi
================================================

Un approccio di *forza bruta* per risolvere questo problema prova a generare una lista di tutte le possibili
stringhe usando i caratteri di  ``s1`` e verificando se ``s2`` è una di esse.
Il numero totale di stringhe è \\(n∗(n−1)∗(n−2)∗ \\ldots ∗3∗2∗1\\), cioè \\(n!\\).
Il fattoriale cresce più velocemente di \\(2^n\\), e quindi un approccio del genere non è auspicabile.


Sezione 3. Esercizi e autovalutazione
--------------------------------------

Fornire un ordine di grandezza \\(O\\), in termini di \\(n\\), del tempo di esecuzione 
dei seguenti frammenti di codice.
(Suggerimento: considerare il numero di volte che un ciclo è eseguito e quante operazioni primitive sono
presenti in ogni iterazione)

**Esercizio 3.1**::

 1  def example1(S):
 2  ”””Return the sum of the elements in sequence S.”””
 3    n = len(S)
 4    total = 0
 5    for j in range(n):     # loop from 0 to n-1
 6      total += S[j]
 7    return total

**Esercizio 3.2**::

 1  def example2(S):
 2  ”””Return the sum of the elements with even index in sequence S.”””
 3    n = len(S)
 4    total = 0
 5    for j in range(0, n, 2):     # increment of 2
 6      total += S[j]
 7    return total

**Esercizio 3.3**::

 1  def example3(S):
 2  ”””Return the sum of the prefix sums of sequence S.”””
 3    n = len(S)
 4    total = 0
 5    for j in range(n):         # loop from 0 to n-1
 6      for k in range(1+j):     # loop from 0 to j
 7        total += S[k]
 8    return total

**Esercizio 3.4**::

 1  def example4(S):
 2  ”””Return the sum of the prefix sums of sequence S.”””
 3    n = len(S)
 4    prefix = 0
 5    total = 0
 6    for j in range(n):
 7      prefix += S[j]
 8      total += prefix
 9    return total

**Esercizio 3.5**::

 1  def example5(A, B): # assume that A and B have equal length
 2  ”””Return the number of elements in B equal to the sum of prefix sums in A.”””
 3    n = len(A)
 4    count = 0
 5    for i in range(n):         # loop from 0 to n-1
 6      total = 0
 7      for j in range(n):       # loop from 0 to n-1
 8        for k in range(1+j):   # loop from 0 to j
 9          total += A[k]
 10     if B[i] == total:
 11       count += 1
 12   return count


**Esercizio 3.5**

Un algoritmo \\(A\\) è eseguito in tempo \\(O(\\log n)\\) per ogni elemento di una sequenza 
di \\(n\\) elementi.  Qual è il tempo di esecuzione nel caso peggiore?
(Suggerimento: il calcolo che costa \\(O(\\log n)\\) è fatto \\(n\\) volte)

**Esercizio 3.6**

Data una sequenza di \\(n\\) elementi \\(S\\), l'algoritmo \\(B\\) sceglie \\(\\log n\\) elementi
in \\(S\\) in modo casuale, ed esegue un calcolo in tempo \\(O(n)\\) per ognuno di essi.
Qual è il tempo di esecuzione nel caso peggiore? 
(Suggerimento: il calcolo che costa \\(O(n)\\) è fatto \\(\\log n\\) volte)

**Esercizio 3.7**

Data una sequenza di \\(n\\) interi \\(S\\), l'algoritmo \\(C\\) esegue
un calcolo in tempo \\(O(n)\\) per ogni numero pari in \\(S\\), 
e un calcolo in tempo \\(O(\\log n)\\) per ogni numero dispari in \\(S\\). 
Quali sono i tempi di esecuzione di \\(C\\) nel migliore e nel peggior caso?
(Suggerimento: considerare i casi per cui tutti gli elementi di \\(S\\) sono pari o sono dispari)

**Esercizio 3.8**

Data una sequenza di \\(n\\) elementi \\(S\\), lalgoritmo \\(D\\) chiama l'algoritmo \\(E\\) 
su ogni elemento \\(S[i]\\). L'algoritmo \\(E\\) è eseguito in tempo \\(O(i)\\) quando è chiamato su \\(S[i]\\).
Qual è il tempo di esecuzione di \\(D\\) nel caso peggiore?
(Suggerimento: caratterizzare il tempi di esecuzione di \\(D\\) usando una sommatoria)

**Esercizio 3.9**

Descrivere un algoritmo efficiente per trovare i dieci più grandi elementi in una sequenza di lunghezza \(n\\). 
Qual è il tempo di esecuzione? 
(Suggerimento: 10 è un numero costante)

**Esercizio 3.10**

Descrivere un algoritmo per trovare minimo e massimo di \\(n\\) numeri usando meno di \\(3n/2\\) confronti.
(Suggerimento: costruire un gruppo di candidati per il minimo e un gruppo di candidati per il massimo)

**Esercizio 3.11**

Una sequenza \\(S\\) contiene \\(n−1\\) interi unici nell'intervallo \\([0,n−1]\\), cioè esiste un solo numero
dell'intervallo che non appartiene a \\(S\\). 
Progettare un algoritmo con tempo di esecuzione \\(O(n)\\) che trovi il numero.
Si può usare uno spazio addizionale \\(O(1)\\).
(Suggerimento: usare una funzione degli interi in \\(S\\) che identifichi immediatamente quale numero manca)

**Esercizio 3.12**

Una sequenza \\(S\\) contiene \\(n\\) interi presei da un intervallo \\([0,4n]\\), con ripetizioni consentite.
Descrivere un algoritmo eficiente per trovare il valore intero \\(k\\) che è presente più spesso in \\(S\\). 
Qual è il tempo di esecuzione dell'algoritmo?
(Suggerimento: usare un array che tenga il conto per ogni valore)
