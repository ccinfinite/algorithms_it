
Capitolo Due: Ricorsione - esercizi e sfide
*******************************************


Sezione 1. Revisione della teoria
---------------------------------

Completare ed eseguire i frammenti di codice delle prossime sezioni (possono essere trovari anche nel Capitolo Due).

**Esercizio 1.1**

La prima soluzione per trovare un valore target in una sequenza non ordinata è la *ricerca sequenziale*.
Un ciclo è utilizzato per esaminare ogni elemento, fino a che il target è trovato o l'insieme dei dati
è esaurito. 
La *ricerca binaria* è un algoritmo che trova un valore target all'interno di una sequenza ordinata e 
indicizzabile di  \\(n\\) elementi, usando la ricorsione in modo più efficiente.
Utilizzare la funzione definita nel seguente ActiveCode per trovare un valore target in un array di interi
che è stato ordinato in precedenza.

.. activecode:: binary-search
   :language: python
   :caption: Function for the binary search

   def binary_search(data, target, low, high):
     # Return True if target is found in data, between low and high
     if low > high:
       return False               # no match found
     else:
       mid = (low + high) / 2
       if target == data[mid]:    # found a match
         return True
       elif target < data[mid]:
         return binary_search(data, target, low, mid - 1)   # call on the left portion
       else:
         return binary_search(data, target, mid + 1, high)  # call on the right portion

        # INSERT NEW CODE HERE


**Esercizio 1.2**

Si consideri il problema di rovesciare gli elementi di \\(A\\), una sequenza indicizzata;
il primo diventa l'ultimo, il secondo diventa il penultimo, e così via.
Una implmementazione ricorsiva è data di seguito. Utilizzarla per rovesciare un array.

.. activecode:: reverse-a-sequence
   :language: python
   :caption: Function for reversing a sequence of elements

   def reverse(A, first, last):
     # Reverse elements in A, between first and last
     if first < last - 1:                           # if there are at least 2 elements
       A[first], A[last-1] = A[last-1], A[first]    # swap first and last
       reverse(A, first+1, last-1)

        # INSERT NEW CODE HERE


**Esercizio 1.3**

Si consideri il problema di sommare gli elementi di una sequenza indicizzata \\(A\\).
La summa di tutti gli \\(n\\) integeri in \\(A\\) è pari alla somma dei primi \\(n-1\\) integeri in \\(A\\),
più il suo ultimo elemento. Forniamo il codice Python di seguito; usarlo su una sequenza di elementi.

.. activecode:: linear-sum
   :language: python
   :caption: Linear sum of a sequence of elements

   def linear_sum(A, n):
     # Returns the sum of the first n numbers of A
     if n == 0:
       return 0
     else:
       return A[n-1] + linear_sum(A, n-1)

    # INSERT NEW CODE HERE


Invece di procedere come sopra, calcoliamo la somma degli elementi della prima metà di \\(A\\) e la somma 
degli elementi della seconda metà di \\(A\\), ricorsivamente, e poi sommiamo questi numeri.

.. activecode:: binary-sum
   :language: python
   :caption: Binary sum of a sequence of elements

   def binary_sum(A, first, last):
     # Return the sum of the numbers in A between first and last
     if first <= last:     # no elements
       return 0
     elif first == last-1:     # one element
       return A[first]
     else:
       mid = (first + last) / 2
       return binary_sum(A, first, mid) + binary_sum(A, mid, last)

    # INSERT NEW CODE HERE


**Esercizio 1.4**

I numeri di Fibonacci sono definiti come \\(F_0 =0\\), o \\(F_1 = 1\\), o \\(F_n = F_{n-2} + F_{n-1}\\).
Il codice Python per questa funzione è scritto nel seguente ActiveCode; 
verificare se è possibile eseguire questa funzione.

.. activecode:: bad-fibonacci
   :language: python
   :caption: Hard to compute Fibonacci function

   def fibonacci(n):
     if n <= 1:
       return n
     else:
       return fibonacci(n-2) + fibonacci(n-1)

    # INSERT NEW CODE HERE


Una versione più efficiente di questo programma è la seguente; confrontarla con il codice precedente.

.. activecode:: good-fibonacci
   :language: python
   :caption: Easy to compute Fibonacci function

   def good_fibonacci(n):
     if n <= 1:
       return (n,0)
     else:
       (a,b) = good_fibonacci(n-1)
       return (a+b,a)

    # INSERT NEW CODE HERE


**Esercizio 1.5**

Il problema della  *unicità dell'elemento* consiste nel decidere se tutti gli elementi di una sequenza \\(A\\) 
sono distinti l'uno dall'altro. 

La prima soluzione è un algoritmo iterativo. Usare questa funzione su un generico array.

.. activecode:: iterative-uniqueness
   :language: python
   :caption: Iterative solution to the element uniqueness problem

   def unique1(A):
     for i in range(len(A)):
       for j in range(i+1, len(A)):
         if A[i] == A[j]:
           return False
     return True

    # INSERT NEW CODE HERE

Una versione ricorsiva inefficiente è implementata di seguito.

.. activecode:: recursive-uniqueness
   :language: python
   :caption: Recursive solution to the element uniqueness problem

   def unique3(A, first, last):
     # Returns True if there are no duplicate elements in A[first:last]
     if last - first <= 1:
       return True
     elif not unique3(A, first, last-1):
       return False
     elif not unique3(a, first+1, last):
       return False
     else:
       return A[first] != A[last-1]

     # INSERT NEW CODE HERE

Una versione migliorata è fornita nello script seguente.

.. activecode:: improved-recursive-uniqueness
   :language: python
   :caption: Improved recursive solution to the element uniqueness problem

   def unique(A, first, last):
     if last-first <= 1:
       return True
     elif A[first] in A[first+1:]:
       return False
     else:
       return unique(A, first+1, last)

     # INSERT NEW CODE HERE




Sezione 2. Problemi sulla ricorsione
------------------------------------

In questa sezione mostreremo alcuni problemi classici che possono essere risolti con funzioni ricorsive;
completare ed eseguire gli ActiveCode in ogni sezione.

**Esercizio 2.1**

La  *Torre di Hanoi* è stata inventata dal matematico francese Edouard Lucas, nel 1883.
Una leggenda racconta di un tempio in cui i sacerdoti ricevevano tre pali e una pila di 64 dischi d'oro,
ciascuno più piccolo di quello sottostante.
Il loro compito era di trasferire tutti i 64 dischi da uno dei tre pali ad un'altro, seguendo due regole:
primo, solo un disco alla volta poteva essere mosso; poi, un disco più grande non poteva essere messo
in cima a uno più piccolo. Alla fine del loro compito, dice la leggenda, il mondo sarebbe scomparso.

Il numero di mosse richieste per spostare correttamente una torre di 64 dischi è \\(2^{64} -1\\), 
cioè 18.446.744.073.709.551.615.
Muovendo un disco al secondo, i sacerdoti avrebbero bisogno di 584.942.417.355 anni.

Per trovare una soluzione ricorsiva al problema, consideriamo il seguente esempio.
Abbiamo tre pali, chiamati palo uno, palo due e palo tre, e supponiamo di avere una torre di quattro dischi,
in origine sul palo uno.
Se sappiamo già come muovere una torre di tre dischi sul palo due, allora possiamo facilmente spostare
il disco in fondo sul palo tre, e poi spostare la torre di tre dischi dal palo due al palo tre.
Non sappiamo come spostare una torre di altezza tre, ma se sappiamo come spostare una torre di altezza due,
allora possiamo spostare il terzo disco da un palo all'altro, e poi spostare la torre di due da un palo
in cima al disco appena mosso.
Alla fine di questa procedura (ricorsiva) saremo in grado di spostare un singolo disco.

L'algoritmo di alto livello su come muovere una torre da un palo di partenza a un palo destinazione
usando un palo intermedio è::

 Sposta una torre di altezza height-1 su un palo intermedio, usando il palo finale
 Sposta il disco rimanente sul palo finale
 Sposta la torre di altezza height-1 dal palo intermedio al palo finale, usando il palo originario

Fino a che rispettiamo la regola che i dischi più grandi rimangono al fondo della pila
possiamo usare i tre passi precedenti, ricorsivamente. Il caso base è raggiunto quando abbiamo una torre
con un solo disco; in questo caso dobbiamo solo spostare un disco verso la sua destinazione finale.
La funzione Python è definita come segue ( si noti l'ordine di ``fromPole``, ``toPole``, e ``withPole`` 
nelle chiamate ricorsive di ``moveTower``)::

 def moveTower(height, fromPole, toPole, withPole):
    if height >= 1:
        moveTower(height-1, fromPole, withPole, toPole)
        moveDisk(fromPole, toPole)
        moveTower(height-1, withPole, toPole, fromPole)

Il codice precedente fa due diverse chiamate ricorsive: con la prima tutti i dischi tranne quello in fondo
sono spostati dalla torre iniziale a un palo intermedio;
la linea successiva sposta il disco in fondo sul palo definitivo;
la seconda chiamata ricorsiva sposta la torre dal palo intermedio in cima al disco più grande.
Il caso base è trovato quando l'altezza della torre è 0, e in questo caso non c'è nulla da fare.

La funzione ``moveDisk`` dice che sta spostando un disco da un palo ad un altro::

 def moveDisk(from,to):
   print("moving disk from", from, "to", to)

Il seguente ActiveCode fornisce l'intera soluzione.

.. activecode:: tower-of-hanoi
   :language: python
   :caption: Recursive solution for the Tower of Hanoi problem

   def moveTower(height,fromPole, toPole, withPole):
       if height >= 1:
           moveTower(height-1,fromPole,withPole,toPole)
           moveDisk(fromPole,toPole)
           moveTower(height-1,withPole,toPole,fromPole)

   def moveDisk(fp,tp):
       print("moving disk from",fp,"to",tp)

   moveTower(3,"A","B","C")



**Esercizio 2.2**

Il crivello di Eratostene è un semplice algoritmo per trovare tutti i numeri primi fino a un intero specificato \\(n\\);
una funzione ricorsiva che implementa l'algoritmo ha il seguente pseudocodice::

 a) create a list of integers 2, 3, 4, ..., n;
 b) set counter i to 2 (the first prime number);
 c) starting from i+i, count up by i and remove those numbers from the list (2*i, 3*i, ...);
 d) find the first number of the list following i; this is the next prime number;
 e) set counter i to this number;
 f) repeat steps c and d until i is greater that n.

Il seguente programma implementa il crivello iterativamente. Stampa i primi 100 numeri primi.

.. activecode:: sieve-of-eratosthenes-iterative
   :language: python
   :caption: Iterative solution for the sieve of Eratosthenes

   from math import sqrt
   def sieve(n):
     primes = list(range(2,n+1))
     max = sqrt(n)
     num = 2
     while num < max:
       i = num
       while i <= n:
         i += num
         if i in primes:
           primes.remove(i)
       for j in primes:
         if j > num:
           num = j
           break
     return primes
   print(sieve(100))

Una soluzione ricorsiva è fornita di seguito.

.. activecode:: sieve-of-eratosthenes-recursive
   :language: python
   :caption: Recursive solution for the sieve of Eratosthenes

   from math import sqrt
   def primes(n):
     if n == 0:
       return []
     elif n == 1:
       return []
     else:
       p = primes(int(sqrt(n)))
       nop = [j for i in p for j in range(i*2, n + 1, i)]
       p = [x for x in range(2, n + 1) if x not in nop]
       return p
     print(primes(100))



Sezione 3. Esercizi ed autovalutazione
--------------------------------------

**Exercise 3.1**

Scrivere un codice ricorsivo per la funzione \\(f(n)=3*n\\).
(Suggerimento: una definizione ricorsiva di questa funzione può essere \\(f(1) = 3\\) e \\( f(n+1)=f(n)+3\\) )

**Esercizio 3.2**

Scrivere un codice ricorsivo per la funzione \\(power(x,n)=x^n\\).
(Suggerimento: una semplice definizione ricorsiva segue dal fatto che \\(x^n = x * x^{n-1}\\), per \\(n > 0\\) )

**Esercizio 3.3**

Scrivere un codice ricorsivo per la funzione che restituisce la somma dei primi \\(n\\) integeri.
(Suggerimento: si osservi che la somma dei primi \\(n\\) numeri è pari alla somma fra \\(n\\) 
e la somma dei primi \\(n-1\\) numeri)

**Esercizio 3.4**

Scrivere un codice ricorsivo per la funzione ``find_index()``, che restituisce l'indice di un numero nalla sequenza di
Fibonacci, se il numero è nella sequenza, e restituisce -1, altrimenti.

**Esercizio 3.5**

Descrivere un algoritmo ricorsivo per trovare il massimo e il minimo in una sequenza di \\(n\\) integeri,
senza usare cicli. Quali sono il tempo e lo spazio di esecuzione?
(Suggerimento: considerare di restituire una tupla che contenga sia il massimo che io minimo)

**Esercizio 3.6**

Scrivere una funzione ricorsiva per rovesciare una lista.

**Esercizio 3.7**

Scrivere una funzione ricorsiva per il problema dell'unicità dell'elemento che sia eseguita in tempo \\(O(n^2)\\), 
senza usare l'ordinamento.
(Suggerimento: dire se un elemento in una sequenza è unico significa dire che gli ultimi \\(n−1\\) elementi
sono unici e sono diversi dal primo elemento)

**Esercizio 3.8**

Scrivere una funzione ricorsiva che calcoli il prodotto di due interi, \\(m\\) e \\(n\\), usando solo l'addizione.
(Suggerimento: il prodotto di \\(m\\) e \\(n\\) è pari alla somma di \\(m\\), \\(n\\) volte)

**Esercizio 3.9**

Scrivere un algoritmo ricorsivo per calcolare l'esponente di due interi positivi \\(m^n \\), usando solo il prodotto.
(Suggerimento: intuire come l'esponente è definito in termini del prodotto)

**Esercizio 3.10**

Scrivere una funzione ricorsiva che, data una stringa di caratteri *s*, restituisca la stringa rovesciata.
Ad esempio, *pots* diventa *stop*.
(Suggerimento: stampare un carattere alla volta, senza spazi extra)

**Exercise 3.11**

Scrivere una funzione ricorsiva che verifichi che una stringa *s* sia palindroma, cioè che sia uguale
alla stringa rovesciata.
(Suggerimento: verificare l'uguaglianza del primo eultimo carattare e applicare la ricorsione)

**Exercise 3.12**.

Usare la ricorsione per scrivere una funzione che determini che una stringa *s* ha più vocali che consonanti.