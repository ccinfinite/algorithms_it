Capitolo Tre: Sequenze basate su array
**************************************


Sezione 1 Introduzione: Tipi sequenza in Python
-----------------------------------------------

Le classi che Python fornisce per l'implementazione delle sequenze sono *list*, *tuple*, e *str*.
Ciascuna di esse supporta l'indicizzazione per accedere a un elemento \\(k\\),
utilizzando una sintassi come ``seq[k]``; condividono anche il concetto di *array* di basso livello
per essere rappresentate. Si differenziano per come le istanze di queste classi sono rappresentate
internamente in Python, come vedremo nel resto del capitolo.

Per scrivere un codice corretto c'è unicamente bisogno di comprendere la sintassi e la semantica
dell'interfaccia pubblica delle classi, ma appare anche importante avere una visione più approfondita
dell'implementazione delle stesse, per raggiungere un buon livello di efficienza dei programmi.
L'uso di base di liste, stringhe e tuple è sostanzialmente simile, ma numerosi dettagli
possono influenzare il comportamento e l'efficienza dei programmi, se sottovalutati.

Sezione 2 Array di basso livello
--------------------------------

La memoria di un computer è sostanzialmente formata da *bit* di informazione,
raggruppati in unità più grandi in modi che dipendono dall'architettura del sistema.
Una tipicaunità è il *byte*, che equivale a 8 bit.
Per tenere traccia dell'informazione immagazzinata nei bytes, viene usato un 
*indirizzo* di memoria: a ogni byte è associato un numero binario unico che serve come indirizzo.
Gli indirizzi sono coordinati con il layout fisico della memoria, e quindi sono definiti in modo sequenziale.
Attraverso l'indirizzo è possibile accedere direttamente e efficientemente a ogni byte;
in linea di principio, il contenuto di ogni byte può essere immagazzinato e recuperato in tempo \\(O(1)\\).
La memoria principale di un computer agisce quindi come una random access memory (RAM).

Un compito molto comune della programmazione consiste nel memorizzare una sequenza di oggetti
in qualche modo relazionati, in tal caso sarebbe sufficiente usare un certo numero di variabili, ma
spesso si preferisce poter utilizzare un singolo nome per la sequenza, e un indice per
riferirsi agli elementi della sequenza stessa. 
Questo è ottenuto memorizzando le variabili una dopo l'altra in una porzione contigua 
della memoria, e denotando questa rappresentazione con il nome di *array*.
Ad esempio, la stringa di testo "STRING" è immagazzinata come una sequenza ordinata di
sei caratteri; ogni carattere è rappresentato con il set di caratteri Unicode, e 
ogni carattere Unicode è rappresentato con 16 bit.
Quindi la stringa sarà memorizzata in 12 bytes di memoria consecutivi.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura31.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Ci riferiamo a questa sequenza come a un array di sei caratteri; 
ogni locazione in un array è chiamate *cella*, e usiamo un indice intero per descrivere le locazioni
dell'array, partendo da 0 (ad esempio, la cella del precedente array con indice 2 contiene il carattere R).

Ogni cella di un array deve usare lo stesso numero di bytes.
Ciò consente di accedere a qualsiasi cella di un array in tenpo costante.
Se è noto l'indirizzo di memoria a cui l'array comincia, il numero di byte per elemento, e
un indice nell'array, l'indirizzo dell'elemento è 
*start + cellsize x index*, e questo calcolo può essere fatto a un basso
livello di astrazione, restando completamente invisibile al programmatore.


Sezione 2.1 Array di riferimenti
================================

Si consideri una struttura basata su array che, per qualche motivo, contenga i nomi di alcuni colori.
In Python si può utilizzare una lista di nomi, come ``[ White , Blue , Yellow , Red , Black ]``.
Gli elementi della lista sono stringhe di lunghezza differente: questo contrasta con la richiesta 
che ogni cella di un array, in Python, debba usare lo stesso numero di byte.
una prima soluzione sarebbe quella di riservare sufficiente spazio per ogni cella per contenere
la stringa con la massima lunghezza; naturalmente è una soluzione insufficiente (molta memoria 
è sprecata) e non generale (non sappiamo se ci sarà un altro nome più lungo di quelli già noti).

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura32.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Una soluzione più efficiente consiste nel rappresentare la istanza della lista attraverso un array 
di riferimenti a oggetti: è immagazzinata una sequenza consecutiva di indirizzi di memoria, ciascuno dei quali
si riferisce a un elemento della sequenza di nomi. Si noti che anche se la dimensione di ogni elemento può
variare, il numero di bit usati per immagazzinare l'indirizzo di memoria di ogni elemento è fisso;
ciò consente un accesso in tempo costante a una lista di elementi, basato sugli indici.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura33.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

In Python le liste sono strutture di riferimenti, e quindi una istanza di una lista può contenere
riferimenti multipli allo stesso oggetto, o un solo oggetto può essere elemento di due o più liste.
Ad esempio, la lista ``primes=[2,3,5,7,11,13,17]`` è una lista di riferimenti a numeri, non una
lista di numeri. Il comando ``temp = primes[3:6]`` restituisce una nuova istanza di lista 
``[7,11,13]``, è quest'ultima contiene riferimenti agli stessi elementi che sono nella lista originaria.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura34.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Se gli elementi della lista sono oggetti di tipo immutabile, non è possibile provocare una modifica degli oggetti
condivisi; ad esempio, il comando ``temp[2] = 15`` non cambia il già esistente oggetto intero 13; 
invece, esso modifica il riferimento nella cella 2 della lista ``temp`` in un nuovo
riferimento a un oggetto diverso. 

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura35.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Lo stesso accade quando si crea una nuova lista come copia di una già esistente, con il comando
``backup = list(primes)``. Viene prodotta una nuova lista che è una *shallow copy*, 
nel senso che essa fa riferimento agli stessi elementi della prima lista.
Se gli elementi dellalista sono oggetti di tipo mutabile, è possibile produrre una  *deep copy* 
(una nuova lista con nuovi elementi), utilizzando la funzione ``deepcopy`` del modulo ``copy``.


Sezione 2.2 Array compatti
==========================

In contrasto con gli array di riferimenti, gli *array compatti* immagazzinano i bit che rappresentano i dati 
direttamente nella struttura. Ad esempio, le stringhe sono rappresentate in Python come array di caratteri,
non come array di riferimenti a caratteri. 
Questa soluzione ha alcuni vantaggi computazionali rispetto agli array di riferimenti;
una struttura di riferimenti usa 64 bit per ogni indirizzo immagazzinato nell'array,
più il numero di bit usato per rappresentare gli elementi. 
Inoltre, in un array compatto i dati sono immagazzinati consecutivamente in memoria, a differenza
delle strutture di riferimenti, i cui elementi risiedono in parti della memoria non consecutive;
è vantaggioso, per ovvi motivi di efficienza, avere vicini in memoria i dati che potrebbero essere utilizzati
nella stessa computazione.

Il supporto principale per creare array compatti si trova nel modulo ``array``.
Esso definisce una classe ``array`` e fornisce una rappresentazione compatta per array di tipi di dati primitivi.
Un array compatto di numeri primi è dichiarato come ``primes = array( i , [2, 3, 5, 7, 11, 13, 17, 19])``,
dove ``i`` rappresenta il tipo dei dati dell'array (interi in queso esempio).
Ciò consente all'interprete di conoscere a priori la quantità di bit necessari per immagazzinare l'array.


Sezione 3 Array dinamici
------------------------

Nella sezione precedente abbiamo osservato che per creare un array compatto deve essere sostanzialmente
dichiarata la dimensione dell'array. Questa informazione serve per allocare la giusta quantità di memoria
che ospiterà l'array. 
Se si desidera estendere le capacità dell'array in modo *dinamico*, una prima soluzione consiste nell'aggiungere
celle all'array precedentemente definito; questo non può essere realizzato facilmente, per il motivo che
le locazioni di memoria vicine potrebbero essere già occupate con altri dati.

Si consideri, ad esempio, la classe ``list`` di Python.
Una lista ha una lunghezza fissata quando è definita, ma è possibile comunque aggiungere elementi alla lista.
Questa possibilità è ottenuta implementando la lista come un *array dinamico*; l'istanza della lista conserva
un array sottostante che ha una capacità maggiore della effettiva lunghezza della lista.
Ad esempio, mentre la lista contiene sei elementi, il sistema costruisce un array sottostante che può
immagazzinare otto o più riferimenti a oggetti. Un nuovo elemento può essere aggiunto allalista usando
la prima cella disponibile dell'array.
Tuttavia, questa soluzione non è sufficiente se l'utente continua ad aggiungere elementi alla lista.
In questo caso, il sistema deve fornire un nuov array più grande del precedente, inizializzandolo
in modo tale che la sua parte iniziale contenga il precedente array.

Di seguito è mostrato che la classe ``list`` di Python è basata su questa strategia::

 1  import sys           # includes the function getsizeof
 2  data = [ ] 			 # empty list
 3  for k in range(n):
 4    a = len(data) 			# a is the number of elements
 5    b = sys.getsizeof(data) 	# size of data in bytes
 6    print( Length: {0:3d}; Size in bytes: {1:4d} .format(a, b))
 7    data.append(None) 		# increase length by one

 Length: 0; Size in bytes : 72
 Length: 1; Size in bytes : 104
 Length: 2; Size in bytes : 104
 Length: 3; Size in bytes : 104
 Length: 4; Size in bytes : 104
 Length: 5; Size in bytes : 136
 Length: 6; Size in bytes : 136
 Length: 7; Size in bytes : 136
 Length: 8; Size in bytes : 136
 Length: 9; Size in bytes : 200
 Length: 10; Size in bytes : 200
 Length: 11; Size in bytes : 200
 Length: 12; Size in bytes : 200
 Length: 13; Size in bytes : 200
 Length: 14; Size in bytes : 200
 Length: 15; Size in bytes : 200
 Length: 16; Size in bytes : 200
 Length: 17; Size in bytes : 272
 Length: 18; Size in bytes : 272
 Length: 19; Size in bytes : 272
 Length: 20; Size in bytes : 272
 Length: 21; Size in bytes : 272
 Length: 22; Size in bytes : 272
 Length: 23; Size in bytes : 272
 Length: 24; Size in bytes : 272
 Length: 25; Size in bytes : 272
 Length: 26; Size in bytes : 352

Si nota che una lista vuota richiede già un certo numero di byte in memoria (72 in questo caso).
Ciò accade perchè ogni oggetto in Pyhton mantiene alcune variabili di istanza private,
come il numero di elementi effettivamente presenti nella lista, 
il numero massimo di elementi immagazzinabili nell'array sottostante, 
e il riferimento all'array allocato. 

Non appena il primo elemento è inserito nella lista, il numero di byte passa da 72 a 104,
per allocare un array in grado di immagazzinare quattro riferimenti a oggetti.
Dopo che il quinto elemento è stato aggiunto alla lista, la memoria usata passa da 104 a 136 byte,
e la lista può ospitare fino a otto riferimenti. Dopo il nono e il diciassettesimo inserimento
la dimensione è ancora incrementata.

Poiché una lista è una struttura di riferimenti, il risultato di ``getsizeof`` per una istanza di lista
include solo la dimensione necessaria per rappresentare la struttura primaria; non prende in considerazione
la memoria usata dagli oggetti che sono gli elementi della lista.
Nel nostro esperimento, None è aggiunto ripetutamente alla lista, ma potremmo aggiungere qualsiasi
tipo di oggetto senza influenzare il numero di bytes riportato da ``getsizeof(data)``.


Sezione 3.1 Implementazione
===========================

In questa sezione forniremo una implementazione di una classe ``DynamicArray``;
dato un array *A* che immagazzina gli elementi di una lista, mostreremo come aggiungere un elemento
quando è stata raggiunta la dimensione massima dell'array, utilizzando i quattro successivi passi::

 1. definire un nuovo array B, con capacità doppia;
 2. B[i] = A[i] (per i = 0,... n−1), con n il numero degli oggetti correnti;
 3. A = B, , cioé si usa B come array che supporta la lista;
 4. aggiungere il nuovo elemento al nuovo array.

Python supporta la creazione di una lista con la classe ``list``;
noi forniamo una implementazione alternativa con il codice seguente::

 1  import ctypes					# provides low-level arrays
 2
 3  class DynamicArray:
 4
 5    def  __init__ (self):
 6      self._n = 0 				# number of elements
 7      self._capacity = 1 			# initial capacity
 8      self._A = self._make_array(self._capacity) 			# define low-level array
 9
 10   def __len__ (self):
 11     return self._n
 12
 13   def __getitem__ (self, k):
 14     if not 0 <= k < self._n:
 15       raise IndexError('invalid index')
 16     return self._A[k]
 17
 18   def append(self, obj):
 19     if self._n == self._capacity:			# full array
 20       self._resize(2*self._capacity) 		# double capacity
 21     self._A[self._n] = obj
 22     self._n += 1
 23
 24   def _resize(self, c):
 25     B = self._make_array(c) 			# new array
 26     for k in range(self._n):
 27       B[k] = self._A[k]
 28     self._A = B 						# A is the new array
 29     self._capacity = c
 30
 31   def _make_array(self, c):
 32     return (c*ctypes.py_object)( ) # see ctypes documentation

In questo frammento di codice abbiamo fornito le funzionalità ``append``, ``__len__``, e ``__getitem__``.
Un nuovo array di basso livello è creato usando il modulo ``ctypes``.


Sezione 3.2 Analisi di array dinamici
=====================================

In questa sezione analizziamo il tempo di esecuzione delle operazioni su array dinamici, usando una tecnica
chiamata *analisi ammortizzata* (*algorithmic amortization*) per mostrare che la strategia prima introdotta
è sufficientemente efficiente.

Rimpiazzare un array pieno con un array nuovo e più grande richiede tempo \\(\\Omega(n)\\),
con \\(n\\) il numero di elementi nell'array.
Questo accade quando cerchiamo di aggiungere un nuovo elemento a un array che è già pieno.
Una volta raddoppiata la capacità dell'array, possiamo aggiungere \\(n\\) nuovi elementi prima che
esso necessiti di essere nuovamente ampliato. Quindi ci sono molte semplici operazioni di aggiunta
a fronte di una più dispendiosa. 

Per mostrare l'efficienza della strategia proposta, immaginiamo che debba essere pagata una moneta
per un ammontare costante di tempo di computazione. Prima di eseguire una operazione dobbiamo avere
sufficienti monete per pagare il tempo di esecuzione dell'operazione stessa. L'ammontare totale
di monete spese per ogni computazione è proporzionale al tempo totale speso dalla computazione.
Dimostriamo quanto segue

**Proposizione:** Sia \\(S\\) una sequenza implementata tramite un array dinamico con capacità iniziale 
pari a 1, e raddoppiando la dimensione dell'array quando esso è pieno. Il tempo totale richiesto per
effettuare \\(n\\) operazioni di aggiunta a \\(S\\) è \\(O(n)\\).

**Dimostrazione** Si assuma che una moneta sia il costo di ogni operazione di aggiunta, e che 
raddoppiare un array di dimensione \\(k\\) (quando serve) costi \\(k\\) monete.
La tecnica di analisi ammortizzata ci consente di far pagare di più alcune operazioni semplici allo scopo
di pagare per altre più dispendiose. 
Ogni operazione di aggiunta costerà quindi tre monete (due in più del costo reale dell'operazione)
quando non causa overflow dell'array.
Useremo le monete extra per pagare il costo dell'operazione di raddoppio. 
Osserviamo che l'operazione di raddoppio di \\(S\\) si verifica quando l'array ha \\(2^i\\) elementi, 
per \\(i \\geq 0\\); raddoppiare la dimensione costa \\(2^i\\) monete.
Queste monete sono state accantonate durante le precedenti operazioni di aggiunta, dalla cella
\\(2^{i−1}\\) alla cella \\(2^i −1\\), e possono essere usate per pagare l'estensione
Il nostro schema di ammortamento, tre monete per ogni operazione, ci consente di pagare 
l'esecuzione di \\(n\\) operazioni di aggiunta usando \\(3n\\) monete;
il tempo di esecuzione ammortizzato di ogni operazione di aggiunta è \\(O(1)\\), 
e il tempo totale di esecuzione di \\(n\\) aggiunte è \\(O(n)\\).

Si osservi che il fondamento della precedente performance lineare  sta nel fatto che lo spazio aggiunto
quando l'array è espanso è sempre proporzionale alla dimensione corrente dell'array.
Potremmo scegliere di aumentare la dimensione dell'array del 30 per cento, ad esempio, 
invece di raddoppiarlo; se l'estensione dell'array segue una crescita geometrica si dimostra che
l'analisi ammortizzata precedente è ancora valida, usando sempre un numero costante di monete
per ogni operazione. 

Nel caso in cui la progressione sia aritmetica invece che geometrica, il costo totale diventa quadratico.
Ad esempio, si supponga di aggiungere una sola cella ogni volta che l'array deve essere ridimensionato;
ciò comporta il ridimensionamento dell'array per ogni operazione di aggiunta, e ciò richiede 
\\(1+2+...+n\\) operazioni, cioè \\(\\Omega(n^2)\\).
In generale, si dimostra

**Proposizione** sia \\(S\\) una sequenza implementata tramite un array dinamico
con capacità iniziale pari a 1, e usando un incremento fisso per ogni ridimensionamento dell'array.
Il tempo totale richiesto per effettuare \\(n\\) operationi di aggiunta a \\(S\\) è \\( O(n^2) \\).

**Dimostrazione** Sia \\(c > 0\\) in numero fisso di celle aggiunte all'array per ogni ridimensionamento.
Durante le \\(n\\) operazioni di aggiunta è richiesto tempo per inizializzare gli array di dimensione 
\\(c\\), \\(2c\\), \\(3c\\), ..., \\(mc\\),  con \\(m = n/c\\);
il tempo totale è proporzionale a \\(c+2c+3c+ \\ldots +mc\\), cioè a \\( \\Omega(n^2) \\).

Per concludere, l'incremento geometrico della capacità quando si ridimensiona l'array mostra una
interessante proprietà per una struttura di dati: la dimensione finale dell'array, alla fine di 
\\(n\\) operazioni, è proporzionale a \\(O(n)\\). 
In generale, ogni contenitore che fornisce operazioni per aggiungere o eliminare uno o più elementi
richiede un uso della memoria pari a \\(O(n)\\).
Ripetuti inserimenti ed eliminazioni possono causare una crescita arbitraria dell'array, senza una relazione 
proporzionale fra il numero di elementi reale e la capacità dell'array. 
Una implementazione robusta di una struttura dati deve provvedere a ridurre la dimensione dell'array,
se possibile, conservando un buon costo ammortizzato per ogni operazione individuale.


Sezione 4 Performance dei tipi sequenza di Python: liste, tuple, e stringhe
---------------------------------------------------------------------------

In questa sezione consideriamo inizialmente il comportamento delle tuple e delle liste 
*immutabili* di Python; successivamente analizzeremo cosa accade con liste *mutabili*. 


Sezione 4.1  Liste immutabili e Tuple
=====================================

I metodi immutabili della classe ``list`` e qelli della classe ``tuple`` sono gli stessi.
Le tuple e le liste immutabili hanno la stessa efficienza, poiché non necessitano di array
dinamici sottostanti. Mostriamo un sommario del costo di alcune operazioni su istanze di queste classi.

**Operazioni in tempo costante** La lunghezza di una lista o di una tupla ``len(data)`` è restituita in tempo
costante, dato che ogni istanza conserva esplicitamente questo valore.
L'accesso diretto a un elemento ``data[j]`` richiede tempo costante, lo stesso richiesto dall'accesso all'array
sottosante.

**Ricerca di un valore**
``data.count(value)``, ``data.index(value)``, e ``value in data`` richiedono iterazioni sulla sequenza,
da sinistra a destra. 
Si noti che l'iterazione per ``count`` deve procedere lungo tutta la sequenza di lunghezza \\(n\\),
e quindi la sua complessità temporale è \\(O(n)\\).
Gli altri due metodi possono interrompere l'iterazione non appena l'indice è stato trovato, o
l'appartenenza verificata, rispettivamente; quindi la loro complessità è \\(O(k)\\), 
con \\(k\\) l'indice della occorrenza più a sinistra di ``value``.

**Confronto di sequenze**
Il confronto fra due sequenze è definito lessicograficamente.
Richiede, nel caso peggiore, una iterazione che usa tempo proporzionale alla lunghezza della sequenza più
corta, quindi \\(O(n)\\).

**Creazione di nuove istanze**
Il tempo di esecuzione di operazioni come lo slicing di una sequenza ``data[a:b]`` 
o la concatenazione di due sequenze ``data1+data2`` è proporzionale alla lunghezza 
del risultato.

Sezione 4.2 Liste mutabili
==========================

La più semplice operazione che cambia una lista è ``data[j] = val``, con la ovvia semantica.
Essa è supportata dal metodo ``__setitem__``, con una complessità nel caso peggiore di \\(O(1)\\),
poichè un elemento della lista è semplicemente rimpiazzato con un altro.
Mostriamo di seguito la complessità dei metodi che aggiungono o rimuovono elementi da una lista.

**Aggiungere elementi a una lista**
Il metodo ``append`` richiede \\(\\Omega(n)\\), nel caso peggiore, quando è necessario un ridimensionamento
dell'array. Seguendo lo schema di analisi ammortizzata, esso richiede tempo \\(O(1)\\).
un altro metodo supportato dalla classe ``list`` è ``insert(k, value)``, che inserisce un dato valore 
``value`` nella lista, all'indice ``k``, mentre tutti gli elementi successivi sono
spostati a destra. Una implementazione di questo metodo è::

 1  def insert(self, k, value):
 2
 3    if self._n == self._capacity: 		# not enough room, resize the array
 4      self._resize(2*self._capacity)
 5    for j in range(self._n, k, −1): 		# shift to right, rightmost first
 6      self._A[j] = self._A[j−1]
 7    self._A[k] = value
 8    self._n += 1

Si osservi che aggingere un elemento potrebbe richiedere il ridimensionamento dell'array.
Ciò costa tempo lineare nel caso peggiore, ma costa tempo ammortizzato \\(O(1)\\).
Spostare \\(k\\) elementi per far spazio al nuovo costa tempo ammortizzato \\(O(n−k+1)\\).
Ne lvalutare ail tempo medio per operazione, osserviamo che inserire all'inizio della lista è
dispendioso, poiché è richiesto tempo lineare per ogni inserimento; 
inserire al centro richiede circa metà del tempo richiesto per inserire all'inizio, ma
ancora tempo lineare; inserire alla fine richiede tempo costante.

**Rimuovere un elemento da una lista**
Il modo più semplice per rimuovere un elemento da una istanza di una classe ``list`` 
è utilizzare il metodo ``pop()``, che rimuove l'ultimo elemento della lista;
la sua esecuzione richiede tempo ammortizzato \\(O(1)\\), dato che tutti gli elementi rimangono
nelle celle originarie dell'array dinamico sottostante, con l'eccezione dell'occasionale
ridimensionamento dell'array. 
Il metodo ``pop(k)`` rimuove il \\(k\\)-mo elemento della lista, e sposta tutti i successivi a sinistra;
richiede complessità \\(O(n−k)\\).

Il metodo ``remove(value)`` rimuove la prima occorrenza di ``value`` dalla lista;
richiede una scasione completa della lista, prima per cercare il valore, e poi per spostare tutti gli altri
elementi a sinistra::

 1  def remove(self, value):
 2    for k in range(self._n):
 3      if self._A[k] == value:
 4        for j in range(k, self._n−1):
 5          self._A[j] = self._A[j+1]
 6        self._A[self._n −1] = None
 7        self._n −= 1
 8        return 					# exit immediately
 9    raise ValueError('value not found')

**Estendere una lista**
Il metodo ``extend`` è usato per aggiungere tutti gli elementi di una lista alla fine della seconda,
con la chiamata ``first.extend(second)``. Il tempo di esecuzione è proporzionale alla lunghezza della lista 
``second``, ed è ammortizzato perché l'array che rappresenta ``first`` può necessitare di ridimensionamento.

**Costruire nuove liste**
Python offre numerosi modi per costruire nuove liste. 
In quasi tutti i casi la richiesta temporale è lineare nella lunghezza della lista.
Ad esempio, è comune creare una lista usando l'operatore di moltiplicazione, come per ``[0]*n``, 
che crea una lista lunga ``n`` con valori tutti pari a 0.


Sezione 5 Alcuni algoritmi sulle stringhe in Python
---------------------------------------------------

In questa sezine analizzeremo il comportamento di alcuni metodi e di alcuni noti algoritmi sulle stringhe.
Si denoti con \\(n\\) e \\(m\\) la lunghezza delle stringhe. Appare naturale valutare come almeno lineare la
complessità dei metodi che producono una nuova stringa a partire da una in input. 
Anche i metodi che verificano condizioni booleane su una stringa, o gli operatori di confronto,
richiedono tempo \\(O(n)\\), in quanto devono verificare, nel caso peggiore, tutti gli \\(n\\) 
caratteri, e applicare una tecnica di corto circuito non appena la risposta è trovata
(ad esempio, ``islower`` restituisce False se il primo carattere della stringa è maiuscolo); 
lo stesso accade per gli operatori di confronto, come ``==`` o ``<=``.

Sezione 5.1 Pattern Matching
============================

Nel classico problema del pattern-matching abbiamo una stringa \\(T\\) di lunghezza \\(n\\)
e una stringa di pattern \\(P\\) di lunghezza \\(m\\), e desideriamo verificare se \\(P\\) 
è una sottostringa di \\(T\\).
La soluzione a questo problema mostra alcuni comportamenti interessanti, dal punto di vista algoritmico.
Una implementazione di forza bruta è eseguita in tempo \\(O(mn)\\), poiché essa considera tutti gli
\\(n−m+1\\) possibili indici da cui il pattern può iniziare, e spende tempo \\(O(m)\\) 
per ogni possibile punto di partenza, verificando se c'è un match.
Soluzioni più raffinate sono eseguite in tempo \\(O(n)\\).

La soluzione formale del problema consiste nel trovare il minimo indice  \\(j\\) in \\(T\\)
in cui cominci \\(P\\), tale che \\(T[j:j+m]\\) sia uguale a \\(P\\).
Il problema del pattern-matching è in relazione con molti metodi della classe ``str`` di Python,
come ad esempio ``in``, ``find``, ``index``, ``count``,
ed è un subtask di altri metodi, come ``partition``, ``split``, e ``replace``.

**Prima soluzione: forza bruta**

In generale, il metodo della forza bruta consiste nell'enumerare tutte le possibili configurazioni 
dell'input, e selezionare la migliore fra esse, in base a qualche tipo di misura.
Si immagina facilmente che l'enumerazione e la ricerca implicano un consumo di tempo non ottimale.
L'algoritmo ricerca tutti i possibili piazzamenti di \\(P\\) in \\(T\\),
restituendo il minimo indice di \\(T\\) in cui comincia la sottotringa \\(P\\) (-1, altrimenti), come segue::

 1  def find brute(T, P):
 2
 3    n, m = len(T), len(P)
 4    for i in range(n−m+1):
 5      k = 0
 6      while k < m and T[i + k] == P[k]:
 7        k += 1
 8      if k == m:			# we reached the end of P,
 9        return i 			# substring T[i:i+m] matches P
 10     return −1 			# no match

Il precedente algoritmo consiste di due cicli annidati: quello esterno su tutti i possibili 
indici di partenza del pattern nel testo \\(T\\); quello interno su tutti i caratteri del pattern
\\(P\\), confrontandoli con il corrispondente carattere nel testo. 
ciò implica che per ogni indice candidato in \\(T\\), si effettuano fino a \\(m\\) confronti
fra caratteri; quindi, nel caso peggiore, si effettuano \\(O(mn)\\) operazioni.

**Seconda soluzione: l'algoritmo di Boyer-Moore**

L'algoritmo di pattern- matching di Boyer-Moore evita molti confronti fra \\(P\\) 
e una parte dei caratteri in \\(T\\). Questo significa che non c'è bisogno, come per l'approccio
di forza bruta, di conrontare tutti i caratteri di \\(T\\) per trovare il match.

Due considerazione ci consentono di scriver questo algoritmo: l'euristica *looking-glass* e la 
euristica *character-jump*. Con la prima si comincia il confronto dalla fine di \\(P\\), 
spostandosi indietro fino all'inizio; se incontriamo un mismatch in una certa locazione di \\(T\\),
possiamo evitare tutti i confronti rimanenti, shiftando \\(P\\) rispetto a \\(T\\).
usando la euristica character-jump.
Infatti, se c'è mismatch fra il carattere del testo \\(T[i]=c\\) e il carattere del pattern \\(P[k]\\), 
si verificano due casi: se \\(c\\) non appartiene a \\(P\\) allora shiftiamo \\(P\\) completamente oltre \\(T[i]\\); 
altrimenti, shiftiamo\\(P\\) finché l'occorrenza del carattere \\(c\\) in P si allinea con \\(T[i]\\).
in entrambi i casi un certo numero di confronti non è eseguito.

Più precisamente, si supponga che un match è trovato per l'ultimo carattere di \\(P\\):
l'algoritmo cerca di trovare u nmatch per il penultimo carattere del pattern, e così via,
finché è trovato un match completo, o si verifica un mismatch in qualche posizione del pattern.
Se questo accade, e se il carattere del testo che ha provocato il mismatch non è presente nel pattern,
si shifta l'intero pattern oltre quella posizione.
Se il carattere che ha provocato il mismatch è presente altrove nel pattern, occorre considerare due
possibili sottocasi, a seconda se la sua ultima occorenza sia (1) prima o (2) dopo il carattere del pattern
che era allineato con il carattere che ha causato il mismatch.

Sia \\(i\\) l'indice del carattere del testo che ha causato il mismatch,
\\(k\\) il corrispondente indice nel pattern,
e \\(j\\) l'indice dell'ultima occorrenza di \\(T[i]\\) nel pattern.
Se (1) \\(j < k\\), shiftiamo il pattern di \\( k− j\\) unità, e l'indice \\(i\\) 
è incrementato di \\(m−(j +1)\\);
se (2) \\(j > k\\), shiftiamo il pattern di una unità, e l'indice \\(i\\) 
è incrementato di \\(m−k\\).
Di seguito introduciamo l'implementazione Python dell'algoritmo::

 1  def boyer_moore(T, P):
 2	  # returns the index of the character in T where P begins, if it exists
 3    n, m = len(T), len(P)
 4    if m == 0: return 0
 5    last = { } 						# build ’last’ dictionary
 6    for k in range(m):
 7      last[ P[k] ] = k 				# later occurrence overwrites
 8
 9    i = m−1 			# index of T
 10   k = m−1 			# index of P
 11   while i < n:
 12     if T[i] == P[k]: 	# match
 13       if k == 0:
 14         return i 		# the pattern begins at index i of text
 15       else:
 16         i −= 1
 17         k −= 1
 18     else:				#no match
 19       j = last.get(T[i], −1) 		# last(T[i]) is -1 if not found
 20       i += m − min(k, j + 1) 		# case analysis for jump step
 21       k = m − 1 					# restart at the end of pattern
 22   return −1

L'efficienza dell'algoritmo si fonda sulla possibilità di creare una funzione ``last(c)``
che restituisce l'indice della occorrenza più a destra di \\(c\\) in \\(P\\) (-1, se \\(c\\) non è in \\(P\\)).
Sel'alfabeto è finito, e se i caratteri possono essere usati come indici di un array, 
``last(c)`` può essere implementata come una look-up table con costo temporale costante, 
contenendo tutti i caratteri del pattern.
Il caso peggiore dell'algoritmo di Boyer-Moore è \\(O(nm+|\\Sigma|)\\).
la funzione ``last`` usa tempo \\(O(m+|\\Sigma|)\\),
e la ricerca del pattern usa tempo \\(O(nm)\\) nel caso peggiore, lo stesso dell'algoritmo di forza bruta. 
Boyer-Moore è capace di evitare di lavorare su grandi porzioni del testo, e evidenze sperimentali
mostrano che il numero medio di confronti per carattere è un quarto rispetto a quelli dell'algoritmo
di forza bruta. 

Euristiche più raffinate consentono di ottenere tempi di esecuzione in \\(O(n+m+|Σ|)\\),
come ad esempio l'algoritmo di Knuth-Morris-Pratt.


Sezione 5.2 Comporre le stringhe
================================

Si suppona di avere una stringa di nome ``document``, e di voler creare una stringa ``letters`` 
che contenga i caratteri alfabetici della string di partenza. 
Una prima soluzione è ::

 letters = ''
 for c in document:
   if c.isalpha( ):
     letters += c

Aggiungiamo ogni carattere del documento a ``letters`` solo se è alfabetico.
Anche se la soluzione appare naturale, essa è estremamente inefficiente. 
Una stringa ha tipo immutabile, e quindi ogni volta che una lettera è aggiunta 
dobbiamo estendere la lista; abbiamo già visto che questa operazione richiede tempo 
proporzionale alla lunghezza della lista stessa. Per un risultato finale di \\(n\\) 
caratteri la serie di concatenazioni usa tempo proporzionale a \\(1+2+3+ \\ldots +n\\),
cioè a \\(O(n^2)\\).
La ragione per cui il comando ``letters += c`` è inefficiente è che deve essere sempre creata una nuova stringa,
mentre quella originale non deve essere cambiata se esistono altre variabili che fanno riferimento ad essa.
Ma se non esistono tali riferimenti allora il comando può essere implementato più efficientemente,
usando un array dinamico, ad esempio. Possiamo verificare se esistono altri riferimenti alla stringa
testando il *contatore dei riferimenti*, un numero che esiste per ogni oggetto.

Un'altra soluzione consiste nel costruire una lista temporanea per immagazzinare 
singoli caratteri, e poi usare il metodo ``join`` della classe ``str`` per comporre il 
risultato finale::

 temp = [ ]
 for c in document:
   if c.isalpha( ):
     temp.append(c)
 letters = ''.join(temp)

La chiamata a  ``append`` richiede tempo \\(O(n)\\), poichè ci sono al più \\(n\\) chiamate,
e ognuna di esse costa un tempo ammortizzato \\(O(1)\\).
La chiamata a ``join`` costa tempo lineare nella lunghezza di ``temp``.


Sezione 5.3 Selection Sort
==========================

Per una sequenza basata su array, 'algoritmo di *selection sort* confronta il primo e il secondo elemento.
Se il secondo è minore del primo, li inverte. Poi considera il terzo elemento dell'array,
invertendolo verso sinistra finché non è nella posizione giusta rispetto ai primi due elementi.
Il processo continua finché l'array non è ordinato::

 Algorithm InsertionSort(A):
   Input: an array A of n elements
   Output: the array A with elements rearranged in nondecreasing order
     for k from 1 to n − 1 do
       Insert A[k] at proper location within A[0], A[1], . . ., A[k].

L'implementazione in Python dell'insertion-sort usa un ciclo esterno per ogni elemento, 
e un ciclo interno che colloca un elemento nella sua giusta posizione all'interno dell'array
formato dagli elementi alla sua sinistra::

 1  def insertion sort(A):
 3    for k in range(1, len(A)):
 4      cur = A[k]
 5      j = k
 6      while j > 0 and A[j−1] > cur:
 7        A[j] = A[j−1]
 8        j −= 1
 9      A[j] = cur

Il caso peggiore è \\(O(n^2)\\), ma se l'array è quasi ordinato l'algoritmo ha tempo di esecuzione \\(O(n)\\).
