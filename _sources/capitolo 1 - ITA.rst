
Capitolo Uno: Analisi degli algoritmi
*************************************

Sezione 1. Introduzione
-----------------------

Una *struttura dati* consiste in una definizione formale di come un insieme di dati è organizzato e di come si
realizza l'accesso ad esso; un *algoritmo* è una procedura definita e usata allo scopo di effettuare 
operazioni sulla struttura dati, entro una quantità di tempo e/o spazio finita e calcolabile.

Il tempo e lo spazio sono le risorse usate da ogni modello di calcolo.
Il nostro scopo è quello di classificare le strutture dati e gli algoritmi come buoni o cattivi in base 
alla quantità di risorse usate durante il calcolo; quindi siamo interessati a sviluppare strumenti di analisi
che descrivano precisamente il tempo e lo spazio utilizzati da ogni algoritmo. 

Per motivi teoretici il tempo e lo spazio sono collegati, e il tempo è considerato come la risorsa più
interessante in informatica. La complessità di un algoritmo può essere influenzata da molti fattori 
(l'ambiente hardware, il sistema operativo, il linguggio di programmazione usato, ecc.), ma è importante 
caratterizzare il tempo di esecuzione di un algoritmo come funzione della dimensione dell'input; 
fatti pari gli altri fattori, la complessità è definita come una relazione fra la dimensione dell'input e il
tempo di esecuzione dell'algoritmo stesso. 

In seguito introdurremo alcuni strumenti che ci consentiranno di realizzare studi sperimentali sulla 
complessità di un algoritmo, e mostreremo che usare questo tipo di esperimenti per valutarne l'efficienza ha
numerose limitazioni. Quindi focalizzeremo l'attenzione sullo sviluppo di alcuni strumenti matematici
da utilizzare per esprimere la suddetta relazione fra dimensione dell'input e tempo di esecuzione di un
algoritmo. 


Sezione 2. Analisi sperimentale degli algoritmi
-----------------------------------------------

Un modo semplice per valutare la complessità di un algoritmo è quello di eseguirlo su un insieme significativo
di dati di input, registrando il tempo utilizzato per ogni esecuzione.
In Python questo è realizzato usando la funzione ``time()`` del modulo ``time``.
Essa restituisce il numero di secondi intercorsi a partire da un momento iniziale;
se ``time()`` è invocata immediatamente prima e dopo l'esecuzione dell'algoritmo saremo in grado di 
misurare il tempo intercorso come differenza dei due valori. 

Misurare il tempo utilizzato in questo modo rappresenta una buona stima della complessità dell'algoritmo,
ma non è una valutazione ottimale. Il tempo trascorso può dipendere, ad esempio, da altri processi eseguiti
sul computer quando il test è eseguito (poichè la CPU è condivisa fra numerosi processi): 
una misura attendibile dovrebbe considerare test statisticamente significativi ripetuti su sistemi identici,
con input e algoritmi altrettanto identici.
I risultati di tali test potrebbero essere visualizzati in una tabella (dimensione-input x tempo-esecuzione),
fornendo così una indicazione circa la relazione funzionale fra i due valori.

L'analisi sperimentale del tempo di esecuzione di un algoritmo è un approccio valido, in particolare nei casi
di raffinamento della qualità del codice, ma esistono alcune limitazioni:

- i tempi di esecuzione sperimentali di due algoritmi sono difficilmente confrontabili,
  a meno che gli esperimenti non siamo condotti in condizioni identiche;
- solo un numero limitato di insiemi di test è solitamente disponibile;
- l'algoritmo deve essere completamente implementato per realizzare una analisi sperimentale
  (cioè, deve essere completamente tradotto in uno specifico linguaggio di programmazione).

Questi fattori rappresentano una seria limitazione all'uso dei metodi sperimentali.


Sezione 3. Analisi formale degli algoritmi
------------------------------------------

Con il termine *analisi formale* si descrive un approccio per analizzare la complessità degli
algoritmi in modo indipendente dagli ambienti hardware e software, attraverso lo studio della
descrizione di alto livello dell'algoritmo (invece che della sua implementazione completa),
considerando tutti i possibili valori di input.
A tale scopo, definiamo un insieme di operazioni primitive ed eseguiamo l'analisi della
composizione di tutte le operazioni primitive che formano l'algoritmo (nella sua forma di
frammento di codice effettivo o di pseudo-codice indipendente dal linguaggio).
L'insieme delle operazioni primitive include:

- l'accesso alla memoria per assegnazione o lettura;
- Le operazioni aritmetiche e i confronti fra valori;
- la chamata di funzioni o metodi;
- l'accesso alle liste;
- la restituzione di un valore da parte di una funzione.

Ogni operazione primitiva corrisponde a una istruzione di basso livello con un tempo di esecuzione costante.
Invece di misurare il tempo di esecuzione dell'algoritmo, conteremo quante volte tali operazioni sono eseguite,
e useremo questo numero come una misura valida del tempo di esecuzione (si assume che il conteggio delle
operazioni sia correlato al tempo di esecuzione su un dato computer, poiché a ogni operazione primitiva
corrisponde un numero finito di istruzioni, e poiché il numero di operazioni primitive è comunque fissato a priori).

Per descrivere la crescita del tempo di esecuzione, si associa ad ogni algoritmo una funzione \\(f(n)\\) 
che caratterizza il numero di operazioni primitive che sono eseguite in funzione della 
dimensione dell'input \\(n\\).
Alcune funzioni sono usualmente usate nello studio della complessità computazionale:

- la funzione *constante*  :math:`f(n)=c`, con \\(c\\) numero costante ;
- la funzione *logaritmica* \\( f (n) = \log_b n \\), con \\( b \\) numero costante ;
- la funzione *lineare* \\( f(n)=n \\);
- la funzione \\( f(n)= n \log n \\);
- la funzione *quadratica* \\( f(n)=n^2 \\);
- la funzione *polinomiale* \\( f(n)=n^k \\), con \\( k \\) numero costante;
- la funzione *esponenziale* \\( f(n)=b^n \\), con \\( b \\) numero costante.

Forniamo di seguito la descrizione di queste funzioni.

**La funzione costante.**
Per ogni argomento \\(n\\), la funzione costante restituisce il valore \\(c\\).
Per quanto semplice, questa funzione è estremamente utile nell'analisi degli algoritmi, poichè essa esprime
il numero di passi necessari per effettuare una operazione elementare su un generico computer,
come sommare due numeri, assegnare un valore a una variabile, o confrontare due numeri.
Una complessità costante è la più bassa che un algoritmo può avere. 

**La funzione logaritmica.**
Questa funzione è definita come \\(x = \log_b n\\), se e solo se \\(b^x = n\\).
Per definizione, \\(\log_b 1 = 0\\). Il valore costante \\(b\\) è la *base* del logaritmo.
La base più comune per la funzione logaritmica in informatica è 2, in quanto gli interi sono
rappresentati in notazione binaria, e poiché una operazione comune in molti algoritmi è quella di
dividere ripetutamente a metà l'input.
Ometteremo la base dalla notazione quando essa è uguale a 2, cioè \\(\log n = \log_2 n\\).
La complessità logaritmica è tipica per gli algoritmi ricorsivi che operamo su parti sempre
più piccole dell'input. 

**La funzione lineare.**
Dato un valore di input \\(n\\), la funzione lineare restituisce lo stesso valore \\(n\\).
Questa funzione è utilizzata ogni volta che si effettua una singola operazione primitiva su 
ogni insieme formato da \\(n\\) elementi.
Ad esempio, confrontare un numero \\(x\\) con ognuno degli elementi di una sequenza di dimensione \\(n\\)
richiede \\(n\\) confronti.
La funzione lineare rappresenta il miglior tempo di esecuzione per gli algoritmi che processano oggetti
che non sono già nella memoria del computer (leggere \\(n\\) oggetti richiede \\(n\\) operazioni).
In generale, gli algoritmi con complessità lineare sono considerati come i più efficienti.

**La funzione \\(n\log n\\).**
Questa funzione assegna a un input \\(n\\) il valore di \\(n\\) moltiplicato per il logaritmo in base 2 
di \\(n\\).
Il tasso di crescita di questa funzione è ovviamente più rapido di quello della funzione lineare, 
ma rimane al di sotto della funzione quadratica; quindi un algoritmo con tempo di esecuzione 
proporzionale a \\(n \log n\\) deve essere preferito rispetto a uno con tempo di esecuzione quadratico.
Molti importanti algoritmi esibiscono un tempo proporzionale a questa funzione.
Ad esempio, il miglior algoritmo per ordinare \\(n\\) valori numerici richiede tempoproporzionale a
\\(n\log n\\).

**La funzione quadratica.**
Dato un valore di input \\(n\\), questa funzione restituisce il valore \\(n^2\\);
nell'analisi degli algoritmi, essa appare in presenza di cicli annidati, in cui il ciclo interno
effettua un numero lineare di operazioni, e il ciclo esterno è effettuato un numero lineare di volte.
In questo caso l'algoritmo effettua \\(n\\) x \\(n = n^2\\) operazioni; ad esempio, 
alcuni algoritmi di ordinamento hanno complessità quadratica.

**La funzione polinomiale.**
Consideriamo ora la funzione polinomiale \\(f(n) = n^k\\), con \\(k\\) un numero naturale fissato. 
Questa funzione assegna a un valore di input \\(n\\) il prodotto di \\(n\\) per se stesso, \\(k\\) volte.
Essa è molto comune nel contesto dell'analisi degli algoritmi e rappresenta, per motivi teorici, 
il limite di ciò che è considerato *feasible* (fattibile) con un computer.
Occorre tuttavia notare che un algoritmo con tempo di esecuzione polonomiale \\( n^\{354\} \\) 
non è da considerare feasible.

**La funzione esponenziale.**
Un'altra funzione utilizzata è la funzione esponenziale, \\(f(n)=b^n\\),
con \\(b\\) costante positiva, chiamata la *base*.
Essa assegna a un argomento in input \\(n\\) il valore ottenuto moltiplicando la base \\(b\\) per se stessa \\(n\\) volte.
Come per la funzione logaritmica, la base più comune è 2.
Ad esempio, se un ciclo inizia effettuando una operazione, e raddoppia il numero di operazioni ad ogni iterazione,
il numero totale di operazioni al momento della \\(n-\\)ma iterazione è \\(2^n\\).
La funzione esponenziale, e ogni funzione che cresce in modo simile ad essa, è considerata *unfeasible* in 
informatica. Se dimostramo che un algoritmo, nel caso migliore, ha complessità esponenziale, 
esso sarebbe praticamento non eseguibile in qualsiasi computer, indipedentemente dalla sua potenza di calcolo.
Ciò dipende dal tasso di crescita estremamente alto. Molti dei problemi aperti in informatica sono in qualche modo
connessi alla complessità esponenziale. 

L'analisi formale della complessità di un algoritmo richiede che il tempo di esecuzione sia espresso come una
funzione della dimensione dell'input; solitamente si considerano gli scenari dei casi *migliore*, *peggiore*
e *medio* (*best*-, *worst*- e *average-case*).
Il caso migliore si presenta quando la configurzione dell'input permette all'algoritmo di lavorare alla
massima velocità; ad esempio, ordinare un insieme di dati che sono giàordinato dovrebbe richiedere 
il minor tempo possibile. Al contrario, il caso peggiore si verifica quando l'algoritmo deve effettuare
tutti i passi possibili per raggiungere il suo obiettivo; si immagini di dover ordinare un insieme di numeri 
in ordine crescente, partendo con l'insieme ordinato in modo decrescente.
Questi due casi sono più semplici da valutare rispetto al caso medio. In questo caso dovremmo essere in grado
di esprimere il tempo di esecuzione come una funzione della dimensione dell'input in base a una media 
su tutti i possibili valori in input con la stessa dimensione. Ciò richiede la definizione della distribuzione
probabilistica dell'input, e va oltre gli obiettivi di queste lezioni.
Considereremo, per valutare la complessità di un algoritmo, sempre il caso peggiore.


Sezione 4. Analisi asintotica
-----------------------------

Abbiamo già sottolineato che nell'analisi degli algoritmi desideriamo caratterizzare la complessità temporale
usando funzioni che associano alla dimensione dell'input \\(n\\) valori che corrispondono ai fattori che
maggiormente influenzano il tasso di crescita.
Di seguito forniremo alcune notazioni che esprimono questa relazione. 

Ogno passo elementare della descrizione di un algoritmo (come pure della sua implementazione)
corrisponde a un numero finito di operazioni primitive; l'obiettivo è di contare il numero di 
operazioni eseguite, a meno di fattori costanti.
 
Nel seguente frammento di codice, la funzione ``find_max`` ricerca l'elemento più grande 
di una lista ``data`` in Python::

 1  def find_max(data):
 2    # Return the maximum element from a list
 3    biggest = data[0]
 4    for val in data:
 5      if val > biggest:
 6        biggest = val
 7    return biggest

Questo algoritmo esegue un numero di operazioni proporzionale a \\(n\\), 
poiché il ciclo è eseguito una volta per ogni elemento di ``data`` 
e, per ogni passo, è eseguito un numero costante di operazioni primitive (confronto e assegnamento).

**La notazione \\( O \\).**
Siano \\(f\\) e \\(g\\) funzioni che associano numeri reali positivi a numeri interi positivi.
Si dice che  \\(f(n)\\) è \\( O(g(n)) \\) se esistono un numero reale \\(c > 0\\) e un intero \\(n_0 \\geq 1\\)
tali che \\(f(n) \\leq cg(n)\\), con \\(n \\geq n_0\\).
La notazione si legge come \\(f(n)\\) è "O grande" di \\(g(n)\\), o \\(f(n)\\) "è dell'ordine di" \\(g(n)\\),
o \\(f(n) \\in O(g(n))\\); la funzione \\(f\\) è “minore di, o uguale a” un'altra funzione \\(g\\)
a meno di un fattore costante e in senso asintotico, dato che \\(n\\) tende all'infinito.

La notazione \\(O\\) è usata per caratterizzare tempo e spazio di esecuzione degli algortimi in funzione del
parametro \\(n\\), che è solitamente inteso come una misura della dimensione del problema.
Ad esempio, se \\(n\\) denota il numero degli elementi della sequenza ``data`` 
nel precedente codice per ``find_max``, diremo che l'algoritmo ha tempo di esecuzione \\(O(n)\\). 
Ciò perché l'assegnamento prima del ciclo richiede un numero costante di operazioni primitive, 
e lo stesso accade per ogni iterazione del ciclo; infine, il ciclo è eseguito \\(n\\) volte.
Dato che ogni operazione primitiva richiede tempo costante, si ha che il tempo di esecuzione 
dell'algoritmo su un input di dimensione \\(n\\) è al più il prodotto di una costante per \\(n\\),
cioé \\(O(n)\\).

**La notazione \\( \\Omega \\).**
Siano \\(f\\) e \\(g\\) funzioni che associano numeri reali positivi a numeri interi positivi.
Si dice che \\(f(n)\\) è \\( \\Omega (g(n)) \\), se \\(g(n)\\) è \\(O(f(n))\\), cioé se esistono un numero reale
\\(c> 0\\) e un intero \\(n_0 \\geq 1\\)
tali che \\(f(n) \\geq cg(n)\\), con \\(n \\geq n_0\\).
La notazione si legge come \\(f(n)\\) è "Omega grande" di \\(g(n)\\); la funzione \\(f\\) is "maggiore di,
o uguale a" una funzione \\(g\\), a meno di un fattore costante e in senso asintotico.

**La notazione \\( \\Theta \\).**
Siano \\(f\\) e \\(g\\) funzioni che associano numeri reali positivi a numeri interi positivi.
Si dice che \\(f(n)\\) è \\(\\Theta (g(n))\\), se \\(f(n)\\) è \\(O(g(n))\\) e \\(f(n)\\) è \\(\\Omega (g(n))\\),
cioé esistono i reali \\(c' > 0\\) e \\(c'' > 0\\), e un intero \\(n_0 \\geq 1\\) tali che 
\\(c'g(n) \\leq f(n) \\leq c''g(n)\\).
Ciò indica che le due funzioni hanno lo stesso tasso di crescita.

**Analisi comparativa.**
La notazione \\(O\\) può essere usata per ordinare classi di funzioni in base al tasso di crescita asintotico.
Le sette funzioni introdotte in precedenza possono essere ordinate nel modo seguente: 
\\(1\\), \\(\\log n\\), \\(n\\), \\(n\\log n\\), \\(n^k\\), \\(2^n\\).
Ciò significa che se \\(f(n)\\) precede \\(g(n)\\) nella sequenza, allora \\(f(n)\\) è \\(O(g(n))\\).
Ad esempio, se due algoritmi risolvono lo stesso problema con tempi d esecuzione \\(O(n)\\) e \\(O(n^2)\\), 
rispettivamente, il primo è asintoticamente migliore del secondo.

Appare interessante confrontare i tassi di crescita delle sette funzioni, 
come nella tabella sottostante.

======= ============= === ============== ========= ========== =============
\\(n\\) \\(\\log n\\) n   \\(n \log n\\) \\(n^2\\) \\(n^3\\)  \\(2^n\\)
======= ============= === ============== ========= ========== =============
8       3             8   24             64        512        256
16      4             16  64             256       4,096      65,536
32      5             32  160            1,024     32,768     4,294,967,296
64      6             64  384            4,096     262,144    1.84×10^19
128     7             128 896            16,384    2,097,152  3.40×10^38
256     8             256 2,048          65,536    16,777,216 1.15×10^77
======= ============= === ============== ========= ========== =============

La seguente tabella analizza la dimensione massima consentita per un input che è processato da un algoritmo 
in 1 secondo, 1 minuto, 1 ora, e mostra che un algoritmo asintoticamente lento è battuto da uno 
asintoticamente più veloce, anche se il fattore costante per quello asintoticamente più veloce è
molto peggiore.

+------------------------------+-----------------------------------+
|                              | Massima dimensione   ( \\(n\\) )  |
+------------------------------+------------+----------+-----------+
| Tempo esecuz \\( \\mu\\)-sec | 1 secondo  | 1 minuto | 1 ora     |
+==============================+============+==========+===========+
| \\(400n\\)                   | 2,500      | 150,000  | 9,000,000 |
+------------------------------+------------+----------+-----------+
| \\(2n^2\\)                   | 707        | 5,477    | 42,426    |
+------------------------------+------------+----------+-----------+
| \\(2^n\\)                    | 19         | 25       |    31     |
+------------------------------+------------+----------+-----------+

La seguente tabella mostra la massima dimensione possibile raggiungibile da un nuovo problema per
ogni quantità di tempo, assumendo che gli algoritmi con un dato tempo di esecuzione ora sono eseguiti 
su una macchina 256 volte più veloce della precedente. 
Ogni input è funzione di ``m``, la precedente dimensione massima del problema.
Anche usando un hardware più veloce, non è possibile compensare la limitazione dovuta a un
algoritmo asintoticamente più lento.

+------------------------------+---------------------------+
| Tempo esecuz \\( \\mu\\)-sec | Nuova massima dimensione  |
+==============================+===========================+
| \\(400n\\)                   | \\(256m\\)                |
+------------------------------+---------------------------+
| \\(2n^2\\)                   | \\(16m\\)                 |
+------------------------------+---------------------------+
| \\(2^n\\)                    | \\(m+8\\)                 |
+------------------------------+---------------------------+

Si noti che l'uso della notazione \\(O\\) può essere fuorviante nel caso in cui il fattore costante sia molto grande.
Una funzione come \\(10^{100} n\\), che è comunque lineare, è decisamente unfeasible;
ciò accade perché il fattore costante è comunque troppo grande da poter essere rappresentato in un computer.
In generale, i fattori costanti si annidano nella complessità degli algoritmi. 
Un algoritmo "veloce" è uno eseguito in tempo \\(O(n\log n)\\) (con un fattore costante ragionevole); 
in alcuni contesti un algoritmo con complessità temporale \\(O(n^2)\\) è considerato accetttabile.
Il confine fra efficienza e inefficienza deve essere tracciato fra gli algoritmi eseguibili in tempo
polinomiale \\(O(n^k)\\) (con un \\(k\\) basso) e quelli eseguibili in tempo esponenziale \\(O(k^n)\\);
la distinzione fra algoritmi polynomial-time e exponential-time è considerata 
una misura robusta della feasibility.


Sezione 5. Esempi di analisi di algoritmi
-----------------------------------------

In questa sezione descriviamo alcuni semplici algoritmi, e mostriamo le rispettive analisi dei tempi di
esecuzione utilizzando le notazioni introdotte in precedenza. 
Useremo una istanza della classe list di Python, ``data``, come rappresentazione di un array di valori numerici.
Una chiamata alla funzione ``len(data)`` è eseguita in tempo costante, dato che ogni istanza di una
lista contiene una variabile che registra la lunghezza della lista stessa;
in questo modo un accesso diretto a tale variabile restituisce il valore voluto in tempo \\(O(1)\\).
Inoltre, la classe list di Python consente l'accesso a un elemento arbitrario della lista
(usando ``data[j]``, per ogni \\(j\\) ) in tempo costante. 
Ciò accade poiché le liste in Python sono implementate come sequenze basate su array, e i riferimenti
agli elementi di una lista sono memorizzati in blocchi consecutivi della memoria. 
Il \\(j-\\)mo elemento di una lista è raggiunto usango l'indice come un offset nell'array. 
Si noti che sarebbe possibile utilizzare qualsiasi linguaggio di programmazione,o qualsiasi pseudo-codice,
per descrive l'algoritmo: l'analisi della complessità rimane la stessa.


Sezione 5.1 Trovare il massimo
==============================

Considerate le precedenti osservazioni circa  ``data``, ``len``, e ``data[j]``, possiamo
confermare l'analisi preliminarmente fatta sull'algoritmo ``find_ max``,
con \\(n\\) la lunghezza di ``data``.
L'inizializzazione di ``biggest`` a ``data[0]`` usa tempo \\(O(1)\\).
Il ciclo è eseguito \\(n\\) volte e, per ogni iterazione, esso esegue un confronto e un eventuale assegnamento.
Infine, lo statement ``return`` usa tempo \\(O(1)\\) in Python.
La funzione è quindi eseguita in tempo \\(O(n)\\).

Sezione 5.2 Prefix averages
===========================

Sia \\(S\\) una sequenza di \\(n\\) numeri; si vuole produrre una sequenza \\(A\\),
tale che \\(A[j]\\) sia la media degli elementi \\(S[0], \\ldots ,S[j]\\), per \\(j = 0, \\ldots ,n-1\\).
Questo problema è chiamato il *prefix averages* di una sequenza, e ha numerose applicazioni.
Ad esempio, date le rate di un mutuo, anno dopo anno, un investitore potrebbe desiderare di 
calcolare le medie annuali delle rate per gli anni più recenti.
Oppure, data una registrazione degli accessi al web, si potrebbe desiderare di sapere le medie
degli accessi su vari intervalli di tempo. 

La prima implementazione di un algoritmo per calcolare i prefix averages è nel seguente codice.
Ogni elemento di \\(A\\) è calcolato separatamente, usando un ciclo per calcolare le somme parziali::

 1  def prefix_average(S):
 2    # Return list A such that A[j] equals average of S[0], ..., S[j], for all j
 3    n = len(S)
 4    A = [0]*n                # list of n zeros
 5    for j in range(n):
 6      total = 0
 7      for i in range(j + 1):
 8        total += S[i]
 9      A[j] = total / (j+1)    # the j-th average
 10   return A

L'assegnamento ``n = len(S)`` è eseguito in tempo costante.
Lo statement ``A = [0]*n`` crea e inizializza una lista di lunghezza \\(n\\), con tutti gli elementi uguali a zero;
esso usa un numero costante di operazioni primitive per ogni elemento, e quindi è eseguito in tempo \\(O(n)\\).
Il corpo del ciclo esterno è controllato dal contatore \\(j\\), ed è eseguito \\(n\\) volte,
per \\(j = 0, \\ldots ,n-1\\).
Ciò implica che gli statement ``total = 0`` e ``A[j] = total / (j+1)`` sono eseguiti \\(n\\) volte ciascuno.
Essi contribuiscono con un numero di operazioni primitive proporzionale a \\(n\\), cioé \\(O(n)\\) volte.
Il corpo del ciclo interno, che è controllato dal contatore \\(i\\), è eseguito \\(j+1\\) volte,
in base al valore corrente del contatore del ciclo esterno \\(j\\).
Quindi, lo statement ``total += S[i]`` è eseguito \\(1+2+3+ \\ldots +n\\) volte, cioé \\(n(n+1)/2\\);
ciò significa un tempo \\(O(n^2)\\).
Il tempo di esecuzione di questa implementazione è dato dalla somma di tre termini: il primo e il secondo
sono \\(O(n)\\), e il terzo è \\(O(n^2)\\).
Il tempo di esecuzione totale di ``prefix_average`` è \\(O(n^2)\\).

Un algoritmo con complessità lineare nel tempo è fornito con il seguente codice::

 1  def prefix_average_linear(S):
 2    # Return list A such that A[j] equals average of S[0], ..., S[j], for all j
 3    n = len(S)
 4    A = [0]*n                # list of n zeros
 5    total = 0
 6    for j in range(n):
 7      total += S[j]
 8      A[j] = total / (j+1)    # average based on current sum
 9    return A

Nel primo algoritmo la *prefix sum* era ricalcolata per ogni valore di \\(j\\), da cui il comportamento quadratico.
In questo nuovo algoritmo si memorizza la prefix sum corrente in modo dinamico, 
calcolando \\(S[0]+S[1]+ \\ldots +S[j]\\) come ``total += S[j]``, in cui  ``total`` è uguale alla somma
\\(S[0]+S[1]+\\ldots +S[ j-1]\\) calcolata nel passo precedente del ciclo su \\(j\\).
L'inizializzazione delle variabili usa tempo \\(O(1)\\).
L'inizializzazione della lista usa tempo \\(O(n)\\).
C'è un singolo ciclo, controllato dal contatore \\(j\\);
la gestione di questo contatore per mezzo dell'iteratore range costa tempo \\(O(n)\\).
Il corpo del ciclo è eseguito in \\(n\\) volte, per \\(j = 0, \\ldots ,n-1\\).
Quindi, gli statement ``total += S[j]`` e ``A[j] = total / (j+1)`` sono eseguiti \\(n\\) volte.
Poiché ognuno di questi statement usa tempo \\(O(1)\\) per iterazione, 
il loro contributo totale è \\(O(n)\\).
Il tempo di esecuzione totale dell'algoritmo ``prefix_average_linear`` è \\(O(n)\\), 
che è molto meglio del tempo quadratico del primo algoritmo fornito.


Sezione 5.3 Three-way set disjointness
======================================

Date tre sequenze di numeri, \\(A\\), \\(B\\) e \\(C\\), senza valori duplicati, il problema della 
*three-way set disjointness* (disgiunzione fra tre insiemi) consiste nel decidere
se l'intersezione delle tre sequenze sia vuota.
Una semplice implementazione in Python è datadi seguito::

 1  def disjoint1(A, B, C):
 2    # Return True if there is no element in common to all three lists
 3    for a in A:
 4      for b in B:
 5        for c in C:
 6          if a == b == c:
 7            return False    # there is a common value
 8    return True             # sets are disjoint

Questo algoritmo cicla attraverso ogni possibile tripla di valori dei tre insiemi e verifica se esistono
valori uguali. Se ogni insieme ha dimensione \\(n\\), la complessità nel caso peggiore è \\(O(n^3)\\).
Si noti che se due elementi selezionati da \\(A\\) e da \\(B\\) non sono uguali, è inutile 
confrontarli con i valori prelevati da \\(C\\); quindi è possibile fornire il seguente
codice migliorato::

 1  def disjoint2(A, B, C):
 2    # Return True if there is no element in common to all three lists
 3    for a in A:
 4      for b in B:
 5        if a == b:           # check C only if there is a match from A and B
 6          for c in C:
 7            if a == c        # here a == b == c
 8              return False   # there is a common value
 9     return True             # sets are disjoint

Il ciclo su \\(A\\) richiede tempo \\(O(n)\\).
Il ciclo su \\(B\\) richiede tempo \\(O(n^2)\\) in totale, poiché esso è eseguito \\(n\\) volte.
Quindi, il confronto ``a == b`` è eseguito \\(O(n^2)\\) volte.
Esistono \\(n\\)x\\(n\\) coppie \\((a,b)\\) da considerare ma, sotto l'ipotesi che non esistono valori duplicati, 
esistono al massimo \\(n\\) coppie con a e b uguali. 
Quindi, il ciclo più interno su \\(C\\), e i comandi all'interno di esso, usano al massimo tempo \\(O(n^2)\\). 
Il tempo totale speso da questo algoritmo è \\(O(n^2)\\).

Sezione 5.4 Element uniqueness
==============================

Data una sequenza \\(S\\) con \\(n\\) elementi, vogliamo decidere se tutti gli elementi di quella collezione sono
diversi fra essi. 
La prima soluzione usa un ovvio algoritmo iterativo, che verifica se tutte le possibili coppie di indici
\\( (j,k) \\), con \\(j < k\\), si riferiscano a elementi diversi fra loro::

 1  def unique1(S):
 2    # Return True if there are no duplicate elements in sequence S
 3    for j in range(len(S)):
 4      for k in range(j+1, len(S)):
 5       if S[j] == S[k]:
 6          return False        # there is a duplicate pair
 7    return True               # all elements are distinct

La prima iterazione del ciclo esterno provoca \\(n-1\\) iterazioni del ciclo interno,
la seconda iterazione del ciclo esterno provoca \\(n-2\\) iterazioni del ciclo interno, e così via.
Il tempo di esecuzione di questa funzione, nel caso peggiore, è proporzionale a
\\((n-1)+(n-2)+ \\ldots +2+1\\), cioé \\(O(n^2)\\).
Si osservi che se la sequenza degli elementi è ordinata, gli elementi duplicati saranno vicini.
Tutto ciò che occorre fare è realizzare un'unica scansione della sequenza ordinata, cercando i 
duplicati consecutivi. Un implementazione in Python è la seguente::

 1  def unique2(S):
 2    # Return True if there are no duplicate elements in sequence S
 3    temp = sorted(S)            # sorting of S
 4    for j in range(1, len(temp)):
 5      if temp[j-1] == temp[j]:
 6        return False            # there is duplicate pair
 7    return True                 # all elements are distinct

La funzione ``sorted`` restituisce una copia della lista di partenza, con gli elementi in ordine crescente.
Il tempo di esecuzione nel caso medio è \\(O(n\\log n)\\).
Il ciclo successivo richiede tempo \\(O(n)\\), da cui discende che l'intero
algoritmo richiede tempo \\(O(n \\log n)\\).


Sezione 6. Un approccio matematico: calcolo delle invarianti
------------------------------------------------------------

Nelle sezioni precedenti abbiamo fornito alcuni esempi di valutazione della complessità di alcuni
algoritmi. L'approccio è sostanzialmente sempre lo stesso: l'algoritmo è scritto
(usando un linguaggio relae o una pseudo-codice), e si effettua la valutazione
delle operazioni di base (in tempo costante) e dei cicli annidati; ciò consente di stabilire una
relazione fra la dimensione dell'input e il numero di operazioni effettuate.
In altri termini, siamo in grado di trovare un "accordo" relativo al tempo utilizzato dall'algoritmo;
ciò non implica, però, che abbiamo fornito una dimostrazione formale del limite calcolato. 

Una delle tecniche utilizzate nel campo dell'analisi della complessità è chiamata la
*loop invariant*, o *calcolo delle invarianti*;
è simile, se non uguale, alla tecnica matematica della *induzione*.
Si supponga di voler provare che un enunciato \\(L\\) relativo a un ciclo sia vero;
definiamo \\(L\\) in termini di enunciati più piccoli \\(L_0, L_1, \\ldots , L_k\\), con:

- l'enunciato iniziale, \\(L_0\\), vero prima che il ciclo inizi;
- se \\(L_{j−1}\\) è vero prima della iterazione \\(j\\), allora \\(L_j\\) sarà vero dopo l'iterazione \\(j\\);
- l'enunciato finale, \\(L_k\\), implica che l'enunciato \\(L\\) sia vero.

Dato il seguente codice per la funzione ``find``, che dovrebbe trovare il più piccolo indice 
per cui l'elemento ``val`` è presente nella sequenza ``Seq``, usiamo la tecnica di loop-invariant 
per provare la sua correttezza::

 1  def find(Seq, val):
 2    # Return index j such that Seq[j] == val, -1 otherwise
 3    n = len(Seq)
 4    j = 0
 5    while j < n:
 6      if Seq[j] == val:
 7        return j
 8      j += 1
 9    return −1

Per dimostrare che ``find`` è corretto, definiamo una serie di enunciati \\(L_j\\),
uno per ogni iterazione \\(j\\) del ciclo ``while``:

**\\(L_j\\):** ``val`` è diverso da ognuno dei primi \\(j\\) elementi di ``Seq``.

Ciò è vero se \\(j=0\\), cioé alla prima iterazione del ciclo, poiché non esistono elementi
fra i primo 0 in ``Seq``.
Alla \\(j\\)-ma iterazione, l'elemento ``val`` è confrontato con ``Seq[j]``,
e \\(j\\) è restituito se i due elementi sono uguali; altrimenti esiste un altro elemento
diverso da ``val``, e l'indice \\(j\\) è incrementato.
L'enunciato \\(L_j\\) è vero per il nuovo valore di \\(j\\); quindi è vero all'inizio della
successiva iterazione. 
Se il ciclo ``while`` termina senza restituire un indice in ``Seq``, allora \\(j = n\\).
Ciò implica che \\(L_n\\) è vero, poichè non esistono elementi di ``Seq`` uguali a ``val``.
Quindi l'algoritmo restituisce correttamente −1.
