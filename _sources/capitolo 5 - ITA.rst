Capitolo Cinque: Liste concatenate
**********************************

In questo capitolo introduciamo una struttura dati nota come *lista concatenata*, che fornisce un'alternativa
alle sequenze basate su array di Python. 
Entrambe mantengono gli elementi ordinati, usando strategie differenti. 
Un array fornisce una rappresentazione centralizzata, con una parte della memoria usata per ospitare
riferimenti agli elementi. 
Una lista concatenata si basa su una rappresentazione più distribuita in cui un oggetto,
chiamato *nodo*, e allocato per ogni elemento. Ogni nodo mantiene un riferimento al relativo elemento, e almeno
un altro riferimento ai nodi vicini, allo scopo di rappresentare l'ordine lineare della sequenza.

Come sempre esiste un trade-off da considerare quando confrontiamo le due soluzioni. Ad esempio, non è possibile
accedere in modo efficiente agli elementi di una lista concatenata attraverso un indice, come
accade per le sequenze basate su array. Non si può conoscere direttamente, esaminando un nodo, la sua 
posizione sulla lista.
Tuttavia le liste concatenate evitano alcuni svantaggi già rilevati per le sequenze basate su array.

Sezione 1. Liste concatenate singolarmente
------------------------------------------

Una *lista concatenata singolarmente* è una collezione di nodi che formano una sequenza lineare.
Ogni nodo contiene un campo *elemento* che è un riferimento a un oggetto della sequenza, e un campo
*next*, che è un riferimento al nodo successivo della lista, come nella figura seguente.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura51.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Il primo e l'ultimo nodo di una lista concatenata sono noti come *head* e *tail* (testa e coda)
della lista, rispettivamente. IL processo di spostarsi da un nodo al successivo, seguendo il
riferimento *next*, finché la coda della lista è raggiunta è chiamato *attraversamento*, 
o *link hopping*, o *pointer hopping*. La coda ha *None* come riferimento al nodo successivo.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura52.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Ogni nodo è rappresentato come un oggetto che conserva un riferimento al suo elemento e un 
riferimento al nodo successivo. Per rappresentare l'intera lista occorre mantenere un riferimento alla testa
della lista. Ogni altro nodo può essere trovato attraversando la lista; ciò implica che non serve
mantenere un riferimento diretto alla coda della lista, anche se questa è una pratica usuale che consente di
evitare tale attraversamento. 
In modo analogo, il numero totale di nodi della lista (la *dimensione*) è mantenuto per evitare 
ulteriori attraversamenti. Per motivi di semplicità, considereremo l'elemento del nodo come 
inserito direttamente nel modo stesso, come nella figura seguente.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura53.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

**Inserire un Elemento in testa a una lista concatenata singolarmente**

Una lista concatenata non ha una dimenzione prefissata, in quanto essa è uguale o proporzionale al numero
effettivo di elementi presenti sulla lista stessa. 
Un nuovo elemento può essere inserito in testa a una lista come mostrato nel successivo pseudocodice.
Il nuovo nodo è creato, il relativo elemento è inserito, il link next si riferisce alla testa corrente,
e la testa della lista si riferisce al nuovo nodo::

 1  add_first(L,e):
 2    newnode = Node(e)
 3    newnode.next = L.head
 4    L.head = newnode
 5    L.size = L.size+1

Mostriamo di seguito l'inserimento di un elemento in testa alla lista, prima dell'inserimento,
dopo la creazione del nuovo nodo, e dopo il riassegnamento della testa, rispettivamente.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura54.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


**Inserire un elemento in coda a una lista concatenata singolarmente**

Un elemento può essere inserito in coda alla lista, se manteniamo un riferimento al nodo coda,
come mostrato nel seguente pseudocodice. Il nuovo nodo è creato (con il relativo campo next
uguale a None), il campo next della coda corrente punta al nuovo nodo, e il riferimento alla coda
punta anch'esso al nuovo nodo::

 1  add_last(L,e):
 2    newnode = Node(e)
 3    newnode.next = None
 4    L.tail.next = newnode
 5    L.tail = newnode
 6    L.size = L.size+1


Mostriamo di seguito l'inserimento di un elemento in coda alla lista, prima dell'inserimento,
dopo la creazione del nuovo nodo, e dopoil riassegnamento della coda, rispettivamente.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura55.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


**Rimuovere un elemento da una lista concatenata singolarmente**

Questa operazione è illustrata nel seguente frammento di codice::

 1  remove_first(L):
 2    if L.head == None: error  # the list is empty
 3    L.head = L.head.next
 4    L.size = L.size−1

Mostriamo di seguito la rimozione della testa di una lista, prima della rimozione, dopo aver eliminato la vecchia
testa, e nella configurazione finale, rispettivamente.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura56.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Si osserva che cancellare l'ultimo nodo di una lista concatenata singolarmente
non è semplice. Per farlo occorre accedere al nodo che precede l'ultimo nodo,
e ciò può essere fatto unicamente partendo dalla lista e attraversandola interamente,
con un numero di salti uguale alla lunghezza della lista stessa. 
Per supportare questa operazione è conveniente avere una lista doppiamente concatenata.


Sezione 1.1 Implementare una pila con una lista concatenata singolarmente
=========================================================================

In questa sezione forniamo una implementazione Python del tipo di dato astratto pila, 
usando una lista concatenata singolarmente. Poichè tutte le operazioni sulla pila riguardano il suo top,
orientiamo il top della pila con la testa della lista.
Questa è la soluzione ottimale: possiamo inserire e cancellare elementi in testa alla lista 
in tempo costante. 

Una classe ``_Node`` è definita allo scopo di rappresentare ogni nodo della lista;
la classe non deve essere direttamente accessibile dall'utente dello stack, e quindi sarà definita
come non pubblica e annidata nella classe ``LinkedStack``.
La definizione di ``_Node`` è mostrata di seguito::

 class _Node:
   __slots__ = '_element' , '_next'        # streamline memory usage
   def __init__ (self, element, next):     # initialize node’s fields
     self._element = element               # reference to user’s element
     self._next = next                     # reference to next node

Un nodo ha due variabili di istanza: ``_element`` e ``_next``.
Definiamo ``__slots__`` poichè possono esistere più istanze di nodi nella stessa lista.
Il costruttore della classe ``_Node`` è progettato per consentire di specificare i valori per 
entrambi i campi di un nuovo nodo. 

Nel codice seguente ogni istanza di pila mantiene due variabili.
Il membro ``_head`` è un riferimento al nodo in testa alla lista (none, se la pila è vuota).
Il membro ``_size`` tiene traccia del numero di elementi presenti nella pila.

L'implementazione di ``push`` è simile a quella per l'inserimento in testa a una lista concatenata
singolarmente, come visto prima. Quando si inserisce un nuovo elemento nella pila occorre chiamare 
il costruttore della classe ``_Node`` come segue::

 self._head = self._Node(e, self._head)

Il campo ``_next`` del nuovo nodo è impostato con il riferimento al nodo top esistente, e 
``self._head`` è riassegnato al nuovo nodo::

 1  class LinkedStack:
 2    # Stack implementation with a singly linked list
 3
 4    # Node class nested in stack
 5    class Node:
 6      __slots__ = '_element' , '_next'
 7      def __init__(self, element, next):
 8        self._element = element
 9        self._next = next
 10
 11   # Stack methods
 12   def __init__(self):
 13     self._head = None
 14     self._size = 0
 15
 16   def __len__(self):
 17     return self._size
 18
 19   def is_empty(self):
 20     return self._size == 0
 21
 22   def push(self, e):
 23     self._head = self._Node(e, self._head)
 24     self._size += 1
 25
 26   def top(self):
 27     if self.is_empty( ):
 28       raise Empty('Stack is empty')
 29     return self._head._element
 30
 31   def pop(self):
 32     if self.is_empty( ):
 33       raise Empty('Stack is empty')
 34     answer = self._head._element
 35     self._head = self._head._next
 36     self._size −= 1
 37     return answer

L'obiettivo del metodo ``top`` è di restituire l'elemento al top della pila.
Se la pila è vuota è sollevata un'eccezione ``Empty``.
Altrimenti, ``self._head`` è il riferimento al primo nodo della lista concatenata, e l'elemento al top
della pila è ``self._head._element``.
Per  ``pop`` è mantenuto un riferimento locale all'elemento che è immagazzinato nel nodo
da rimuovere, e quell'elemento è restituito. Si noti che per tutti i metodi, nel caso peggiore, 
si usa tempo costante.


Sezione 1.2 Implementare una coda con lista concatenata singolarmente
=====================================================================

In questa sezione forniamo una implementazione del tipo di dato astratto coda, per mezzo di una lista 
concatenata singolarmente. 
Abbiamo bisogno di poter fare operazioni a entrambi gli estremi della coda, e quindi manterremo
i due riferimenti head e tail come variabili di istanza per ogni coda.
Allineiamo l'inizio della coda con la testa della lista, e la fine della coda con la parte finale della lista;
in questo modo possiamo accodare elementi in fondo e eliminarli dalla testa, come atteso.
L'implementazione della classe ``LinkedQueue`` è data nel seguente codice::

 1  class LinkedQueue:
 2    # Queue implementation with a singly linked list
 3
 4    class Node:
 5      __slots__ = '_element' , '_next'
 6      def __init__(self, element, next):
 7        self._element = element
 8        self._next = next
 9
 10   def __init__ (self):
 11     self._head = None
 12     self._tail = None
 13     self._size = 0
 14
 15   def __len__ (self):
 16     return self._size
 17
 18   def is_empty(self):
 19     return self._size == 0
 20
 21   def first(self):
 22     if self.is_empty( ):
 23       raise Empty('Queue is empty')
 24     return self._head._element
 25
 26   def dequeue(self):
 27     if self.is_empty( ):
 28       raise Empty('Queue is empty')
 29     answer = self._head._element
 30     self._head = self._head._next
 31     self._size −= 1
 33     if self.is_empty( ):       # special case if queue is empty
 33       self._tail = None        # removed head had been the tail
 34     return answer
 35
 36   def enqueue(self, e):
 37     newnode = self._Node(e, None)
 38     if self.is_empty( ):
 39       self._head = newnode
 40     else:
 41       self._tail._next = newnode
 42     self._tail = newnode
 43     self._size += 1

In molti aspetti la precedente implementazione è simile a quella della classe ``LinkedStack``;
ad esempio, la definizione della classe ``Node`` è la stessa. 
Il metodo ``dequeue`` per ``LinkedQueue`` è simile a quello ``pop`` per ``LinkedStack``:
entrambi rimuovono la testa della lista concatenata, con la differenza che la coda mantiene il 
riferimento al fondo. 
In genere una operazione alla testa non ha effetto sul fondo, ma quando il metodo ``dequeue`` 
è chiamato su una coda con un solo elemento, si rimuovono testa e fondo della lista
simultaneamente; ``self._tail`` è settato a None.
Qualcosa di simile accade nella implementazione di ``enqueue``.
Il nuovo nodo è il nuovo fondo della lista, ma quando esso è l'unico present diventa anche
la nuova testa.
Tutte leoperazioni hanno tempo di esecuzione costante nel caso peggiore, e l'uso dello spazio è lineare
nel numero di elementi della coda.


Sezione 2. Liste concatenate circolari
--------------------------------------

Una lista concatenata in cui la fine della lista usa il campo next per puntare alla testa della lista 
è chiamata *lista concatenata circolare*.
Essa offre un modello generale per dati che sono ciclici, cioè che non prevedono una particolare
nozione di inizio o fine, mantenendo comunque un riferimento a un nodo particolare della lista.
Tale nodo è chiamato *corrente*, e si avanza nella lista usando ``current.next``.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura57.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Un esempio di uso della lista circolare è il *round-robin scheduler*,
che itera attraverso una collezione di elementi in modo circolare, e serve
ogni elemento effettuando una certa azione su di esso.
Ad esempio, loscheduler seguente alloca una risorsa condivisa da un certo insieme di client
(come tempo di CPU assegnato da un sistema operativo, ad esempio).
Un round-robin scheduler ripete i passi seguenti su una coda Q::

 1 e = Q.dequeue()
 2 Service element e
 3 Q.enqueue(e)

Questi passi possono essere facilmente eseguiti usando la classe ``LinkedQueue`` 
introdotta in precedenza; tuttavia, si nota che occorre rimuovere un nodo dalla lista
(facendo tutti gli aggiustamenti opportuni alla testa della lista e per la sua dimensione),
creare un nuovo nodo, inserirlo alla fine della lista e incrementare la dimensione.
Tutto questo lavoro può essere evitato con una lista circolare, in cui il trasferimento di un oggetto
dall'inizio alla fien della listaè ottenuto semplicemente facendo avanzare un riferimento che 
identifica il confine della coda.

Nella prossima sezione forniremo una implementazione della classe ``CircularQueue``
che supporta l'ADT coda, con un nuovo metodo ``rotate( )`` che sposta il primo 
elemento della coda in fondo alla stessa. Un round-robin scheduler è realizzato con i seguenti passi::

 1. Service element Q.front()
 2. Q.rotate()

Sezione 2.1 Implementare una coda con una lista concatenata circolare
=====================================================================

Per implementare l'ADT coda con una lista concatenata circolare ci basiamo sulla precedente idea che
la coda abbia un inizio e una fine, con campo next della fine collegato alla testa.
In questo caso le uniche due variabili di istanza sono ``_tail``,
un riferimento al nodo in fondo alla coda,
e ``_size``, il numero corrente di elementi nella coda.
Siamo in grado di trovare la testa della coda semplicemente seguento il riferimento next
del fondo della coda, cioè ``self._tail._next``.
Possiamo accodare un nuovo nodo ponendolo dopo il fondo della coda eprima della testa corrente,
e rendendolo il nuovo fondo della coda.

Per implementare l'operazione di rimozione dell'inizio della coda e del suo inserimento in fondo
alla coda, aggiungiamo il metodo ``rotate``, che sostanzialmente esegue ``self._tail = self._tail._next``
(la vecchia testa diventa il nuovo fondo, e il nodo dopo la vecchia testa diventa la nuova testa).
L'implementazione completa è nel seguente codice::

 1  class CircularQueue:
 2    # Queue implementation using circularly linked list
 3
 4    class _Node:
 6      __slots__ = '_element' , '_next'
 7      def __init__(self, element, next):
 8        self._element = element
 9        self._next = next
 7
 8    def __init__(self):
 9      self._tail = None
 10     self._size = 0
 11
 12   def __len__ (self):
 13     return self._size
 14
 15   def is_empty(self):
 16     return self._size == 0
 17
 18   def first(self):
 19     if self.is_empty( ):
 20       raise Empty('Queue is empty')
 21     head = self._tail._next
 22     return head._element
 23
 24   def dequeue(self):
 25     if self.is_empty( ):
 26       raise Empty('Queue is empty')
 27     oldhead = self._tail._next
 28     if self._size == 1:
 29       self._tail = None
 30     else:
 31       self._tail._next = oldhead._next
 32     self._size −= 1
 33     return oldhead._element
 34
 35   def enqueue(self, e):
 36     newnode = self._Node(e, None)
 37     if self.is_empty( ):
 38       newnode._next = newnode
 39     else:
 40       newnode._next = self._tail._next
 41       self._tail._next = newnode
 42     self._tail = newnode
 43     self._size += 1
 44
 45   def rotate(self):
 46     if self._size > 0:
 47       self._tail = self._tail._next


Sezione 3. Liste doppiamente concatenate
----------------------------------------

Ci sono alcune limitazione nel progetto di una lista concatenata singolarmente.
Mentre possiamo inserire un nodo a ogi capo della lista in modo efficiente,
e possiamo rimuovere un nodo dalla testa della lista, non possiamo cancellare in modo
efficiente dalla fine della lista, o un nodo in una posizione interna.
Ciò accade perché dobbiamo aggiornare anche il riferimento``next`` del nodo che precede
il nodo da cancellare.

Una *lista doppiamente concatenata* è una lista in cui ogni nodo mantiene un riferimento
al nodo successivo ``next`` e al nodo precedente ``prev``.
Ciò consente l'inserimento e la cancellazione di nodi in posizione arbitraria nella lista.

**Sentinelle header e trailer**

L'implementazione standard di una lista doppiamente concatenata prevede due nodi speciali a
entrambi i capi della lista, *header* e *trailer*, rispettivamente.
Agiscono come sentinelle, e non contengono nessun elemento.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura58.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

In una lista vuota il campo ``next`` dell'header punta al trailer,  e il campo ``prev`` del trailer 
punta allo header.
In una lista non vuota il campo  ``next`` dello header si riferisce al nodo che contiene
il primo elemento della sequenza, e il campo ``prev`` del traile si riferisce al nodo che 
contiene l'ultimo elemento della sequenza.
L'aggiunta di questi due nodi semplifica le operazioni richeste.
In particolare, tutti gli inserimenti sono fatti nello stesso modo, poiché un nuovo nodo
sarà sempre collocato fra due nodi esistenti. 
Ogni elemento da cancellare sarà anch'esso sempre fra due modi.
Non è richiesta nessuna eccezione quando la lista è vuota.

**Inserimento e cancellazione con una lista doppiamente concatenata**

Ogni inserimento in tale lista sarà sempre fatto fra due nodi già esistenti.
Ad esempio, se un nuovo elemento è inserito all'inizio della sequenza, il nuovo nodo andrà fra
lo header e il nodo al momento successivo. Nella figura seguente mostriamo cosa accade quando 
si aggiunge un elemento a una lista doppiamente concatenata con header e trailer,prima dell'operazione,
dopo aver creato il nuovo nodo, e dopo aver collegato i vicino con esso.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura59.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Mostriamo di seguito come aggiungere un elemento in testa alla lista; la procedura è
la stessa del normale inserimento.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura510.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Per la cancellazione, i due vicini al nodo da cancellare devono essere direttamente collegati fra loro, 
bypassando il nodo originario. In questo modo il nodo non fa più parte della lista. 
La stessa implementazione è usata quando si cancella il primo o l'ultimo node della lista.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura511.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center


Sezione 3.1 Implementazione di una lista doppiamente concatenata
================================================================

Forniamo la definizione della classe ``DoublyLinkedBase``; 
è una classe di basso livello, e quindi non forniamo la sua interfaccia.
Vedremo che l'inserimento e la cancellazione di un nodo sono effettuate im tempo \\(O(1)\\),
nel caso peggiore.

La classe ``Node`` utilizzata qui è simile a quella della lista concatenata singolarmente,
ma include un attributo ``prev``, in aggiunta a ``next`` e ``element``.
il costruttore della lista istanzia i due nodi sentinella e li collega fra loro. 
Un dato membro ``size`` è mantenuto::

 1  class _DoublyLinkedBase:
 2
 3    class Node:
 4      __slots__ = '_element' , '_prev' , '_next'
 5      def __init__(self, element, prev, next):
 6        self._element = element
 7        self._prev = prev                # previous node reference
 8        self._next = next                # next node reference
 9
 10   def __init__(self):
 11     self._header = self._Node(None, None, None)
 12     self._trailer = self._Node(None, None, None)
 13     self._header._next = self._trailer        # trailer is after header
 14     self._trailer._prev = self._header        # header is before trailer
 15     self._size = 0
 16
 17   def __len__(self):
 18     return self._size
 19
 20   def is_empty(self):
 21     return self._size == 0
 22
 23   def _insert_between(self, e, predecessor, successor):
 25     newnode = self._Node(e, predecessor, successor)
 26     predecessor._next = newnode
 27     successor._prev = newnode
 28     self._size += 1
 29     return newnode
 30
 32   def _delete_node(self, node):
 33     predecessor = node._prev
 34     successor = node._next
 35     predecessor._next = successor
 36     successor._prev = predecessor
 37     self._size −= 1
 38     element = node._element                             # record deleted element
 39     node._prev = node._next = node._element = None      # useful for the garbage collection
 40     return element                                      # return deleted element

I metodi ``_insert_between`` e ``_delete_node`` sono utility non pubbliche; 
supportano l'inserimento e la cancellazione, rispettivamente, e richiedono uno o più
riferimenti a nodi come parametri. 
Il primo crea un nuovo nodo, con i campi inizializzati per collegarsi con i nodi vicini; 
anche i campi dei nodi vicini sono modificati per includere il nuovo nodo nella lista.
Il secondo metodo collega fra loro i nodi vicini del nodo da cancellare, bypassandolo.
I campi ``prev``, ``next``, e ``element`` del nodo cancellato sono impostati a None,
cosa che aiuta il garbage collector di Python.


Sezione 3.2 Implementazione di una deque con una lista doppiamente concatenata
==============================================================================

La coda double-ended (deque) può essere implementata con un array, e tutte le operazioni lavorano in tempo
*ammortizzato* \\(O(1)\\), data la necessità di ridimensionare l'array.
La stessa struttura dati può essere realizzata con una lista doppiament concatenata; 
in questo caso tutte le operazioni della deque lavorano in tempo \\(O(1)\\), nel caso peggiore.
L'implementazione della classe ``LinkedDeque`` eredita da ``_DoublyLinkedBase``;
ci basiamo sui metodi ereditati ``__init__`` (per inizializzare una nuova istanza),
``__len__``, ``is_empty``,  ``_insert_between`` e ``_delete_node``.

Come previsto, l'inserimento di un elemento in testa alla deque avviene inserendo
lo stesso fra lo header e il nodo immediatamente successivo; l'inserimento alla fine 
della deque avviene inserendo l'elemento immediatamente prima del trailer.
Queste operazioni hanno successo anche quando la deque è vuota e, in questo caso, il
nuovo nodo è inserito fra le due sentinelle::

 1  class LinkedDeque(_DoublyLinkedBase):
 2
 3    def first(self):
 4      if self.is_empty( ):
 5        raise Empty("Deque is empty")
 6      return self._header._next._element
 7
 8    def last(self):
 9      if self.is_empty( ):
 10       raise Empty("Deque is empty")
 11    return self._trailer._prev._element
 12
 13   def insert_first(self, e):
 14       self._insert_between(e, self._header, self._header._next)
 15
 16   def insert_last(self, e):
 17     self._insert_between(e, self._trailer._prev, self._trailer)
 18
 19   def delete_first(self):
 20     if self.is_empty( ):
 21      raise Empty("Deque is empty")
 22     return self._delete_node(self._header._next)
 23
 24   def delete_last(self):
 25     if self.is_empty( ):
 26       raise Empty("Deque is empty")
 27     return self._delete_node(self._trailer._prev)


Sezione 4. Liste posizionali
----------------------------

Pile, code e code double-ended consentono operazioni all'inizio o/e alla fine di una sequenza.
In questa sezione introduciamo un tipo di dato astratto che fornisce un modo per riferirsi
a elementi in qualsiasi posizione nella sequenza, e di effettuare inserimenti e 
cancellazioni su essi; è chiamata *lista posizionale*.

Un modo semplice di implementare questo tipo si struttura è quello di usare una sequenza
basata su array, in cui gli indici interi indicano la posizione degli elementi, o la
posizione in cui inserimento o cancellazione devono essere fatti.
Abbiamo già parlato della necessità di shiftare parte della sequenza quando un 
elemento in una data posizione è cancellato o inserito.

L'implementazione di questo tipo di dato con liste concatenate è altrettanto inefficiente, poichè
trovare un elemento conoscendo la sua posizione richiede l'attraversamento della lista e
ilconteggio degli elementi. Inoltre, la posizione degli elementi può mutare, a seguito di
inserimenti o cancellazioni.

Necessitiamo di una astrazione in cui la posizione è descritta in un altro modo, e in cui
possiamo gestire situazioni di inserimento e/o cancellazione in tempo \\(O(1)\\).
La lista concatenata potrebbe essere un candidato per l'implementazione, poichè il riferimento
al nodo potrebbe essere usato per descrivere la posizione di ogni elemento nella sequenza; 
i metodi ``_insert_between`` e ``_delete_node`` implementano l'inserimento e la cancellazione
generalizzati. 
Esistono alcuni motivi per cui questa non è una strada percorribile. 
Primo, l'uso diretto dei riferimenti ai nodi è una violazione dei principi di astrazione e 
incapsulamento, poichè consente all'utente di manipolare i nodi direttamente, 
invalidando potenzialmente la consistenza di una lista.
Poi, è sempre preferibile progettare strutture dati flessibili, usabili e robuste,
in cui i dettagli di basso livello non sono accessibili daparte dell'utente, e che possono
essere riprogettate facilmente per migliorare la performance.
Per questi motivi non ci basiamo direttamente sui nodi, o sui riferimenti ad essi, ma
introduciamo una astrazione indipendente della posizione per denotare la locazione di un
elemento in una lista, e definiamo un tipo di dato astratto lista posizionale che
incapsula una lista doppiamente concatenata.

Sezione 4.1 Il tipo di dato astratto lista posizionale
======================================================

Per definire una astrazione di una sequenza di elementi, insieme a un modo per identificare la 
locazione di ogni elemento, introduciamo i tipi di dato astratto *lista posizionale* e  *posizione*.
Una posizione è un marker all'interno di una lista e deve rimanere immutato quando ci sono 
cambiamenti nella lista; non è più valida solo se è usato un comando esplicito per cancellarla.
Il metodo ``p.element( )`` restituisce l'elemento immagazzinato nella posizione ``p``.

I metodi supportati da una lista **L** sono:

- **L.first( )**: restituisce la posizione del primo elemento di **L**, o None se **L** è vuota;
- **L.last( )**: restituisce la posizione dell'ultimo elemento di **L**, o None se **L** è vuota;
- **L.before(p)**: restituisce la posizione di **L** immediatamente prima della posizione **p**, o None se **p** è la prima posizione;
- **L.after(p)**: restituisce la posizione di **L** immediatamente dopo della posizione **p**, o None se **p** è l'ultima posizione;
- **L.is_empty( )**: restituisce True se la lista **L** non contiene elementi;
- **len(L)**: restituisce il numero di elementi nella lista;
- **iter(L)**: restituisce un iteratore in avanti per gli elementi della lista.

L'ADT lista posizionale include anche i seguenti metodi di update:

- **L.add_first(e)**: inserisce un nuovo elemento **e** in testa a **L**, restituendo la sua posizione;
- **L.add_last(e)**: inserisce un nuovo elemento **e** in coda a **L**, restituendo la sua posizione;
- **L.add_before(p, e)**: inserisce un nuovo elemento **e** prima della posizione **p** in **L**, restituendo la sua posizione;
- **L.add_after(p, e)**: inserisce un nuovo elemento **e** dopo la posizione **p** in **L**, restituendo la sua posizione;
- **L.replace(p, e)**: rimpiazza l'elemento in posizione **p** con l'elemento **e**, restituendo l'elemento eliminato;
- **L.delete(p)**: rimuove e restituisce l'elemento in posizione **p** in **L**, invalidando la posizione.

Se ``p`` non è una posizione valida per la lista, si verifica un errore.
Si noti che i metodi ``first( )`` e ``last( )`` restituiscono le posizione associate;
il primo elemento di una lista posizionale può essere determinato invocando ``L.first().element()``.
Questo è usato per navigare nella lista. Ad esempio, il codice seguente stampa tutti gli elementi
della lista ``poslis``::

 marker = poslis.first( )
 while marker is not None:
   print(marker.element( ))
   marker = poslis.after(marker)

Se ``after`` è chiamato sull'ultima posizione è restituito None.
Lo stesso accade quando ``before`` è chamato sulla prima posizione della lista, o quando 
``first`` o ``last`` sono chiamati su liste vuote.
La seguente tabella mostra una serie di operazioni su una lista posizionale L.
Variabili come p, q, ed r sono usate per identificare istanze di posizioni.

==================== ===================== ==============
Operazioni           Valore restituito     L
==================== ===================== ==============
L.add_last(3)        p                     3p
L.first( )           p                     3p
L.add_after(p,5)     q                     3p, 5q
L.before(q)          p                     3p, 5q
L.add_before(q,3)    r                     3p, 3r, 5q
r.element( )         3                     3p, 3r, 5q
L.after(p)           r                     3p, 3r, 5q
L.add first(7)       s                     7s, 3p, 3r, 5q
L.delete(L.last( ))  5                     7s, 3p, 3r
L.replace(p,10)      3                     7s, 10p, 3r
==================== ===================== ==============



Section 4.2 Implementazione con lista doppiamente concatenata
=============================================================

In questa sezione forniamo una implementazione della classe ``PositionalList`` 
usando una lista doppiamente concatenata; ogni metodo della classe è  eseguito nel caso peggiore
in tempo \\(O(1)\\). Per farlo, usiamo la classe ``DoublyLinkedBase`` introdotta in precedenza,
e costruiamo una interfaccia pubblica che rispetti la definizione dell'ADT lista posizionale.

La classe pubblica ``Position`` è annidata nella classe ``PositionalList``, e le sue istanze 
rappresentano le locazioni degli elementi all'interno della lista. 
I metodi di ``PositionalList`` possono creare istanze di ``Position`` in modo ridondante,
cioè che si riferiscono allo stesso nodo (ad esempio, quando il primo e l'ultimo nodo sono lo stesso).
Per questo motivo, i metodi ``__eq__`` e ``__ne__`` sono definiti per verificare se due 
posizioni si riferiscono allo stesso nodo.

Il metodo non pubblico ``_validate`` verifica che una posizione sia valida, e determina il nodo sottostante.
Si noti che una posizione mantiene un riferimento a un nodo di una lista concatenata, e anche un
riferimento all'istanza della lista che contiene quel nodo.
Ciò implica che siamo in grado di conoscere quando una istanza di una posizione non appartiene alla
lista indicata, e quando una istanza di posizione è valida, ma si riferisce a un nodo che non è più
parte di quella lista (questo è fatto semplicemente, poiché il ``_delete_node`` della classe base
inizializza a None i riferimenti precedenti e successivi di un nodo cancellato).
Per  accedere alla lista posizionale definiamo metodi che usano  ``_validate``
per fare un "unwrap" di ogni posizione, e che usano ``_make_position`` per fare un "wrap" 
dei nodi come istanze di ``Position`` da restituire all'utente::

 1  class PositionalList(_DoublyLinkedBase):
 2    # A sequential container of elements with positional access
 3
 4    class Position:
 5      # The location of a single element
 6      def __init__ (self, container, node):
 7        self._container = container
 8        self._node = node
 9
 10     def element(self):
 11       return self._node._element
 12
 13     def __eq__ (self, other):
 14       # Returns True if other is a position representing the same location
 15       return type(other) is type(self) and other._node is self._node
 16
 17     def __ne__ (self, other):
 18       # Returns True if other does not represent the same location
 19       return not (self == other)
 20
 21   #------------------------------- utility methods -------------------------------
 22
 23   def _validate(self, p):
 24     # Returns node in position p, or raise error if invalid
 25     if not isinstance(p, self.Position):
 26       raise TypeError('p must be proper Position type')
 27     if p._container is not self:
 28       raise ValueError('p does not belong to this container')
 29     if p._node._next is None:                  # invalid node
 30       raise ValueError('p is no longer valid')
 31     return p._node
 32
 33   def _make_position(self, node):
 34     # Returns Position instance for given node (None if sentinel)
 35     if node is self._header or node is self._trailer:
 36       return None
 37     else:
 38       return self.Position(self, node)
 39
 40   #------------------------------- accessors -------------------------------
 41
 42   def first(self):
 43     # Returns the first Position in the list
 44     return self._make_position(self._header._next)
 45
 46   def last(self):
 47     # Returns the last Position in the list
 48     return self._make_position(self._trailer._prev)
 49
 50   def before(self, p):
 51     # Returns the Position before Position p
 52     node = self._validate(p)
 53     return self._make_position(node._prev)
 54
 55   def after(self, p):
 56     # Returns the Position after Position p
 57     node = self._validate(p)
 58     return self._make_position(node._next)
 59
 60   def __iter__(self):
 61     # Generate a forward iteration of the elements of the list
 62     cursor = self.first( )
 63     while cursor is not None:
 64       yield cursor.element( )
 65       cursor = self.after(cursor)

I metodi di update sono nel codice seguente::

 66   # inherited version overridden, returns Position, rather than Node
 67   def _insert_between(self, e, predecessor, successor):
 68     # Adds element between existing nodes and return new Position
 69     node = super()._insert_between(e, predecessor, successor)
 70     return self._make_position(node)
 71
 72   def add_first(self, e):
 73     # Inserts element e at the front of the list and returns Position
 74     return self._insert_between(e, self._header, self._header._next)
 75
 76   def add_last(self, e):
 77     # Inserts element e at the back of the list and returns Position
 78     return self._insert_between(e, self._trailer._prev, self._trailer)
 79
 80   def add_before(self, p, e):
 81     # Inserts element e into list before Position p and returns Position
 82     original = self._validate(p)
 83     return self._insert_between(e, original._prev, original)
 84
 85   def add_after(self, p, e):
 86     # Inserts element e into list after Position p and returns Position
 87     original = self._validate(p)
 88     return self._insert_between(e, original, original._next)
 89
 90   def delete(self, p):
 91     # Removes and returns the element at Position p
 92     original = self._validate(p)
 93     return self._delete_node(original)
 94
 95   def replace(self, p, e):
 96     # Replace the element at Position p with e
 97     original = self._validate(p)
 98     old_value = original._element           # temporarily store old element
 99     original._element = e                   # replace with new element
 100    return old_value                        # return the old element value


Sezione 5. Ordinare una lista posizionale
-----------------------------------------

Introduciamo una implementazione dell'*insertion sort* per una lista posizionale.
Manteniamo la variabile ``marker`` che rappresenta la posizione più a destra della
porzione della lista correntemente ordinata.
Il pivot e immediatamente dopo il marker, e usiamo la variabile ``move_left`` 
per spostarci a sinistra dal marker, mentre c'è ancora un elemento con valore più grande del pivot.
In figura mostriamo la situazione di una lista durante il procedimento.

.. image::  https://bitbucket.org/ccinfinite/algorithms/downloads/figura512.png
   :height: 100px
   :width: 200px
   :scale: 10
   :alt: alternate text
   :align: center

Di seguito il codice per l'insertion sort::

 1  def insertion_sort(L):
 2
 3    if len(L) > 1:
 4      marker = L.first( )
 5      while marker != L.last( ):
 6        pivot = L.after(marker)
 7        value = pivot.element( )
 8        if value > marker.element( ):
 9          marker = pivot                        # pivot becomes new marker
 10       else:
 11         move_left = marker                    # find leftmost item greater than value
 12         while move_left != L.first( ) and L.before(move_left).element( ) > value:
 13           move_left = L.before(move_left)
 14         L.delete(pivot)
 15         L.add_before(move_left, value)        # insert value before move_left



Sezione 6. Sequenze basate su array e su collegamento 
-----------------------------------------------------

Le strutture dati basate su array e quelle basate su collegamento hanno vantaggi e svantaggi.
In questa sezione discutiamo di alcuni di essi.

Sezione 6.1 Vantaggi delle sequenze basate su array
===================================================

In questo tipo di strutture siamo in grado di accedere a un generico elemento in tempo \\(O(1)\\);
di contro, trovare il \\(k\\)-mo elemento in una lista concatenata richiede tempo \\(O(k)\\)
(*linear time* complexity), per attraversare la lista.

La rappresentazione tramite array usa meno memoria rispetto alle strutture concatenate.
Anche se la lunghezza effettiva di un array dinamico può essere maggiore del numero degli
elementi immagazzinati, notiamo che sia le liste basate su array che le liste concatenate sono
strutture di riferimenti e quindi la memoria richiesta per immagazzinare gli elementi effettivi
è la stessa per entrambe le strutture.
Inoltre per una struttura basata su array con \\(n\\) elementi, il caso peggiore capita tipicamente
quando si deve ridimesionare l'array per  \\(2n\\) riferimenti.
Con una struttura concatenata la memoria deve essere dedicata non solo ai riferimentiper ogni oggetto,
ma anche ai riferimenti per i collegamenti fra i nodi; una lista concatenata singolarmente
di lunghezza \\(n\\) richiede sempre \\(2n\\) riferimenti (uno per l'elemento e uno per il nodo successivo,
per ogni nodo); con una lista doppiamente concatenata sono necessari \\(3n\\) riferimenti.

Le operazioni con un limite asintotico equivalente sono più efficienti per le sequenze basate su array,
per un fattore costante, rispetto alle stesse operazioni effettuate su strutture concatenate.
Ciò dipende dalla necessità di dover istanziare un nuovo nodo ogni volta che si devono realizzare
operazioni come la enqueue. 

Sezione 6.2 Vantaggi delle sequenze basate su collegamento
==========================================================

Se una operazione su una data struttura dati è usata in un sistema in real-time,
essa è progettata per fornire risposte immediate; un lungo ritardo causato 
da una singola operazione ammortizzata potrebbe avere un effetto avverso sul sistema.
In questo caso le strutture basate su collegamento hanno un vantaggio su quelle basate su array.

Un altro importante vantaggio sta nel fatto che le strutture basate su collegamento supportano inserimento
e cancellazione in tempo \\(O(1)\\) per posizioni arbitrarie.
Inserire o cancellare un elemento alla fine di una lista basata su array può essere fatto in tempo
costante; ma inserimenti o cancellazioni più generali sono più costose. Ad esempio, una chiamata a un
inserimento o cancellazione con indice \\(k\\) usa tempo \\(O(n−k+1)\\),
dovendo shiftare opportunamente tutti gli elementi successivi al punto di inserimento o cancellazione.